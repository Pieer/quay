'use strict'
### global page ###

$ ->
  $menu      = $ '#menus a'
  $carte     = $ '.carte-container'
  current    = ''
  $activeNav = $menu.first()

  activeMenu = ->
    $activeNav.removeClass('active')
    # Add active button state
    $activeNav = $(@)
    $activeNav.addClass('active')

    unless $(@).data('kind') is 'pdf'
      # Find the selected carte
      ref = $(@).attr('href').replace('/menu#','.').replace('/menu.html#','.')
      # Reveal cartes
      if current is ''
        $carte.addClass('active')
        $(ref).fadeIn(100)
      else
        if(current is ref)
          $carte.toggleClass('active')
          $(current).fadeToggle $carte.hasClass('active')
        else
          $carte.addClass('active')
          $current = $(current)
          $current.fadeOut( 500, ->
            $(ref).fadeIn(300)
          )
      current = ref
    return
  
  $menu.on 'click', activeMenu

  selectedMenu = -1
  for item, index in ['lunch','dinner','tasting']
    if page item
      selectedMenu = index
      break

  if selectedMenu >= 0
    $menu.eq(selectedMenu).click()
  else
    $menu.first().click() if $(window).width() < 769
