'use strict'
### global FastClick:false, Carousel: false , Collapse: false, Nav: false , Popup: false , page ###

$ ->
  # Static and Cached variables
  _initialW = 0
  $window   = $ window
  $top      = $ '.top'
  $nav      = $ '#nav'

  # Implement fast click on mobile
  FastClick.attach(document.body)

  $('p').each ->
    $(this).html $(this).html().replace(/\s([^\s<]+)\s*$/, '&nbsp;$1')
    return

  # Get format to change the header and hero height
  headerHeight = ->
    w = $window.width()
    h = $window.height()
    # Avoid recalcul size when top bar disapear on mobile browser
    if _initialW isnt w
      # % of the screen visible
      ph = '0.6'
      if w > 500
        ph = '0.5'
        if w > 768
          ph = '0.6'
          if h < 900
            ph = '0.8'
        else
          if h < 380
            ph = '0.9'
      $top.css
        'height': h*ph
        'max-height': h
        'min-height': '200px'

      _initialW = w
    return

  headerHeight()
  $window.resize ->
    headerHeight()


  # Init navigation functionality ( hide/show, absolute/fixed )
  nav = new Nav
    win: $window
    nav: $nav
    top: $top
    content: $ '.container'
    toggler: $ '.nav-toggle, .nav-container'

  if page 'index','reservation'
    # Initialize top carousel
    new Carousel
      elms:  $ '.top-carousel div'
      ctrls: $ '.top .carousel-controler li'
      # lbls:  $ '.carousel-label'
      autoRotate: true

    new Collapse
      nav: nav
      togglePanel: $ '.toggle-panel'

  if page 'reservation', 'news-events'
    window.confirmationPopup = new Popup
      popup: $('.popup-bg')
      title: $('#ptitle')
      para: $('#ppara')

  if page 'functions', 'about'
    # Initialize panel carousels
    $('.small-carousel-container').each ->
      new Carousel
        elms:  $(@).find '.content-carousel'
        ctrls: $(@).find '.carousel-controler li'
        autoRotate: false

  if page 'contact'
    # Show marker dynamically to fix chrome retina bugs...
    new Map
      win: $window
      top: $top
      mapMarker: $ '.marker'

  if page 'functions','our-team'
    # Intialize collapsible elements
    new Collapse
      nav: nav
      togglePanel: $ '.toggle-panel'

  if page 'functions'
    $('#wedding-video').prettyEmbed
      videoID: 'jxnx1M9LfRM'
      customPreviewImage: '../static/images/bg/video-weddings.jpg'
      showInfo: false
      showControls: false
      loop: false
      colorScheme: 'dark'
      showRelated: false
      useFitVids: true

  if page 'menu'
    $('#chef-video').prettyEmbed
      videoID: '-OxA_W90Spo'
      customPreviewImage: '../static/images/chef.jpg'
      showInfo: false
      showControls: false
      loop: false
      colorScheme: 'dark'
      showRelated: false
      useFitVids: true

  if page 'food-philosophy'
    $('#video-philosophy').prettyEmbed
      videoID: '-OxA_W90Spo'
      customPreviewImage: '../static/images/philosophy.jpg'
      showInfo: false
      showControls: false
      loop: false
      colorScheme: 'dark'
      showRelated: false
      useFitVids: true
  
  return