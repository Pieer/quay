'use strict'

window.page = (pageName...) ->
  isPage = false
  for element in pageName
    if element is 'index'
      isPage = true if location.pathname is '/' or location.pathname is '/index'
    else
      isPage = true if location.href.indexOf(element) > -1
  isPage