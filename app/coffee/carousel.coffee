'use strict'
### global requestInterval: false, clearRequestInterval: false ###

class window.Carousel
  constructor: (opts) ->
    @i = 0
    @toUpdate = []
    @elms = opts.elms
    # @lbls = opts.lbls
    @ctrls = opts.ctrls
    @toUpdate.push @elms

    if opts.autoRotate
      @ticker = @auto()
      window.addEventListener 'scroll', =>
        @pause()
        return

    # Check if there is labels
    # if @lbls
    #   @toUpdate.push @lbls

    # Check if there is controler
    if @ctrls
      @toUpdate.push @ctrls
      # Bind controller
      @ctrls.on 'click', @manual

  auto: ->
    requestInterval =>
      @update()
    , 5000
    # Func return interval

  manual: (el) =>
    if @ticker
      clearRequestInterval(@ticker)
    $el = $(el.target)
    if $el.prop('tagName') is 'SPAN'
      $el = $el.parent()
    @update $el.index()
    if @ticker
      @ticker = @auto()
    return

  pause: () ->
    clearRequestInterval(@ticker)
    @ticker = setTimeout( =>
      clearRequestInterval(@ticker)
      @ticker = @auto()
      return
    ,1000)
    return

  update: (index = @i) ->
    @updateClass()
    if index is @i
      @i++
      @i = 0 if @i >= @elms.length
    else
      @i = index
    @updateClass()
    return

  updateClass: ->
    for element in @toUpdate
      # element.toggleClass('active')
      element.eq(@i).toggleClass('active')
    el = @elms.eq(@i)
    setTimeout( ->
      el.toggleClass('anim')
    ,100)

    return