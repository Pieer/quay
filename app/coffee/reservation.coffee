'use strict'

$ ->
  #  input.name: name,company_name,phone,postcode,contact,date,period,time,email,comments,newsletter,button
  isFormValid = true
  freeze = false
  filters = 
    email: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    phone: /^(?:.{8,})$/

  textFields = 'input[type=text], input[type=tel], input[type=email], input[type=number]'

  checkField = (field) ->
    val = field.value
    switch field.name
      when 'email'
        filters.email.test val
      when 'cm-irlhjku-irlhjku'
        filters.email.test val
      when 'personal_phone'
        filters.phone.test val
      else
        val isnt ''

  toggleFocusIndicator = (e, val) ->
    $(e.target).parent().toggleClass 'focus',val
    $('form').toggleClass 'focus-form',val
    return

  validationIndicator = (e) ->
    if e
      keyCode = e.keyCode or e.which
      return  if keyCode is 9 # Press tab? -> return

    $(e.target).parent().toggleClass 'valid-field',checkField(e.target)
    return

  toggleError = (el, valid) ->
    $(el).parent().toggleClass 'error',!valid
    unless valid
      isFormValid = false
    return

  sendForm = (el) ->
    $(el).addClass('loading')
    # Map form with server id
    $.ajax
      url: 'reservation/' # the endpoint
      type: 'POST' # http method
      data:
        csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        full_name: $('#id_first_name').val() + ' ' + $('#id_last_name').val()
        first_name: $('#id_first_name').val()
        last_name: $('#id_last_name').val()
        email_address: $('#id_email_address').val()
        company: $('#id_company').val()
        personal_phone: $('#id_personal_phone').val()
        hotel_phone: $('#id_hotel_phone').val()
        postcode: $('#id_postcode').val()
        date: $('#id_date').val()
        time: $('#id_time').val()
        lunch_dinner: $('input[name=lunch_dinner]:checked').val()
        num_people: $('#id_num_people').val()
        special_requests: $('#id_special_requests').val()
        optin: $('#id_optin').is(':checked')

      success: (json) ->
        freeze = false
        displayMessage(json)
        ga('send', 'event', 'form', 'submit', 'reservation')
        return
        
      error: () ->
        freeze = false
        displayMessage
          title: 'THERE WAS AN ERROR WITH OUR SERVER'
          para: 'Please try again later or call us.'
        return

    freeze = false
    return

  submitSubscription = (e) ->
    e.preventDefault()
    unless freeze
      freeze = true
      isFormValid = true
      email = document.getElementById('fieldEmail')
      toggleError(email, checkField email)
      sendSubscribtion(true,$('#subscribe_form').serialize()) if isFormValid
      freeze = isFormValid
      false

  sendSubscribtion = (showMessage, formData) ->
    $('form').addClass('loading')

    $.post '/subscribe/', formData, (data) ->
      if showMessage
        if data.Status is 200
          freeze = false
          displayMessage
            title: 'Thanks for subscribing.'
            para: data.Message
          return
        else
          freeze = false
          displayMessage
            title: 'THERE WAS AN ERROR WITH OUR SERVER'
            para: data.Message
          return
    false

  displayMessage = (json) ->
    $('form').removeClass('loading')
    window.confirmationPopup.show json
    return

  submitForm = (e) ->
    e.preventDefault()
    unless freeze
      freeze = true
      isFormValid = true
      e.preventDefault()
      # Check if every fields are valid
      $(textFields).each ->
        unless @.name in ['company','special_requests','hotel_phone']
          toggleError(@, checkField @ )
        return

      # Display error on selectbox wrapper if they are not valid
      select = $('select').parent()
      toggleError(select, select.hasClass 'valid-field')

      # Send the form if all the mandatoryt fields are valid
      if isFormValid
        sendForm(e.target)
      else
        ga('send', 'event', 'form', 'click', 'reservation')
        freeze = false
        # If invalid, scroll to error
        scrollTop = $('.error').first().offset().top - 30
        scrollTop -= 100 if $(window).width() > 768
        $('html,body').animate(
          scrollTop: scrollTop
        , 500)
    false


  createHourRange = (from, to, options=[])->
    for i in [from...to]  # jshint ignore:line
      options.push('<option value=\''+i+':00\'>'+i+':00 PM</option>')
      options.push('<option value=\''+i+':30\'>'+i+':30 PM</option>')
    options

  updateTimePicker = ->
    $time = $('#id_time')
    if $(@).val() is 'lunch'
      $time.html createHourRange(12,14)

    else
      $time.html createHourRange(18,22)
    $time.selectOrDie('destroy')
    initSelect('id_time')
    return

  initSelect = (id) ->
    $('#'+id).selectOrDie
      placeholder: 'Select'
      onChange: ->
        $(@).parent().addClass 'valid-field'
        $(@).parent().parent().removeClass 'error'
        updateTimePeriod(@.value) if id is 'id_time'
        return
    return

  updateTimePeriod = (val)->
    # Change dinner/lunch radiobox according to time selected
    if parseInt(val.substring(0,2))>14
      period = 'dinner'
    else 
      period = 'lunch'

    $('input[name=lunch_dinner]').filter('[value='+period+']').attr('checked', true)
    return


  # Init Selectboxes
  initSelect('id_time')
  initSelect('id_num_people')

  # Init Date picker
  $('#id_date').pickadate
    editable: false
    today: ''
    clear: ''
    onSet: ->
      window.l = @
      day = @.get('select', 'ddd')
      if(day is 'Mon' or day is 'Tue' or day is 'Wed')
        if $('input[name=lunch_dinner]:radio').val() is 'lunch'
          $time = $('#id_time')
          $time.html createHourRange(18,22)
          $time.selectOrDie('destroy')
          initSelect('id_time')
        $('#lunch').prop('disabled', true)
        $('input[name=lunch_dinner]').filter('[value=dinner]').attr('checked', true)
      else
        $('#lunch').prop('disabled', false)


  # Bind input to events
  $('input[name=lunch_dinner]:radio').click updateTimePicker
  $('#reservation_form').submit submitForm
  $('#subscribe_form').submit submitSubscription
  $('.floatlabel-wrapper').find(textFields)
    .focus true, toggleFocusIndicator
    .blur false, toggleFocusIndicator
    .on 'keyup blur change', validationIndicator

  return