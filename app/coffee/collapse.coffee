'use strict'

class window.Collapse
  constructor: (opts) ->
    @nav = opts.nav
    @togglePanel = opts.togglePanel
    @togglePanel.on 'click', (e) =>
      @toggle(e)
      return false

  toggle: (e) ->
    $target = $(e.target).parents('.collapsible').first()
    $target.toggleClass('active')

    scrollTop = $target.offset().top
    $('html,body').animate(
      scrollTop: scrollTop
    , 500)
    if $target.hasClass('active')
      @nav.hide()
    return
