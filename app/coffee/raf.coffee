'use strict'
### global requestAnimFrame: false ###

window.requestAnimFrame = (->
  window.requestAnimationFrame or window.webkitRequestAnimationFrame or window.mozRequestAnimationFrame or window.oRequestAnimationFrame or window.msRequestAnimationFrame or (callback) ->
    window.setTimeout callback, 1000 / 60
)()

window.requestInterval = (fn, delay) ->
  return window.setInterval(fn, delay) if not window.requestAnimationFrame and not window.webkitRequestAnimationFrame and not (window.mozRequestAnimationFrame and window.mozCancelRequestAnimationFrame) and not window.oRequestAnimationFrame and not window.msRequestAnimationFrame

  start = new Date().getTime()
  handle = {}

  theLoop = ->
    current = new Date().getTime()
    delta = current - start
    if delta >= delay
      fn.call()
      start = new Date().getTime()
    handle.value = requestAnimFrame(theLoop)
    return 

  handle.value = requestAnimFrame(theLoop)
  return handle

window.clearRequestInterval = (handle) ->
  (if window.cancelAnimationFrame then window.cancelAnimationFrame(handle.value) else (if     window.webkitCancelAnimationFrame then window.webkitCancelAnimationFrame(handle.value) else     (if window.webkitCancelRequestAnimationFrame then     window.webkitCancelRequestAnimationFrame(handle.value) else (if     window.mozCancelRequestAnimationFrame then window.mozCancelRequestAnimationFrame(handle.value)     else (if window.oCancelRequestAnimationFrame then     window.oCancelRequestAnimationFrame(handle.value) else (if window.msCancelRequestAnimationFrame then window.msCancelRequestAnimationFrame(handle.value)         else clearInterval(handle)))))))