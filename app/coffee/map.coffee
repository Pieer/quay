'use strict'

class window.Map
  constructor: (opts) ->
    @win = opts.win
    @top = opts.top
    @mapMarker = opts.mapMarker
    @isVisible = @hideShow()
    @HH = @top.height()
    
    @win
      .on 'scroll', =>
        @isVisible = @hideShow()
        return
      .on 'resize', =>
        @HH = @top.height()
        return

  hideShow: ->
    st = @win.scrollTop()
    show = st > @HH + 80 and st < @HH + 2000
    if @isVisible isnt show
      @mapMarker.toggleClass 'active', show
    return show