'use strict'

class window.Nav
  constructor: (opts) ->
    @pause = false
    @toggleHidden = false
    @win = opts.win
    @nav = opts.nav
    @top = opts.top
    @content = opts.content
    @toggler = opts.toggler

    @toggler.on 'click', =>
      @toggleNav()

    @win
      .on 'scroll', =>
        @updatePosition()
        return
      .on 'resize', =>
        @initialPosition()
        return
    
    @initialPosition()
    @updatePosition()

  initialPosition: ->
    # Set absolute position of the nav to avoid reflow
    @HH = @top.height()
    @nav.css('top', @HH)
    return

  # Open or close nav on mobile
  toggleNav: ->
    @content.toggleClass 'overflow'
    @nav.toggleClass 'active'
    @toggler.toggleClass 'close'
    return

  hide: ->
    # Used in the carousel
    @pause = true
    @nav.fadeOut(300)
    @toggler.fadeOut(300)
    @toggleHidden = true
    setTimeout =>
      @nav.removeClass('show')
      @nav.css('display', 'block')
      @pause = false
      @toggleHidden = false
      return
    , 2000
    return

  updatePosition: () ->
      
    st = @win.scrollTop()
    unless @pause # Avoid changes after a limit
      classes = ''
      if st > @HH + 80
        classes = 'hide' # Opacity 0
        if st > @HH + 200
          classes += ' hide-transition' # Add opacity transition
          if st > @HH + 300
            classes += ' fixed' # Fix nav to top
            if st > @HH + 600
              classes += ' show' # Opacity 1
      @nav.attr('class',classes)
    unless @toggleHidden
      @toggler.fadeIn(300)
      @toggler.removeClass 'close'
    return