'use strict'

class window.Popup
  constructor: (opts) ->
    @popup = opts.popup
    @title = opts.title
    @para = opts.para
    @popup.click =>
      @.hide()

  show: (message) =>    
    @title.html(message.title)
    @para.html(message.para)
    @popup.show =>
      @popup.addClass('active')

  hide: =>
    @popup.removeClass('active')
    setTimeout =>
      @popup.hide()
    ,500