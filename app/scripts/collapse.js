'use strict';
window.Collapse = (function() {
  function Collapse(opts) {
    this.nav = opts.nav;
    this.togglePanel = opts.togglePanel;
    this.togglePanel.on('click', (function(_this) {
      return function(e) {
        return _this.toggle(e);
      };
    })(this));
  }

  Collapse.prototype.toggle = function(e) {
    var $target, scrollTop;
    $target = $(e.target).parents('.collapsible').first();
    $target.toggleClass('active');
    scrollTop = $target.offset().top;
    $('html,body').animate({
      scrollTop: scrollTop
    }, 500);
    if ($target.hasClass('active')) {
      this.nav.hide();
    }
  };

  return Collapse;

})();
