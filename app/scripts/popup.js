'use strict';
var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.Popup = (function() {
  function Popup(opts) {
    this.hide = __bind(this.hide, this);
    this.show = __bind(this.show, this);
    this.popup = opts.popup;
    this.title = opts.title;
    this.para = opts.para;
    this.popup.click((function(_this) {
      return function() {
        return _this.hide();
      };
    })(this));
  }

  Popup.prototype.show = function(message) {
    this.title.html(message.title);
    this.para.html(message.para);
    return this.popup.show((function(_this) {
      return function() {
        return _this.popup.addClass('active');
      };
    })(this));
  };

  Popup.prototype.hide = function() {
    this.popup.removeClass('active');
    return setTimeout((function(_this) {
      return function() {
        return _this.popup.hide();
      };
    })(this), 500);
  };

  return Popup;

})();
