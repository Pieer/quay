'use strict';
var __slice = [].slice;

window.page = function() {
  var element, isPage, pageName, _i, _len;
  pageName = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  isPage = false;
  for (_i = 0, _len = pageName.length; _i < _len; _i++) {
    element = pageName[_i];
    if (element === 'index') {
      if (location.pathname === '/' || location.pathname === '/index') {
        isPage = true;
      }
    } else {
      if (location.href.indexOf(element) > -1) {
        isPage = true;
      }
    }
  }
  return isPage;
};
