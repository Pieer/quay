'use strict';

/* global requestInterval: false, clearRequestInterval: false */
var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.Carousel = (function() {
  function Carousel(opts) {
    this.manual = __bind(this.manual, this);
    this.i = 0;
    this.toUpdate = [];
    this.elms = opts.elms;
    this.ctrls = opts.ctrls;
    this.toUpdate.push(this.elms);
    if (opts.autoRotate) {
      this.ticker = this.auto();
      window.addEventListener('scroll', (function(_this) {
        return function() {
          _this.pause();
        };
      })(this));
    }
    if (this.ctrls) {
      this.toUpdate.push(this.ctrls);
      this.ctrls.on('click', this.manual);
    }
  }

  Carousel.prototype.auto = function() {
    return requestInterval((function(_this) {
      return function() {
        return _this.update();
      };
    })(this), 5000);
  };

  Carousel.prototype.manual = function(el) {
    var $el;
    if (this.ticker) {
      clearRequestInterval(this.ticker);
    }
    $el = $(el.target);
    if ($el.prop('tagName') === 'SPAN') {
      $el = $el.parent();
    }
    this.update($el.index());
    if (this.ticker) {
      this.ticker = this.auto();
    }
  };

  Carousel.prototype.pause = function() {
    clearRequestInterval(this.ticker);
    this.ticker = setTimeout((function(_this) {
      return function() {
        clearRequestInterval(_this.ticker);
        _this.ticker = _this.auto();
      };
    })(this), 1000);
  };

  Carousel.prototype.update = function(index) {
    if (index == null) {
      index = this.i;
    }
    this.updateClass();
    if (index === this.i) {
      this.i++;
      if (this.i >= this.elms.length) {
        this.i = 0;
      }
    } else {
      this.i = index;
    }
    this.updateClass();
  };

  Carousel.prototype.updateClass = function() {
    var el, element, _i, _len, _ref;
    _ref = this.toUpdate;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      element = _ref[_i];
      element.eq(this.i).toggleClass('active');
    }
    el = this.elms.eq(this.i);
    setTimeout(function() {
      return el.toggleClass('anim');
    }, 100);
  };

  return Carousel;

})();
