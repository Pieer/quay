'use strict';
window.Nav = (function() {
  function Nav(opts) {
    this.pause = false;
    this.toggleHidden = false;
    this.win = opts.win;
    this.nav = opts.nav;
    this.top = opts.top;
    this.content = opts.content;
    this.toggler = opts.toggler;
    this.toggler.on('click', (function(_this) {
      return function() {
        return _this.toggleNav();
      };
    })(this));
    this.win.on('scroll', (function(_this) {
      return function() {
        _this.updatePosition();
      };
    })(this)).on('resize', (function(_this) {
      return function() {
        _this.initialPosition();
      };
    })(this));
    this.initialPosition();
    this.updatePosition();
  }

  Nav.prototype.initialPosition = function() {
    this.HH = this.top.height();
    this.nav.css('top', this.HH);
  };

  Nav.prototype.toggleNav = function() {
    this.content.toggleClass('overflow');
    this.nav.toggleClass('active');
    this.toggler.toggleClass('close');
  };

  Nav.prototype.hide = function() {
    this.pause = true;
    this.nav.fadeOut(300);
    this.toggler.fadeOut(300);
    this.toggleHidden = true;
    setTimeout((function(_this) {
      return function() {
        _this.nav.removeClass('show');
        _this.nav.css('display', 'block');
        _this.pause = false;
        _this.toggleHidden = false;
      };
    })(this), 2000);
  };

  Nav.prototype.updatePosition = function() {
    var classes, st;
    st = this.win.scrollTop();
    if (!this.pause) {
      classes = '';
      if (st > this.HH + 80) {
        classes = 'hide';
        if (st > this.HH + 200) {
          classes += ' hide-transition';
          if (st > this.HH + 300) {
            classes += ' fixed';
            if (st > this.HH + 600) {
              classes += ' show';
            }
          }
        }
      }
      this.nav.attr('class', classes);
    }
    if (!this.toggleHidden) {
      this.toggler.fadeIn(300);
      this.toggler.removeClass('close');
    }
  };

  return Nav;

})();
