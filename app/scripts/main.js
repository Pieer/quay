'use strict';

/* global FastClick:false, Carousel: false , Collapse: false, Nav: false , Popup: false , page */
$(function() {
  var $nav, $top, $window, headerHeight, nav, _initialW;
  _initialW = 0;
  $window = $(window);
  $top = $('.top');
  $nav = $('#nav');
  FastClick.attach(document.body);
  $('p').each(function() {
    $(this).html($(this).html().replace(/\s([^\s<]+)\s*$/, '&nbsp;$1'));
  });
  headerHeight = function() {
    var h, ph, w;
    w = $window.width();
    h = $window.height();
    if (_initialW !== w) {
      ph = '0.6';
      if (w > 500) {
        ph = '0.5';
        if (w > 768) {
          ph = '0.6';
          if (h < 900) {
            ph = '0.8';
          }
        } else {
          if (h < 380) {
            ph = '0.9';
          }
        }
      }
      $top.css({
        'height': h * ph,
        'max-height': h,
        'min-height': '200px'
      });
      _initialW = w;
    }
  };
  headerHeight();
  $window.resize(function() {
    return headerHeight();
  });
  nav = new Nav({
    win: $window,
    nav: $nav,
    top: $top,
    content: $('.container'),
    toggler: $('.nav-toggle, .nav-container')
  });
  if (page('index', 'reservation')) {
    new Carousel({
      elms: $('.top-carousel div'),
      ctrls: $('.top .carousel-controler li'),
      autoRotate: true
    });
  }
  if (page('reservation', 'news-events')) {
    window.confirmationPopup = new Popup({
      popup: $('.popup-bg'),
      title: $('#ptitle'),
      para: $('#ppara')
    });
  }
  if (page('functions', 'about')) {
    $('.small-carousel-container').each(function() {
      return new Carousel({
        elms: $(this).find('.content-carousel'),
        ctrls: $(this).find('.carousel-controler li'),
        autoRotate: false
      });
    });
  }
  if (page('contact')) {
    new Map({
      win: $window,
      top: $top,
      mapMarker: $('.marker')
    });
  }
  if (page('functions', 'our-team')) {
    new Collapse({
      nav: nav,
      togglePanel: $('.toggle-panel')
    });
  }
  if (page('functions')) {
    $('#wedding-video').prettyEmbed({
      videoID: 'jxnx1M9LfRM',
      customPreviewImage: '../static/images/bg/video-weddings.jpg',
      showInfo: false,
      showControls: false,
      loop: false,
      colorScheme: 'dark',
      showRelated: false,
      useFitVids: true
    });
  }
  if (page('menu')) {
    $('#chef-video').prettyEmbed({
      videoID: '-OxA_W90Spo',
      customPreviewImage: '../static/images/chef.jpg',
      showInfo: false,
      showControls: false,
      loop: false,
      colorScheme: 'dark',
      showRelated: false,
      useFitVids: true
    });
  }
  if (page('food-philosophy')) {
    $('#video-philosophy').prettyEmbed({
      videoID: '-OxA_W90Spo',
      customPreviewImage: '../static/images/philosophy.jpg',
      showInfo: false,
      showControls: false,
      loop: false,
      colorScheme: 'dark',
      showRelated: false,
      useFitVids: true
    });
  }
});
