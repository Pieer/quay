'use strict';

/* global page */
$(function() {
  var $activeNav, $carte, $menu, activeMenu, current, index, item, selectedMenu, _i, _len, _ref;
  $menu = $('#menus a');
  $carte = $('.carte-container');
  current = '';
  $activeNav = $menu.first();
  activeMenu = function() {
    var $current, ref;
    $activeNav.removeClass('active');
    $activeNav = $(this);
    $activeNav.addClass('active');
    if ($(this).data('kind') !== 'pdf') {
      ref = $(this).attr('href').replace('/menu#', '.').replace('/menu.html#', '.');
      if (current === '') {
        $carte.addClass('active');
        $(ref).fadeIn(100);
      } else {
        if (current === ref) {
          $carte.toggleClass('active');
          $(current).fadeToggle($carte.hasClass('active'));
        } else {
          $carte.addClass('active');
          $current = $(current);
          $current.fadeOut(500, function() {
            return $(ref).fadeIn(300);
          });
        }
      }
      current = ref;
    }
  };
  $menu.on('click', activeMenu);
  selectedMenu = -1;
  _ref = ['lunch', 'dinner', 'tasting'];
  for (index = _i = 0, _len = _ref.length; _i < _len; index = ++_i) {
    item = _ref[index];
    if (page(item)) {
      selectedMenu = index;
      break;
    }
  }
  if (selectedMenu >= 0) {
    return $menu.eq(selectedMenu).click();
  } else {
    if ($(window).width() < 769) {
      return $menu.first().click();
    }
  }
});
