'use strict';
window.Map = (function() {
  function Map(opts) {
    this.win = opts.win;
    this.top = opts.top;
    this.mapMarker = opts.mapMarker;
    this.isVisible = this.hideShow();
    this.HH = this.top.height();
    this.win.on('scroll', (function(_this) {
      return function() {
        _this.isVisible = _this.hideShow();
      };
    })(this)).on('resize', (function(_this) {
      return function() {
        _this.HH = _this.top.height();
      };
    })(this));
  }

  Map.prototype.hideShow = function() {
    var show, st;
    st = this.win.scrollTop();
    show = st > this.HH + 80 && st < this.HH + 2000;
    if (this.isVisible !== show) {
      this.mapMarker.toggleClass('active', show);
    }
    return show;
  };

  return Map;

})();
