'use strict';
$(function() {
  var checkField, createHourRange, displayMessage, filters, freeze, initSelect, isFormValid, sendForm, sendSubscribtion, submitForm, submitSubscription, textFields, toggleError, toggleFocusIndicator, updateTimePeriod, updateTimePicker, validationIndicator;
  isFormValid = true;
  freeze = false;
  filters = {
    email: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
    phone: /^(?:.{8,})$/
  };
  textFields = 'input[type=text], input[type=tel], input[type=email], input[type=number]';
  checkField = function(field) {
    var val;
    val = field.value;
    switch (field.name) {
      case 'email':
        return filters.email.test(val);
      case 'cm-irlhjku-irlhjku':
        return filters.email.test(val);
      case 'personal_phone':
        return filters.phone.test(val);
      default:
        return val !== '';
    }
  };
  toggleFocusIndicator = function(e, val) {
    $(e.target).parent().toggleClass('focus', val);
    $('form').toggleClass('focus-form', val);
  };
  validationIndicator = function(e) {
    var keyCode;
    if (e) {
      keyCode = e.keyCode || e.which;
      if (keyCode === 9) {
        return;
      }
    }
    $(e.target).parent().toggleClass('valid-field', checkField(e.target));
  };
  toggleError = function(el, valid) {
    $(el).parent().toggleClass('error', !valid);
    if (!valid) {
      isFormValid = false;
    }
  };
  sendForm = function(el) {
    $(el).addClass('loading');
    $.ajax({
      url: 'reservation/',
      type: 'POST',
      data: {
        csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
        full_name: $('#id_full_name').val(),
        email_address: $('#id_email_address').val(),
        company: $('#id_company').val(),
        personal_phone: $('#id_personal_phone').val(),
        hotel_phone: $('#id_hotel_phone').val(),
        postcode: $('#id_postcode').val(),
        date: $('#id_date').val(),
        time: $('#id_time').val(),
        lunch_dinner: $('input[name=lunch_dinner]:checked').val(),
        num_people: $('#id_num_people').val(),
        special_requests: $('#id_special_requests').val(),
        optin: $('#id_optin').is(':checked')
      },
      success: function(json) {
        freeze = false;
        return displayMessage(json);
      },
      error: function() {
        freeze = false;
        displayMessage({
          title: 'THERE WAS AN ERROR WITH OUR SERVER',
          para: 'Please try again later or call us'
        });
      }
    });
    if ($('#id_optin').is(':checked')) {
      sendSubscribtion(false);
    }
    freeze = false;
  };
  submitSubscription = function(e) {
    var email;
    e.preventDefault();
    if (!freeze) {
      freeze = true;
      isFormValid = true;
      email = document.getElementById('fieldEmail');
      toggleError(email, checkField(email));
      if (isFormValid) {
        sendSubscribtion(true);
      }
      freeze = isFormValid;
      return false;
    }
  };
  sendSubscribtion = function(showMessage) {
    $('form').addClass('loading');
    $.getJSON('http://email.pollen.com.au/t/r/s/irlhjku/?callback=?', $('#subscribe_form').serialize(), function(data) {
      if (showMessage) {
        if (data.Status === 200) {
          freeze = false;
          displayMessage({
            title: 'Thanks for subscribing.',
            para: data.Message
          });
        } else {
          freeze = false;
          displayMessage({
            title: 'THERE WAS AN ERROR WITH OUR SERVER',
            para: data.Message
          });
        }
      }
    });
    return false;
  };
  displayMessage = function(json) {
    $('form').removeClass('loading');
    window.confirmationPopup.show(json);
  };
  submitForm = function(e) {
    var scrollTop, select;
    e.preventDefault();
    if (!freeze) {
      freeze = true;
      isFormValid = true;
      e.preventDefault();
      $(textFields).each(function() {
        var _ref;
        if ((_ref = this.name) !== 'company' && _ref !== 'special_requests' && _ref !== 'hotel_phone') {
          toggleError(this, checkField(this));
        }
      });
      select = $('select').parent();
      toggleError(select, select.hasClass('valid-field'));
      if (isFormValid) {
        sendForm(e.target);
      } else {
        freeze = false;
        scrollTop = $('.error').first().offset().top - 30;
        if ($(window).width() > 768) {
          scrollTop -= 100;
        }
        $('html,body').animate({
          scrollTop: scrollTop
        }, 500);
      }
    }
    return false;
  };
  createHourRange = function(from, to, options) {
    var i, _i;
    if (options == null) {
      options = [];
    }
    for (i = _i = from; from <= to ? _i < to : _i > to; i = from <= to ? ++_i : --_i) {
      options.push('<option value=\'' + i + ':00\'>' + i + ':00 PM</option>');
      options.push('<option value=\'' + i + ':30\'>' + i + ':30 PM</option>');
    }
    return options;
  };
  updateTimePicker = function() {
    var $time;
    $time = $('#id_time');
    if ($(this).val() === 'lunch') {
      $time.html(createHourRange(12, 14));
    } else {
      $time.html(createHourRange(18, 22));
    }
    $time.selectOrDie('destroy');
    initSelect('id_time');
  };
  initSelect = function(id) {
    $('#' + id).selectOrDie({
      placeholder: 'Select',
      onChange: function() {
        $(this).parent().addClass('valid-field');
        $(this).parent().parent().removeClass('error');
        if (id === 'id_time') {
          updateTimePeriod(this.value);
        }
      }
    });
  };
  updateTimePeriod = function(val) {
    var period;
    if (parseInt(val.substring(0, 2)) > 14) {
      period = 'dinner';
    } else {
      period = 'lunch';
    }
    $('input[name=lunch_dinner]').filter('[value=' + period + ']').attr('checked', true);
  };
  initSelect('id_time');
  initSelect('id_num_people');
  $('#id_date').pickadate({
    editable: false,
    today: '',
    clear: ''
  });
  $('input[name=lunch_dinner]:radio').click(updateTimePicker);
  $('#reservation_form').submit(submitForm);
  $('#subscribe_form').submit(submitSubscription);
  $('.floatlabel-wrapper').find(textFields).focus(true, toggleFocusIndicator).blur(false, toggleFocusIndicator).on('keyup blur change', validationIndicator);
});
