<!DOCTYPE html>
<!-- [if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!-- [if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!-- [if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!-- [if gt IE 8]><!-->
<html ng-app="pollenApp" class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <base href="/">
    <title>Quay Restaurant</title>
    <meta name="description" content="Quay Restaurant is Australia's most awarded restaurant, and one of two Australian restaurants on the prestigious S. Pellegrino World's Top 50 List. Owned and operated by the Fink Group, the restaurant offers outstanding modern cuisine by award winning chef Peter Gilmore, and is situated in the stunning dress circle of Sydney Harbour.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimal-ui ,user-scalable=no">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="favicon-128.png" sizes="128x128">
    <meta name="google-site-verification" content="tfMV1epfpTJQ7IBfO2gNoU5-6G4IV9SB-BhD4b_mHjc">
    <meta name="keywords" content="Quay Restaurant, Fink Group, Chef Peter Gilmore, S.Pellegrino World's Top 50, Nature Based Cuisine, Functions, Weddings, Corporate Events, John Fink, Leon Fink">
    <meta property="og:title" content="Quay restaurant">
    <meta property="og:type" content="place">
    <meta property="og:description" content="At Quay our aim is to provide a truly memorable dining experience. An experience that celebrates the diversity of our wonderful Australian ingredients cooked with imagination and passion.">
    <meta property="og:site_name" content="Quay restaurant">
    <meta property="article:author" content="https://www.facebook.com/pages/Quay-Restaurant/114127581932410">
    <meta property="article:publisher" content="https://www.facebook.com/pages/Quay-Restaurant/114127581932410">
    <meta property="place:location:latitude" content="-33.858018">
    <meta property="place:location:longitude" content="151.210091">
    <meta property="og:url" content="http://www.quay.com.au">
    <meta name="twitter:image:src" content="http://www.quay.com.au/images/twitter.jpg">
    <meta property="og:image" content="http://www.quay.com.au/images/facebook.jpg">
    <!-- build:css({.tmp,app}) /styles/main.css-->
    <link rel="stylesheet" href="/styles/main.css">
    <!-- endbuild-->
    <script type="text/javascript" src="//use.typekit.net/bmo5vxg.js"></script>
    <script>
      try{Typekit.load();}catch(e){}
      
    </script>
  </head>
  <body class="page-awards footer-over-bg">
    <!--[if lt IE 9]>
    <style>body{height: 880px !important;}#nav,#content{display:none;}*{margin: 0;}img{width: 100%;}</style>
    <a href="http://browsehappy.com/" style="display:block; text-align:center;" target="_blank"><img style="border: 0;" src="/images/ie8.jpg" /></a>
    <![endif]-->
    <div class="nav-toggle hide-desktop"><span></span><span></span><span></span></div>
    <div id="nav">
      <ul>
        <li><a href="/">HOME</a></li>
        <li><a href="menu.html">MENU</a></li>
        <li><a href="about.html">ABOUT</a></li>
        <li><a href="functions.html">FUNCTIONS</a></li>
        <li><a href="contact.html">CONTACT</a></li>
        <li class="reservation"><a href="/reservation">RESERVATIONS</a></li>
      </ul>
    </div>
    <div id="content">
      <div class="top"><a href="/" class="icon-logo"></a>
        <div class="top-carousel full-bg">
          <div class="image-awards active"></div>
        </div>
      </div>
      <div class="panel-top white">
        <div class="container">
          <div class="panel">
            <h1 class="main-title">AWARDS</h1>
            <p>Quay has long been one of Australia’s most awarded restaurants. Quay has held the coveted 3 Hat & 3 Star rating in The Sydney Morning Herald Good Food Guide & Australian Gourmet Traveller Restaurant Guide for 13 consecutive years.</p>
          </div>
          <div class="line"></div>
        </div>
      </div>
      <section>
        <div class="container">
          <div class="panel white full award-container">
            <div class="two-columns">
              <div class="column">
                <p>Quay awarded the Hottest Restaurant in Australia in The Australian Hot 50 Restaurant Awards 2013<br><br><strong>Restaurant of the Year 2013</strong><br>The Sydney Morning Herald 2013 Good Food Guide<br><br><strong>Restaurant of the Year 2013</strong><br>Australian Gourmet Traveller 2013 Restaurant Guide<br><br><strong>Best Restaurant in Australasia</strong><br>S. Pellegrino World’s 50 Best Restaurants 2012<br><br><strong>Quay awarded 3 hats in Sydney Morning Herald Good Food Guide 2012</strong><br>For the 10th Consecutive year Quay is awarded 3 Chefs hats<br><br><strong>Placed 27th in the world</strong><br>S. Pellegrino World’s 50 Best Restaurants 2010<br><br><strong>Restaurant of the Year 2010</strong><br>The Sydney Morning Herald 2010 Good Food Guide<br><br><strong>Placed 46th in the world</strong><br>S. Pellegrino World’s 50 Best Restaurants 2009<br><br><strong>Restaurant of the Year 2009</strong><br>Australian Gourmet Traveller 2009 Restaurant Guide<br><br><strong>Restaurant of the Year 2008</strong><br>Restaurant & Catering 2008 National Awards for Excellence</p>
              </div>
              <div class="column">
                <p>Quay awarded 3 hats in Sydney Morning Herald Good Food Guide 2014 for the 12th conescutive year<br><br>Quay awarded 3 hats in Sydney Morning Herald Good Food Guide 2013<br><br><strong>Placed 29th in the world</strong><br>S. Pellegrino World’s 50 Best Restaurants 2012<br><br><strong>Chef of the Year 2012</strong><br>Peter Gilmore Awarded Chef of the year in Sydney Morning Herald Good Food Guide<br><br><strong>Placed 26th in the world</strong><br>S. Pellegrino World’s 50 Best Restaurants 2011<br><br><strong>Best Restaurant in Australasia</strong><br>S. Pellegrino World’s 50 Best Restaurants 2010<br><br><strong>Restaurant of the Year 2010</strong><br>Australian Gourmet Traveller 2010 Restaurant Guide<br><br><strong>Restaurant of the Year 2009</strong><br>The Sydney Morning Herald 2009 Good Food Guide<br><br><strong>Restaurant of the Year 2009</strong><br>Restaurant & Catering 2009 National Awards for Excellence</p>
              </div>
            </div>
          </div>
          <section class="panel dark full left latest-news-partial">
            <h2 class="title">LATEST NEWS</h2><a href="#">
              <div class="underline">Overseas Passenger Terminal construction</div>
              <div class="date">Sept 19, 2014</div>
              <p>During the overseas passenger terminal upgrade Quay will have modified opening hours. We will be closed for lunch Monday to Thursday and open Friday to Sunday lunch from 12pm - 2pm.</p></a><a href="news-events.html" class="button">NEWS + EVENTS</a>
            <div class="line"></div>
          </section>
          <section class="container">
            <div class="panel full media"><a href="reviews-media.html" class="button">REVIEWS</a></div>
          </section>
        </div>
        <section class="container">
          <div class="panel full contact">
            <div class="left">
              <p class="address">Upper Level,<br>Overseas Passenger Terminal<br>The Rocks, Sydney 2000</p>
            </div>
            <div class="right"><a href="tel:+61-2-9251-5600" class="tel">(61 2) 9251 5600</a><br><br><br><strong>Lunch service </strong>
              <p>Friday to Sunday from 12pm - 2pm</p><br><strong>Dinner service</strong>
              <p>Daily from 6pm - 10pm</p>
            </div>
            <div class="line"></div>
          </div>
        </section>
      </section>
      <div class="footer">
        <div class="icon-logo"></div>
        <ul class="footer-nav">
          <li><a href="/reservation">Reservations</a></li>
          <li><a href="functions.html">Functions</a></li>
          <li><a href="contact.html">Contact</a></li>
          <li><a href="reviews-media.html">Reviews & Media</a></li>
        </ul>
        <ul class="social">
          <li><a href="http://instagram.com/quayrestaurant" target="_blank"><span class="icon-instagram"></span></a></li>
          <li><a href="http://www.youtube.com/user/QuaySydney" target="_blank"><span class="icon-play"></span></a></li>
        </ul>
        <div class="legal">© Quay Restaurant.<br>Site by <a href="http://www.pollen.com.au/" target="_blank">Pollen. </a>Photography by <a href="http://www.brettstevensphotography.com/" target="_blank">Brett Stevens.</a></div>
      </div>
    </div>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      
      ga('create', 'UA-6526479-13', 'auto');
      ga('require', 'displayfeatures');
      ga('send', 'pageview');
      
    </script>
    <!-- build:js(app) /scripts/vendor.js-->
    <script src="/bower_components/modernizr/modernizr.js"></script>
    <script src="/bower_components/fastclick/lib/fastclick.js"></script>
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/pickadate/lib/picker.js"></script>
    <script src="/bower_components/pickadate/lib/picker.date.js"></script>
    <script src="/bower_components/fitvids/jquery.fitvids.js"></script>
    <!-- endbuild-->
    
    <!-- build:js({.tmp,app}) /scripts/scripts.js-->
    <script src="/scripts/selectordie.js"></script>
    <script src="/scripts/raf.js"></script>
    <script src="/scripts/router.js"></script>
    <script src="/scripts/prettyembed.js"></script>
    <script src="/scripts/popup.js"></script>
    <script src="/scripts/carousel.js"></script>
    <script src="/scripts/menu.js"></script>
    <script src="/scripts/nav.js"></script>
    <script src="/scripts/collapse.js"></script>
    <script src="/scripts/map.js"></script>
    <script src="/scripts/main.js"></script>
    <script src="/scripts/reservation.js"></script>
    <!-- endbuild-->
  </body>
</html>