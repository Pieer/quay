# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0018_auto_20141114_0754'),
    ]

    operations = [
        migrations.AddField(
            model_name='awardpage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='awardpage',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contactpage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contactpage',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='foodphilosophypage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='foodphilosophypage',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='media',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='media',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='newseventspage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='newseventspage',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ourproducerspage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ourproducerspage',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ourteampage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ourteampage',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='producers',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='producers',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reviewsmediapage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='reviewsmediapage',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='team',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='team',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 14), auto_now=True),
            preserve_default=False,
        ),
    ]
