# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0002_remove_reservation_time'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Award',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=1000)),
                ('image', models.FileField(upload_to=b'award_images/', blank=True)),
                ('team_image', models.FileField(upload_to=b'images/', blank=True)),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Background',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cssClass', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=1000)),
                ('image', models.FileField(upload_to=b'award_images/', blank=True)),
                ('position', models.IntegerField()),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewsArticle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=1000)),
                ('image', models.FileField(upload_to=b'news_images/', blank=True)),
                ('published_at', models.DateTimeField(auto_now=True)),
                ('published', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewsPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('background', models.ForeignKey(to='cms.Background')),
                ('news_articles', models.ManyToManyField(to='cms.NewsArticle')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='background',
            field=models.ForeignKey(to='cms.Background'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='news_article',
            field=models.ForeignKey(to='cms.NewsArticle'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='reservation',
            name='created_at',
            field=models.DateTimeField(default=datetime.date(2014, 11, 5), auto_now=True),
            preserve_default=False,
        ),
    ]
