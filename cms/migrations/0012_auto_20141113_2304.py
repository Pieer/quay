# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0011_menuitem_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('course_number', models.CharField(max_length=250)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MenuCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.CharField(max_length=250)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameModel(
            old_name='MenuItem',
            new_name='Dish',
        ),
        migrations.AddField(
            model_name='course',
            name='dishes',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.Dish'),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='menu',
            name='lunch_dinner',
        ),
        migrations.RemoveField(
            model_name='menu',
            name='menu_item',
        ),
        migrations.AddField(
            model_name='menu',
            name='category',
            field=models.ForeignKey(default='', to='cms.MenuCategory'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='menu',
            name='courses',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.Course'),
            preserve_default=True,
        ),
    ]
