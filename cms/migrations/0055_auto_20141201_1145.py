# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0054_reservationpage'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservationpage',
            name='cancelation_policy',
            field=models.CharField(default='', max_length=10000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reservationpage',
            name='credit_card',
            field=models.CharField(default='', max_length=10000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reservationpage',
            name='dress_code',
            field=models.CharField(default='', max_length=10000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reservationpage',
            name='intro_copy',
            field=models.CharField(default='', max_length=10000),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='reservationpage',
            name='background',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to=b'cms.Background'),
        ),
    ]
