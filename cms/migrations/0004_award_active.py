# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0003_auto_20141105_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='award',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
