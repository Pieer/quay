# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0023_auto_20141114_1222'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Producers',
            new_name='Producer',
        ),
    ]
