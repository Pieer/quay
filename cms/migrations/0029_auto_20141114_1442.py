# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0028_newsevents'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsevents',
            name='date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
