# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0050_menupage_wines_list'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menupage',
            name='wines_list',
            field=models.FileField(upload_to=b'pdf/', blank=True),
        ),
    ]
