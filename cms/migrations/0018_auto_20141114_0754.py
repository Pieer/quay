# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0017_auto_20141114_0109'),
    ]

    operations = [
        migrations.CreateModel(
            name='AwardPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('column_left', models.CharField(max_length=10000)),
                ('column_right', models.CharField(max_length=10000)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContactPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FoodPhilosophyPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('intro_copy2', models.CharField(max_length=1000)),
                ('nature', models.CharField(max_length=1000)),
                ('texture', models.CharField(max_length=1000)),
                ('intensity', models.CharField(max_length=1000)),
                ('purity', models.CharField(max_length=1000)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FunctionsPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quote', models.CharField(max_length=2000)),
                ('editor', models.CharField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewsEventsPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OurProducersPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OurTeamPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=10000)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Producers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=2000)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReviewsMediaPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('background', models.ForeignKey(to='cms.Background')),
                ('media', sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.Media')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('role', models.CharField(max_length=255)),
                ('small_description', models.CharField(max_length=500)),
                ('description', models.CharField(max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='ourproducerspage',
            name='producers',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.Producers'),
            preserve_default=True,
        ),
    ]
