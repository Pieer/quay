# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0022_team_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='ourteampage',
            name='producers',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.Team'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='team',
            name='description',
            field=models.CharField(max_length=2000),
        ),
    ]
