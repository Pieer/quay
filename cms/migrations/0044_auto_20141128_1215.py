# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0043_auto_20141126_1743'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contactpage',
            old_name='intro_copy',
            new_name='gift_voucher_copy',
        ),
        migrations.AddField(
            model_name='contactpage',
            name='reservation_copy',
            field=models.CharField(default='', max_length=1000),
            preserve_default=False,
        ),
    ]
