# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0035_newsarticle_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsarticle',
            name='image',
            field=models.FileField(upload_to=b'news/', blank=True),
        ),
    ]
