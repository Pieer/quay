# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0041_auto_20141126_1737'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='functionspage',
            name='greenroom_carou',
        ),
        migrations.RemoveField(
            model_name='functionspage',
            name='restaurant_carou',
        ),
        migrations.RemoveField(
            model_name='functionspage',
            name='uppertower_carou',
        ),
    ]
