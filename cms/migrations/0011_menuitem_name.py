# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0010_auto_20141113_1846'),
    ]

    operations = [
        migrations.AddField(
            model_name='menuitem',
            name='name',
            field=models.CharField(default='', max_length=1500),
            preserve_default=False,
        ),
    ]
