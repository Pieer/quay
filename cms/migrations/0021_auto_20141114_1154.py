# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0020_auto_20141114_1120'),
    ]

    operations = [
        migrations.RenameField(
            model_name='media',
            old_name='image',
            new_name='pdf',
        ),
    ]
