# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0032_auto_20141114_1517'),
    ]

    operations = [
        migrations.AddField(
            model_name='newseventspage',
            name='events',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.NewsArticle'),
            preserve_default=True,
        ),
    ]
