# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0055_auto_20141201_1145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reservationpage',
            name='background',
        ),
    ]
