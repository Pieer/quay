# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0053_auto_20141201_1056'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReservationPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('active', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('background', models.ManyToManyField(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
