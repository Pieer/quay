# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0046_auto_20141128_1531'),
    ]

    operations = [
        migrations.RenameField(
            model_name='specialcontact',
            old_name='name_function',
            new_name='function',
        ),
        migrations.AddField(
            model_name='specialcontact',
            name='name',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
