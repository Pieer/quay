# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0047_auto_20141128_1602'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialcontact',
            name='image',
            field=models.ImageField(upload_to=b'peoples/', blank=True),
        ),
    ]
