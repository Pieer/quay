# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0038_newseventspage_events'),
    ]

    operations = [
        migrations.AddField(
            model_name='functionspage',
            name='greenroom_carou',
            field=models.ManyToManyField(to='cms.Background'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='greenroom_copy1',
            field=models.CharField(default='', max_length=2000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='greenroom_copy2',
            field=models.CharField(default='', max_length=2000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='greenroom_image',
            field=models.ForeignKey(related_name=b'functionspage_greenroom_image', default='', to='cms.Background'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='restaurant_carou',
            field=models.ManyToManyField(related_name=b'functionspage_restaurant_carou', to='cms.Background'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='restaurant_copy1',
            field=models.CharField(default='', max_length=2000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='restaurant_copy2',
            field=models.CharField(default='', max_length=2000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='restaurant_image',
            field=models.ForeignKey(related_name=b'functionspage_restaurant_image', default='', to='cms.Background'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='uppertower_carou',
            field=models.ManyToManyField(related_name=b'functionspage_uppertower_carou', to='cms.Background'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='uppertower_copy1',
            field=models.CharField(default='', max_length=2000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='uppertower_copy2',
            field=models.CharField(default='', max_length=2000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='uppertower_image',
            field=models.ForeignKey(related_name=b'functionspage_uppertower_image', default='', to='cms.Background'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='wedding_background',
            field=models.ForeignKey(related_name=b'functionspage_wedding_background', default='', to='cms.Background'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='wedding_copy',
            field=models.CharField(default='', max_length=1000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='wedding_title',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='functionspage',
            name='wedding_video',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='functionspage',
            name='background',
            field=models.ForeignKey(related_name=b'functionspage_background', to='cms.Background'),
        ),
    ]
