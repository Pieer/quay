# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20141114_0009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menupage',
            name='additional_info',
            field=models.CharField(max_length=1500),
        ),
    ]
