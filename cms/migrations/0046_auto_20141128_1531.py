# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0045_auto_20141128_1412'),
    ]

    operations = [
        migrations.AddField(
            model_name='functionspage',
            name='contacts',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.SpecialContact'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='specialcontact',
            name='image',
            field=models.ImageField(default='', upload_to=b'media/', blank=True),
            preserve_default=False,
        ),
    ]
