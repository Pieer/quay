# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0019_auto_20141114_0801'),
    ]

    operations = [
        migrations.AddField(
            model_name='media',
            name='image',
            field=models.FileField(default='', upload_to=b'media/', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='media',
            name='url',
            field=models.CharField(max_length=500, null=True, blank=True),
            preserve_default=True,
        ),
    ]
