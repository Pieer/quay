# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0033_newseventspage_events'),
    ]

    operations = [
        migrations.DeleteModel(
            name='NewsEvent',
        ),
    ]
