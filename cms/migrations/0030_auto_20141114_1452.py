# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0029_auto_20141114_1442'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='NewsEvents',
            new_name='NewsEvent',
        ),
        migrations.AddField(
            model_name='newseventspage',
            name='media',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.NewsEvent'),
            preserve_default=True,
        ),
    ]
