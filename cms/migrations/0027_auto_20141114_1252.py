# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0026_auto_20141114_1234'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ourteampage',
            old_name='producers',
            new_name='teams',
        ),
    ]
