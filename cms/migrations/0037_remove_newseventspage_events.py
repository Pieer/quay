# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0036_auto_20141114_1552'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='newseventspage',
            name='events',
        ),
    ]
