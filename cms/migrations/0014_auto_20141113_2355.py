# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_course_course_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='menu',
            name='additional_info',
            field=models.CharField(default='', max_length=1000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='menu',
            name='prices',
            field=models.CharField(default='', max_length=250),
            preserve_default=False,
        ),
    ]
