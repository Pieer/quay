# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('full_name', models.CharField(max_length=255)),
                ('email_address', models.EmailField(max_length=255)),
                ('company', models.CharField(max_length=255, null=True, blank=True)),
                ('personal_phone', models.CharField(max_length=255)),
                ('hotel_phone', models.CharField(max_length=255, null=True, blank=True)),
                ('postcode', models.CharField(max_length=255)),
                ('reservation_time', models.DateTimeField(null=True, blank=True)),
                ('time', models.DateTimeField(null=True, blank=True)),
                ('num_people', models.CharField(max_length=255)),
                ('lunch_dinner', models.CharField(max_length=255)),
                ('special_requests', models.CharField(max_length=1000, blank=True)),
                ('optin', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
