# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0030_auto_20141114_1452'),
    ]

    operations = [
        migrations.RenameField(
            model_name='newseventspage',
            old_name='media',
            new_name='events',
        ),
    ]
