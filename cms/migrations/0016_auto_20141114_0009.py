# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0015_auto_20141114_0007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menu',
            name='additional_info',
            field=models.CharField(max_length=1000, null=True, blank=True),
        ),
    ]
