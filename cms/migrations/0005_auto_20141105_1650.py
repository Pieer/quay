# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0004_award_active'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='award',
            name='active',
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
