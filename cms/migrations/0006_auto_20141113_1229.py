# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0005_auto_20141105_1650'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cssClass', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address_line_1', models.CharField(max_length=250)),
                ('address_line_2', models.CharField(max_length=250)),
                ('address_line_3', models.CharField(max_length=250)),
                ('tel', models.CharField(max_length=250)),
                ('fax', models.CharField(max_length=250)),
                ('email', models.CharField(max_length=250)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HomePage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('welcome_copy', models.CharField(max_length=500)),
                ('menu_copy', models.CharField(max_length=500)),
                ('menu_copy2', models.CharField(max_length=500)),
                ('function_copy', models.CharField(max_length=500)),
                ('about_copy', models.CharField(max_length=500)),
                ('promo_title', models.CharField(max_length=500)),
                ('promo_copy', models.CharField(max_length=500)),
                ('display_promo', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('background', models.ManyToManyField(to='cms.Background')),
                ('news_article', models.ForeignKey(to='cms.NewsArticle')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OpeningHours',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lunch_service', models.CharField(max_length=250)),
                ('dinner_service', models.CharField(max_length=250)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='book',
            name='image',
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='books',
            field=models.ManyToManyField(to='cms.Book'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='author',
            field=models.CharField(default=b'', max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='book_image',
            field=models.ForeignKey(to='cms.BookImage', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='link',
            field=models.CharField(default=b'', max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='quote',
            field=models.CharField(default=b'', max_length=500),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='quote_author',
            field=models.CharField(default=b'', max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='sub_author',
            field=models.CharField(default=b'', max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='sub_title_line_1',
            field=models.CharField(default=b'', max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='sub_title_line_2',
            field=models.CharField(default=b'', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='description',
            field=models.CharField(default=b'', max_length=1500),
        ),
        migrations.AlterField(
            model_name='book',
            name='position',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='book',
            name='title',
            field=models.CharField(default=b'', max_length=255),
        ),
    ]
