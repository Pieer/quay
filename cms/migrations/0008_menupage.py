# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0007_auto_20141113_1412'),
    ]

    operations = [
        migrations.CreateModel(
            name='MenuPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('intro', models.CharField(max_length=500)),
                ('additional_info', models.CharField(max_length=500)),
                ('philosophy', models.CharField(max_length=500)),
                ('our_produce', models.CharField(max_length=500)),
                ('active', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
