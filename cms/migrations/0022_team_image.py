# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0021_auto_20141114_1154'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='image',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
    ]
