# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0012_auto_20141113_2304'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='course_title',
            field=models.CharField(default='', max_length=250),
            preserve_default=False,
        ),
    ]
