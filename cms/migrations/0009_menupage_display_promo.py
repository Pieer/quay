# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0008_menupage'),
    ]

    operations = [
        migrations.AddField(
            model_name='menupage',
            name='display_promo',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
