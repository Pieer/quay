# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0039_auto_20141114_1731'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmploymentPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('headline', models.CharField(max_length=255)),
                ('intro_copy', models.CharField(max_length=1000)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('background', models.ForeignKey(to='cms.Background')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Positions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('job_title', models.CharField(max_length=250)),
                ('job_description', models.CharField(max_length=5000)),
                ('active', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='employmentpage',
            name='jobs',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.Positions'),
            preserve_default=True,
        ),
    ]
