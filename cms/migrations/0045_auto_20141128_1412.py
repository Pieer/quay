# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0044_auto_20141128_1215'),
    ]

    operations = [
        migrations.CreateModel(
            name='SpecialContact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('name_function', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255, null=True, blank=True)),
                ('fax', models.CharField(max_length=255, null=True, blank=True)),
                ('email', models.EmailField(max_length=255)),
                ('active', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='contactpage',
            name='contacts',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.SpecialContact'),
            preserve_default=True,
        ),
    ]
