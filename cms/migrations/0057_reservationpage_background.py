# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0056_remove_reservationpage_background'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservationpage',
            name='background',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, to='cms.Background'),
            preserve_default=True,
        ),
    ]
