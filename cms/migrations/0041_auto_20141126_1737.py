# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0040_auto_20141117_1027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='functionspage',
            name='greenroom_carou',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, related_name=b'functionspage_greenroom_carou', to=b'cms.Background'),
        ),
        migrations.AlterField(
            model_name='functionspage',
            name='restaurant_carou',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, related_name=b'functionspage_restaurant_carou', to=b'cms.Background'),
        ),
        migrations.AlterField(
            model_name='functionspage',
            name='uppertower_carou',
            field=sortedm2m.fields.SortedManyToManyField(help_text=None, related_name=b'functionspage_uppertower_carou', to=b'cms.Background'),
        ),
    ]
