# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0049_auto_20141128_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='menupage',
            name='wines_list',
            field=models.FileField(default='', upload_to=b'media/', blank=True),
            preserve_default=False,
        ),
    ]
