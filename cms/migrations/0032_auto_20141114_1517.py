# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0031_auto_20141114_1457'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='newseventspage',
            name='events',
        ),
        migrations.AddField(
            model_name='newsarticle',
            name='reservation_link',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
