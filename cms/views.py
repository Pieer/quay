from cms.models import *
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import *
from pprint import pprint
import json
from quay import settings
from django.core.mail import EmailMultiAlternatives
from createsend import *
import requests

def reservation(request):
  if request.method == 'POST':

    form = ReservationForm(request.POST)

    first_name = request.POST['first_name']
    last_name = request.POST['last_name']

    if form.is_valid():
      e = Reservation()
      e.full_name = form.cleaned_data['full_name']
      e.email_address = form.cleaned_data['email_address']
      e.company = form.cleaned_data['company']
      e.personal_phone = form.cleaned_data['personal_phone']
      e.hotel_phone = form.cleaned_data['hotel_phone']
      e.postcode = form.cleaned_data['postcode']
      e.num_people = form.cleaned_data['num_people']
      e.lunch_dinner = form.cleaned_data['lunch_dinner']
      e.special_requests = form.cleaned_data['special_requests']
      e.optin = form.cleaned_data['optin']
 
      combinedDateTime = form.cleaned_data['date'] + form.cleaned_data['time']
      e.reservation_time = datetime.strptime(combinedDateTime,'%d %B, %Y%H:%M')

      e.save()

      link = "http://" + request.META['HTTP_HOST'] + '/admin/reservations/reservation/' + str(e.id)

      html_body = settings.EMAIL_HTML_BODY % {'reservation_link':link,'reservation_time':e.reservation_time,'full_name':e.full_name, 'email_address':e.email_address, 'company':e.company ,'personal_phone':e.personal_phone ,'hotel_phone':e.hotel_phone,'postcode':e.postcode,'num_people':e.num_people,'special_requests':e.special_requests}
      text_body = settings.EMAIL_HTML_BODY % {'reservation_link':link,'reservation_time':e.reservation_time,'full_name':e.full_name, 'email_address':e.email_address, 'company':e.company ,'personal_phone':e.personal_phone ,'hotel_phone':e.hotel_phone,'postcode':e.postcode,'num_people':e.num_people,'special_requests':e.special_requests}

      email = EmailMultiAlternatives(settings.EMAIL_SUBJECT, text_body, settings.EMAIL_FROM, [settings.EMAIL_TO],[settings.EMAIL_TO_ADMIN],headers={'ReplyTo':e.email_address})
      local_tz = timezone('Australia/Sydney')

      now_sydney= local_tz.localize(e.reservation_time)

      subject_stub = e.full_name + " " + str(now_sydney)

      subject = settings.EMAIL_SUBJECT % {'description': subject_stub}


      email = EmailMultiAlternatives(subject, text_body, settings.EMAIL_FROM, [settings.EMAIL_TO],[settings.EMAIL_TO_ADMIN],headers={'ReplyTo':e.email_address})
      email.attach_alternative(html_body, "text/html")
 
      mail_error = False
      try:
        email.send(fail_silently=False)
        sub = Subscriber({'api_key': '403fe978bf82ea531d01edef9f576523'})
        sub.add('8a9f7433251762b2432b357997fc9329',e.email_address,e.full_name ,'',True,False)

        if (e.optin == True):
            params = {'apikey':'33993a84-3be3-11e5-bf2c-001e67ad826b','PatronName':first_name,'PatronSurname':last_name, 'PatronEmail': e.email_address, 'PatronMobile': e.personal_phone , 'category': '55c9713b5292a'}
            r = requests.post('http://api.myguestlist.com.au/add_db.php', data=params)


      except Exception, e:
        mail_error = True
 

      response_data = {}
      response_data['title'] = 'THANK YOU<br>FOR YOUR ENQUIRY'
      response_data['para'] = 'Your enquiry has been sent. We will reply to your enquiry shortly. We do receive a high volume of reservations requests daily. Please allow at least 48 hours in which to reply to your request.'
 
      return HttpResponse(json.dumps(response_data),content_type="application/json")
 
    else:
      return HttpResponse(json.dumps({"": ""}),content_type="application/json")
  else:
    form = ReservationForm()
 
  view = ReservationPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('reservation.html', {'resa':view, 'openingHours': openingHours(), 'contact': contactInfo(), 'form': form}, context_instance=RequestContext(request))

def subscribe(request):
  if request.method == 'POST':
    email_address = request.POST['email_address']
    params = {'apikey':'33993a84-3be3-11e5-bf2c-001e67ad826b', 'PatronEmail': email_address, 'category': '55c9713b5292a'}
    r = requests.post('http://api.myguestlist.com.au/add_db.php', data=params)
    response_data = {}
    response_data['Message'] = 'You will start receiving the newsletters soon.'
    response_data['Status'] = 200

    return HttpResponse(json.dumps(response_data),content_type="application/json")
  else:
    return HttpResponse(json.dumps({"": ""}),content_type="application/json")

def openingHours():
  return OpeningHours.objects.distinct()[:1][0]

def contactInfo():
  return Contact.objects.distinct()[:1][0]

def home(request):
  view = HomePage.objects.exclude(active=False).distinct()[:1][0]

  return render_to_response('index.html', {'home':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))

def about(request):

  view = AboutPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('about.html', {'view':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))

def menu(request):

  view = MenuPage.objects.exclude(active=False).distinct()[:1][0]
  lunch = Menu.objects.get(category__category='lunch')
  dinner = Menu.objects.get(category__category='dinner')
  tasting = Menu.objects.get(category__category='tasting')

  return render_to_response('menu.html', {'menu':view, 'openingHours': openingHours(), 'contact': contactInfo(), 'lunch':lunch, 'dinner':dinner, 'tasting':tasting}, context_instance=RequestContext(request))

def functions(request):

  view = FunctionsPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('functions.html', {'functions':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))


def contact(request):

  view = ContactPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('contact.html', {'contactpage':view, 'openingHours': openingHours(), 'contact': contactInfo()})


def foodPhilosophy(request):

  view = FoodPhilosophyPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('food-philosophy.html', {'foodPhilosophy':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))


def ourProducers(request):

  view = OurProducersPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('our-producers.html', {'ourProducers':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))


def reviewsMedia(request):

  view = ReviewsMediaPage.objects.exclude(active=False).distinct()[:1][0]
  medias = Media.objects.exclude(active=False)
  print medias
  return render_to_response('reviews-media.html', {'reviewsMedia':view, 'openingHours': openingHours(), 'contact': contactInfo(), 'medias':medias}, context_instance=RequestContext(request))


def awards(request):

  view = AwardPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('awards.html', {'award':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))


def newsEvents(request):

  view = NewsEventsPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('news-events.html', {'newsEvents':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))


def ourTeam(request):

  view = OurTeamPage.objects.exclude(active=False).distinct()[:1][0]
  return render_to_response('our-team.html', {'ourTeam':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))

def employment(request):

  view = EmploymentPage.objects.distinct()[:1][0]
  return render_to_response('employment.html', {'employment':view, 'openingHours': openingHours(), 'contact': contactInfo()}, context_instance=RequestContext(request))

