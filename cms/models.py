# AboutPage
# Award
# AwardPage
# BookImage
# Book
# Background
# ContactPage
# Contact
# Course
# OpeningHours
# Dish
# EmploymentPage
# FoodPhilosophyPage
# FunctionsPage
# HomePage
# Media
# Menu
# MenuCategory
# MenuPage
# NewsArticle
# NewsPage
# NewsEventsPage
# OurProducersPage
# OurTeamPage
# Positions
# Producer
# Reservation
# ReservationForm
# ReviewsMediaPage
# Team


from pytz import timezone
from django import forms
from pprint import pprint
from datetime import datetime
from django.db import models
from django.utils import formats
import string
from django.forms import ModelForm
from django.utils.html import strip_tags
from django.template.defaultfilters import slugify
from sortedm2m.fields import SortedManyToManyField

####################################################################
# Partials
####################################################################

class NewsArticle(models.Model):
  title            = models.CharField(max_length=255)
  description      = models.CharField(max_length=1000)
  date             = models.DateField(auto_now=False, blank=True, null=True)
  image            = models.FileField(upload_to='news/', blank=True)
  published        = models.BooleanField(default=False)
  reservation_link = models.BooleanField(default=False)
  published_at     = models.DateTimeField(auto_now=True)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return self.title + '  -  created: ' + formats.date_format(self.created_at, "SHORT_DATE_FORMAT")

class Award(models.Model):
  title            = models.CharField(max_length=255)
  description      = models.CharField(max_length=1000)
  image            = models.FileField(upload_to='award_images/', blank=True)
  team_image       = models.FileField(upload_to='images/', blank=True)
  created_at       = models.DateTimeField(auto_now=True)

class BookImage(models.Model):
  cssClass         = models.CharField(max_length=255)
  created_at       = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    return unicode(self.cssClass)

class Book(models.Model):
  title            = models.CharField(max_length=255, default='')
  sub_title_line_1 = models.CharField(max_length=255, default='')
  sub_title_line_2 = models.CharField(max_length=255, default='')
  author           = models.CharField(max_length=255, default='')
  sub_author       = models.CharField(max_length=255, default='')
  description      = models.CharField(max_length=1500, default='')
  book_image       = models.ForeignKey(BookImage, null=True)
  link             = models.CharField(max_length=255, default='')
  quote            = models.CharField(max_length=500, default='')
  quote_author     = models.CharField(max_length=255, default='')
  position         = models.IntegerField(blank=False, default=0)
  created_at       = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    return unicode(self.title)

class Background(models.Model):
  cssClass         = models.CharField(max_length=255)
  created_at       = models.DateTimeField(auto_now=True)

  def __unicode__(self):
      return unicode(self.cssClass)

  def image_thumb(self):
    path = 'bg/bg-'
    end = ''
    if any(self.cssClass[:-3] in s for s in ['uppertower','greenroom','restaurant']):
      path = 'rooms/'
    else:
      end = '-375'
    return '<img src="../../../../../static/images/%s%s%s.jpg" width="100" />' % (path,self.cssClass,end)
  image_thumb.allow_tags = True

class Contact(models.Model):
  address_line_1   = models.CharField(max_length=250)
  address_line_2   = models.CharField(max_length=250)
  address_line_3   = models.CharField(max_length=250)
  tel              = models.CharField(max_length=250)
  fax              = models.CharField(max_length=250)
  email            = models.CharField(max_length=250)

class OpeningHours(models.Model):
  lunch_service    = models.CharField(max_length=250)
  dinner_service   = models.CharField(max_length=250)

class Dish(models.Model):
  name             = models.CharField(max_length=1500)
  description      = models.CharField(max_length=1500)
  def __unicode__(self):
    return unicode(self.name)

class Course(models.Model):
  course_title     = models.CharField(max_length=250)
  course_number    = models.CharField(max_length=250)
  dishes           = SortedManyToManyField(Dish)
  def __unicode__(self):
    return unicode(self.course_title)

class MenuCategory(models.Model):
  category         = models.CharField(max_length=250)
  def __unicode__(self):
    return unicode(self.category)

class Positions(models.Model):
  job_title        = models.CharField(max_length=250)
  job_description  = models.CharField(max_length=5000)
  active           = models.BooleanField(default=False)
  def __unicode__(self):
    return unicode(self.job_title)

class Menu(models.Model):
  category         = models.ForeignKey(MenuCategory)
  courses          = SortedManyToManyField(Course)
  prices           = models.CharField(max_length=250)
  additional_info  = models.CharField(max_length=1000, null=True, blank=True)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(self.category.category)
  # def item_list(self):
  #   # return [item.description for item in Menu_item.objects.filter(menu=self).order_by('order')]
  #   return [item.description for item in self.menu_item.order_by('order')]


####################################################################
# Pages
####################################################################

class HomePage(models.Model):
  background       = models.ManyToManyField(Background)
  welcome_copy     = models.CharField(max_length=500)
  menu_copy        = models.CharField(max_length=500)
  menu_copy2       = models.CharField(max_length=500)
  function_copy    = models.CharField(max_length=500)
  about_copy       = models.CharField(max_length=500)
  news_article     = models.ForeignKey(NewsArticle)
  display_promo    = models.BooleanField(default=False)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  # def __unicode__(self):
  #   return unicode([(field.name, field.value_to_string(self)) for field in self._meta.fields])
  def __unicode__(self):
    return 'Home page   -  created: ' + formats.date_format(self.created_at, "SHORT_DATE_FORMAT")
class MenuPage(models.Model):
  background       = models.ForeignKey(Background)
  intro            = models.CharField(max_length=500)
  additional_info  = models.CharField(max_length=1500)
  philosophy       = models.CharField(max_length=500)
  our_produce      = models.CharField(max_length=500)
  display_promo    = models.BooleanField(default=False)
  wines_list       = models.FileField(upload_to='pdf', blank=True)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return 'Menu page   -  created: ' + formats.date_format(self.created_at, "SHORT_DATE_FORMAT")

class AboutPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  news_article     = models.ForeignKey(NewsArticle)
  books            = models.ManyToManyField(Book)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)

  def image_thumb(self):
    path = 'bg/bg-'
    end = ''
    if any(self.background.cssClass[:-3] in s for s in ['uppertower','greenroom','restaurant']):
      path = 'rooms/'
    else:
      end = '-375'
    return '<img src="../../../../../static/images/%s%s%s.jpg" width="100" />' % (path,self.background.cssClass,end)
  image_thumb.allow_tags = True

  def __unicode__(self):
    return 'About page   -  created: ' + formats.date_format(self.created_at, "SHORT_DATE_FORMAT")


class NewsPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  news_articles    = models.ManyToManyField(NewsArticle)
  created_at       = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    return unicode(self.headline)

class EmploymentPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  jobs             = SortedManyToManyField(Positions)
  created_at       = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    return unicode(self.headline)

class SpecialContact(models.Model):
  title         = models.CharField(max_length=255)
  image         = models.ImageField(upload_to='peoples/', blank=True)
  name          = models.CharField(max_length=255)
  function      = models.CharField(max_length=255)
  phone         = models.CharField(max_length=255, blank=True, null=True)
  fax           = models.CharField(max_length=255, blank=True, null=True)
  email         = models.EmailField(max_length=255)
  active        = models.BooleanField(default=False)
  created_at    = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    return unicode(self.title)

class FunctionsPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background, related_name='functionspage_background')

  uppertower_image = models.ForeignKey(Background, related_name='functionspage_uppertower_image')
  uppertower_copy1 = models.CharField(max_length=2000)
  uppertower_copy2 = models.CharField(max_length=2000)
  uppertower_carou = SortedManyToManyField(Background, related_name='functionspage_uppertower_carou')
  

  greenroom_image  = models.ForeignKey(Background, related_name='functionspage_greenroom_image')
  greenroom_copy1  = models.CharField(max_length=2000)
  greenroom_copy2  = models.CharField(max_length=2000)
  greenroom_carou  = SortedManyToManyField(Background, related_name='functionspage_greenroom_carou')

  restaurant_image = models.ForeignKey(Background, related_name='functionspage_restaurant_image')
  restaurant_copy1 = models.CharField(max_length=2000)
  restaurant_copy2 = models.CharField(max_length=2000)
  restaurant_carou = SortedManyToManyField(Background, related_name='functionspage_restaurant_carou')

  wedding_title    = models.CharField(max_length=255)
  wedding_copy     = models.CharField(max_length=1000)
  wedding_video    = models.CharField(max_length=255)
  wedding_background = models.ForeignKey(Background, related_name='functionspage_wedding_background')

  contacts         = SortedManyToManyField(SpecialContact)

  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(self.headline)

class ContactPage(models.Model):
  headline         = models.CharField(max_length=255)
  reservation_copy = models.CharField(max_length=1000)
  gift_voucher_copy= models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  contacts         = SortedManyToManyField(SpecialContact)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(self.headline)

class FoodPhilosophyPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  intro_copy2      = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  nature           = models.CharField(max_length=1000)
  texture          = models.CharField(max_length=1000)
  intensity        = models.CharField(max_length=1000)
  purity           = models.CharField(max_length=1000)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    return unicode(strip_tags(self.headline))

class Producer(models.Model):
  name             = models.CharField(max_length=255)
  description      = models.CharField(max_length=2000)
  background       = models.ForeignKey(Background)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(strip_tags(self.name))

class OurProducersPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  producers        = SortedManyToManyField(Producer)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(strip_tags(self.headline))

class Media(models.Model):
  quote            = models.CharField(max_length=2000)
  editor           = models.CharField(max_length=500)
  pdf              = models.FileField(upload_to='media/', blank=True)
  url              = models.CharField(max_length=500, null=True, blank=True)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(strip_tags(self.editor))

class ReviewsMediaPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  media            = SortedManyToManyField(Media)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(self.headline)

class AwardPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  column_left      = models.CharField(max_length=10000)
  column_right     = models.CharField(max_length=10000)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(self.headline)


class NewsEventsPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=1000)
  background       = models.ForeignKey(Background)
  events           = SortedManyToManyField(NewsArticle)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(self.headline)

class Team(models.Model):
  name             = models.CharField(max_length=255)
  image            = models.ImageField(upload_to='peoples/', blank=True)
  role             = models.CharField(max_length=255)
  small_description= models.CharField(max_length=500)
  description      = models.CharField(max_length=5000)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(self.name)
  def image_thumb(self):
    return '<img src="/media/%s" width="100" />' % (self.image)
  image_thumb.allow_tags = True

class OurTeamPage(models.Model):
  headline         = models.CharField(max_length=255)
  intro_copy       = models.CharField(max_length=10000)
  background       = models.ForeignKey(Background)
  teams            = SortedManyToManyField(Team)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)
  def __unicode__(self):
    return unicode(self.headline)






####################################################################
# Reservation app
####################################################################
class ReservationPage(models.Model):
  background       = SortedManyToManyField(Background)
  intro_copy       = models.CharField(max_length=10000)
  dress_code       = models.CharField(max_length=10000)
  cancelation_policy = models.CharField(max_length=10000)
  credit_card      = models.CharField(max_length=10000)
  active           = models.BooleanField(default=False)
  created_at       = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    return 'Reservation page   -  created: ' + formats.date_format(self.created_at, "SHORT_DATE_FORMAT")

class Reservation(models.Model):
  full_name        = models.CharField(max_length=255)
  email_address    = models.EmailField(max_length=255)
  company          = models.CharField(max_length=255, blank=True, null=True)
  personal_phone   = models.CharField(max_length=255)
  hotel_phone      = models.CharField(max_length=255, blank=True, null=True)
  postcode         = models.CharField(max_length=255)
  reservation_time = models.DateTimeField(auto_now=False, blank=True, null=True)
  num_people       = models.CharField(max_length=255)
  lunch_dinner     = models.CharField(max_length=255)
  special_requests = models.CharField(max_length=1000, blank=True)
  optin            = models.BooleanField(default=False, blank=True)
  created_at       = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    fmt = "%Y%m%d %H:%M:%S %Z%z"
    #local_tz
    now_utc        = self.reservation_time
    now_sydney     = now_utc.astimezone(timezone('Australia/Sydney'))

    return str(self.full_name + " " + str(now_sydney))
 
class ReservationForm(ModelForm):
  full_name        = forms.CharField( widget=forms.TextInput(attrs={'placeholder': 'Eg. John Smith'}))
  email_address    = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Eg. john@email.com'}))
  company          = forms.CharField( widget=forms.TextInput(attrs={'placeholder': 'Eg. Quay Restaurant'}), required=False)
  personal_phone   = forms.CharField( widget=forms.TextInput(attrs={'placeholder': 'Eg. 02 9251 5600'}))
  hotel_phone      = forms.CharField( widget=forms.TextInput(attrs={'placeholder': 'Eg. 02 9251 5600'}), required=False)
  postcode         = forms.CharField( widget=forms.TextInput(attrs={'placeholder': 'Eg. 2000'}))
  date             = forms.CharField( widget=forms.DateInput(attrs={'placeholder': 'Eg. 2000'}))
  time             = forms.CharField( widget=forms.Select(   attrs={'placeholder': 'Select'}))
  lunch_dinner     = forms.CharField( widget=forms.DateInput(attrs={'placeholder': 'Eg. 2000'}), required=False)
  num_people       = forms.CharField( widget=forms.TextInput(attrs={'placeholder': 'Select'}))
  special_requests = forms.CharField( widget=forms.TextInput(attrs={'placeholder': 'Eg. I have an allergy to peanuts.'}), required=False)
  optin            = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
 
 
  class Meta:
    fields = ['full_name',
              'email_address',
              'company',
              'personal_phone',
              'hotel_phone',
              'postcode',
              'date',
              'time',
              'num_people',
              'lunch_dinner',
              'special_requests',
              'optin']
    model = Reservation
