from django.contrib import admin
from cms.models import *
from django.forms import ModelForm
from suit_redactor.widgets import RedactorWidget

class ReservationAdmin(admin.ModelAdmin):
  pass
admin.site.register( Reservation, ReservationAdmin)

class NewsArticleFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'description': RedactorWidget(editor_options={'lang': 'en'}),
    }
  
class NewsArticleAdmin(admin.ModelAdmin):
  form = NewsArticleFormAdmin
  fieldsets = [
    ('', {'fields': ('title'           ,)}),
    ('', {'fields': ('description'     ,)}),
    ('', {'fields': ('date'            ,)}),
    ('', {'fields': ('image'           ,)}),
    ('', {'fields': ('published'       ,)}),
    ('', {'fields': ('reservation_link',)}),
  ]
admin.site.register( NewsArticle, NewsArticleAdmin)


class AwardAdmin(admin.ModelAdmin):
  pass
admin.site.register( Award, AwardAdmin)


class BookFormAdmin(ModelForm):
  class Meta:
    widgets = { 'description': RedactorWidget(editor_options={'lang': 'en'}) }

class BookAdmin(admin.ModelAdmin):
  form = BookFormAdmin
  fieldsets = [
    ('', {'fields': ('title'           ,)}),
    ('', {'fields': ('sub_title_line_1',)}),
    ('', {'fields': ('sub_title_line_2',)}),
    ('', {'fields': ('author'          ,)}),
    ('', {'fields': ('sub_author'      ,)}),
    ('', {'fields': ('book_image'      ,)}),
    ('', {'fields': ('position'        ,)}),
    ('', {'fields': ('description'     ,)}),
    ('', {'fields': ('quote'           ,)}),
    ('', {'fields': ('quote_author'    ,)}),
    ('', {'fields': ('link'            ,)}),
  ]

admin.site.register( Book, BookAdmin)


class BookImageAdmin(admin.ModelAdmin):
  pass
admin.site.register( BookImage, BookImageAdmin)


class BackgroundAdmin(admin.ModelAdmin):
  fieldsets = [
    ('', {'fields': ('cssClass'   ,)}),
    ('', {'fields': ('image_thumb',)})
  ]
  list_display = ('cssClass','image_thumb')
  readonly_fields = ['image_thumb']
admin.site.register( Background, BackgroundAdmin)

class HomePageFormAdmin(ModelForm):
  class Meta:
    widgets = { 'welcome_copy': RedactorWidget(editor_options={'lang': 'en'}) }

class HomePageAdmin(admin.ModelAdmin):
  form = HomePageFormAdmin
  fieldsets = [
    ('', {'fields': ('background'   ,)}),
    ('', {'fields': ('welcome_copy' ,)}),
    ('', {'fields': ('menu_copy'    ,)}),
    ('', {'fields': ('menu_copy2'   ,)}),
    ('', {'fields': ('function_copy',)}),
    ('', {'fields': ('about_copy'   ,)}),
    ('', {'fields': ('news_article' ,)}),
    ('', {'fields': ('display_promo',)}),
    ('', {'fields': ('active'       ,)}),
  ]
admin.site.register( HomePage, HomePageAdmin)

class ReservationAdmin(ModelForm):
  class Meta:
    widgets = { 
      'intro_copy': RedactorWidget(editor_options={'lang': 'en'}),
      'dress_code': RedactorWidget(editor_options={'lang': 'en'}),
      'cancelation_policy': RedactorWidget(editor_options={'lang': 'en'}),
      'credit_card': RedactorWidget(editor_options={'lang': 'en'}),
    }

class ReservationPageAdmin(admin.ModelAdmin):
  form = ReservationAdmin
  fieldsets = [
    ('', {'fields': ('background'         ,)}),
    ('', {'fields': ('intro_copy'         ,)}),
    ('', {'fields': ('dress_code'         ,)}),
    ('', {'fields': ('cancelation_policy' ,)}),
    ('', {'fields': ('credit_card'        ,)}),
    ('', {'fields': ('active'             ,)}),
  ]
admin.site.register( ReservationPage, ReservationPageAdmin)

# class MenuCategoryAdmin(admin.ModelAdmin):
#   pass
# admin.site.register(MenuCategory, MenuCategoryAdmin)

class DishFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'description': RedactorWidget(editor_options={'lang': 'en'}),
    }

class DishAdmin(admin.ModelAdmin):
  form = DishFormAdmin
  fieldsets = [
    ('', {'fields': ('name'       ,)}),
    ('', {'fields': ('description',)}),
  ]
admin.site.register(Dish, DishAdmin)

class CourseAdmin(admin.ModelAdmin):
  pass
admin.site.register(Course, CourseAdmin)

class MenuFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'prices': RedactorWidget(editor_options={'lang': 'en'}),
      'additional_info': RedactorWidget(editor_options={'lang': 'en'}),
    }


class MenuAdmin(admin.ModelAdmin): 
  form = MenuFormAdmin
  fieldsets = [
    ('', {'fields': ('category'       ,)}),
    ('', {'fields': ('courses'        ,)}),
    ('', {'fields': ('active'         ,)}),
    ('', {'fields': ('prices'         ,)}),
    ('', {'fields': ('additional_info',)}),
  ]

admin.site.register(Menu, MenuAdmin)


class PositionsFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'job_title': RedactorWidget(editor_options={'lang': 'en'}),
      'job_description': RedactorWidget(editor_options={'lang': 'en'}),
    }

class PositionsPageAdmin(admin.ModelAdmin): 
  form = PositionsFormAdmin
  fieldsets = [
    ('', {'fields': ('job_title'      ,)}),
    ('', {'fields': ('job_description',)}),
    ('', {'fields': ('active'         ,)}),
  ]

admin.site.register(Positions, PositionsPageAdmin)

class EmploymentFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'intro_copy': RedactorWidget(editor_options={'lang': 'en'}),
    }

class EmploymentPageAdmin(admin.ModelAdmin): 
  form = EmploymentFormAdmin
  fieldsets = [
    ('', {'fields': ('headline'  ,)}),
    ('', {'fields': ('intro_copy',)}),
    ('', {'fields': ('background',)}),
    ('', {'fields': ('jobs'      ,)}),    
  ]

admin.site.register(EmploymentPage, EmploymentPageAdmin)




class MenuPageFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'additional_info': RedactorWidget(editor_options={'lang': 'en'}),
      'our_produce': RedactorWidget(editor_options={'lang': 'en'}),
    }

class MenuPageAdmin(admin.ModelAdmin):
  form = MenuPageFormAdmin
  fieldsets = [
    ('', {'fields': ('background'      ,)}),
    ('', {'fields': ('intro'           ,)}),
    ('', {'fields': ('wines_list'      ,)}),
    ('', {'fields': ('additional_info' ,)}),
    ('', {'fields': ('philosophy'      ,)}),
    ('', {'fields': ('our_produce'     ,)}),
    ('', {'fields': ('active'          ,)}),
  ]
admin.site.register( MenuPage, MenuPageAdmin)

class AboutPageAdmin(admin.ModelAdmin):
  fieldsets = [
    ('', {'fields': ('headline'    ,)}),
    ('', {'fields': ('intro_copy'  ,)}),
    ('', {'fields': ('background'  ,)}),
    ('', {'fields': ('image_thumb' ,)}),
    ('', {'fields': ('news_article',)}),
    ('', {'fields': ('books'       ,)}),
    ('', {'fields': ('active'      ,)}),
  ]  
  readonly_fields = ['image_thumb']
  
admin.site.register( AboutPage, AboutPageAdmin)


class ContactAdmin(admin.ModelAdmin):
  pass
admin.site.register( Contact, ContactAdmin)

class OpeningHoursAdmin(admin.ModelAdmin):
  pass
admin.site.register( OpeningHours, OpeningHoursAdmin)

class SpecialContactAdmin(admin.ModelAdmin):
  pass
admin.site.register( SpecialContact, SpecialContactAdmin)

class ContactPageFormAdmin(ModelForm):
  class Meta:
    widgets = {
      'reservation_copy': RedactorWidget(editor_options={'lang': 'en'}),
      'gift_voucher_copy': RedactorWidget(editor_options={'lang': 'en'}),
    }

class ContactPageAdmin(admin.ModelAdmin):
  form = ContactPageFormAdmin
  fieldsets = [
    ('', {'fields': ('headline'       ,)}),
    ('', {'fields': ('reservation_copy',)}),
    ('', {'fields': ('gift_voucher_copy' ,)}),
    ('', {'fields': ('background' ,)}),
    ('', {'fields': ('contacts' ,)}),
    ('', {'fields': ('active'     ,)}),
  ]
admin.site.register(ContactPage, ContactPageAdmin)

class ProducerFormAdmin(ModelForm):
  class Meta:
    widgets = {
      'name': RedactorWidget(editor_options={'lang': 'en'}),
      'description': RedactorWidget(editor_options={'lang': 'en'}),
    }

class ProducerAdmin(admin.ModelAdmin):
  form = ProducerFormAdmin
  fieldsets = [
    ('', {'fields': ('name'       ,)}),
    ('', {'fields': ('description',)}),
    ('', {'fields': ('background' ,)}),
    ('', {'fields': ('active'     ,)}),
  ]
admin.site.register( Producer, ProducerAdmin)

class OurProducersPageFormAdmin(ModelForm):
  class Meta:
    widgets = {
      'headline': RedactorWidget(editor_options={'lang': 'en'}),
    }

class OurProducersPageAdmin(admin.ModelAdmin):
  form = OurProducersPageFormAdmin
  fieldsets = [
    ('', {'fields': ('headline'  ,)}),
    ('', {'fields': ('intro_copy',)}),
    ('', {'fields': ('background',)}),
    ('', {'fields': ('producers' ,)}),
    ('', {'fields': ('active'    ,)}),
  ]
admin.site.register( OurProducersPage, OurProducersPageAdmin)

class MediaFormAdmin(ModelForm):
  class Meta:
    widgets = {
      'quote': RedactorWidget(editor_options={'lang': 'en'}),
    }

class MediaAdmin(admin.ModelAdmin):
  form = MediaFormAdmin
  fieldsets = [
    ('', {'fields': ('quote' ,)}),
    ('', {'fields': ('editor',)}),
    ('', {'fields': ('active',)}),
    ('', {'fields': ('pdf'   ,)}),
    ('', {'fields': ('url'   ,)}),
  ]
admin.site.register( Media, MediaAdmin)

class ReviewsMediaPageAdmin(admin.ModelAdmin):
  pass
admin.site.register( ReviewsMediaPage, ReviewsMediaPageAdmin)



class FunctionsPageFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'intro_copy':       RedactorWidget(editor_options={'lang': 'en'}),
      'uppertower_copy1': RedactorWidget(editor_options={'lang': 'en'}),
      'uppertower_copy2': RedactorWidget(editor_options={'lang': 'en'}),
      'greenroom_copy1':  RedactorWidget(editor_options={'lang': 'en'}),
      'greenroom_copy2':  RedactorWidget(editor_options={'lang': 'en'}),
      'restaurant_copy1': RedactorWidget(editor_options={'lang': 'en'}),
      'restaurant_copy2': RedactorWidget(editor_options={'lang': 'en'}),
      'wedding_copy':     RedactorWidget(editor_options={'lang': 'en'}),
    }

class FunctionsPageAdmin(admin.ModelAdmin):
  form = FunctionsPageFormAdmin
  fieldsets = [
    ('', {'fields': ('headline'           ,)}),
    ('', {'fields': ('intro_copy'         ,)}),
    ('', {'fields': ('background'         ,)}),
    ('', {'fields': ('uppertower_image'   ,)}),
    ('', {'fields': ('uppertower_copy1'   ,)}),
    ('', {'fields': ('uppertower_copy2'   ,)}),
    ('', {'fields': ('uppertower_carou'   ,)}),
    ('', {'fields': ('greenroom_image'    ,)}),
    ('', {'fields': ('greenroom_copy1'    ,)}),
    ('', {'fields': ('greenroom_copy2'    ,)}),
    ('', {'fields': ('greenroom_carou'    ,)}),
    ('', {'fields': ('restaurant_image'   ,)}),
    ('', {'fields': ('restaurant_copy1'   ,)}),
    ('', {'fields': ('restaurant_copy2'   ,)}),
    ('', {'fields': ('restaurant_carou'   ,)}),
    ('', {'fields': ('wedding_title'      ,)}),
    ('', {'fields': ('wedding_copy'       ,)}),
    ('', {'fields': ('wedding_video'      ,)}),
    ('', {'fields': ('wedding_background' ,)}),
    ('', {'fields': ('contacts' ,)}),
    ('', {'fields': ('active'             ,)}),
  ]
  pass
admin.site.register(FunctionsPage, FunctionsPageAdmin)

class FoodPhilosophyPageFormAdmin(ModelForm):
  class Meta:
    widgets = {
      'headline': RedactorWidget(editor_options={'lang': 'en'}),
      'intro_copy2': RedactorWidget(editor_options={'lang': 'en'}),
    }

class FoodPhilosophyPageAdmin(admin.ModelAdmin):
  form = FoodPhilosophyPageFormAdmin
  fieldsets = [
    ('', {'fields': ('headline'   ,)}),
    ('', {'fields': ('intro_copy' ,)}),
    ('', {'fields': ('intro_copy2',)}),
    ('', {'fields': ('background' ,)}),
    ('', {'fields': ('nature'     ,)}),
    ('', {'fields': ('texture'    ,)}),
    ('', {'fields': ('intensity'  ,)}),
    ('', {'fields': ('purity'     ,)}),
    ('', {'fields': ('active'     ,)}),
  ]
admin.site.register(FoodPhilosophyPage, FoodPhilosophyPageAdmin)

class AwardPageFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'column_left': RedactorWidget(editor_options={'lang': 'en'}),
      'column_right': RedactorWidget(editor_options={'lang': 'en'}),
    }

class AwardPageAdmin(admin.ModelAdmin):
  form = AwardPageFormAdmin
  fieldsets = [
    ('', {'fields': ('headline'    ,)}),
    ('', {'fields': ('intro_copy'  ,)}),
    ('', {'fields': ('background'  ,)}),
    ('', {'fields': ('column_left' ,)}),
    ('', {'fields': ('column_right',)}),
    ('', {'fields': ('active'      ,)}),
  ]
admin.site.register(AwardPage, AwardPageAdmin)

class NewsEventsPageAdmin(admin.ModelAdmin):
  pass
admin.site.register(NewsEventsPage, NewsEventsPageAdmin)

class TeamFormAdmin(ModelForm):
  class Meta:
    widgets = { 
      'description': RedactorWidget(editor_options={'lang': 'en'}),
    }

class TeamAdmin(admin.ModelAdmin):
  form = TeamFormAdmin
  fieldsets = [
    ('', {'fields': ('name'             ,)}),
    ('', {'fields': ('image'            ,)}),
    ('', {'fields': ('image_thumb'      ,)}), 
    ('', {'fields': ('role'             ,)}),
    ('', {'fields': ('small_description',)}),
    ('', {'fields': ('description'      ,)}),
    ('', {'fields': ('active'           ,)}),
  ]
  list_display = ('name','image_thumb')
  readonly_fields = ['image_thumb']
admin.site.register(Team, TeamAdmin)


class OurTeamPageAdmin(admin.ModelAdmin):
  pass
admin.site.register(OurTeamPage, OurTeamPageAdmin)

