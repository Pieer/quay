"use strict"

# generated on 2014-08-15 using generator-gulp-webapp 0.1.0
gulp = require("gulp")
util = require("gulp-util")

# load plugins
$ = require("gulp-load-plugins")()
onError = (err) ->
  util.beep()
  console.log err
  @emit "end"
  return

gulp.task "styles", ->
  gulp.src("app/styles/**/*.sass")
    .pipe($.plumber(errorHandler: onError))
    .pipe($.compass(
      css: "dist/styles"
      sass: "app/styles"
      image: "app/images"
    ))
    .pipe($.autoprefixer('last 2 versions', 'ie 9'))
    .pipe($.plumber.stop())
    .pipe(gulp.dest("dist/styles"))
    .pipe $.size()

gulp.task "scripts", ->
  gulp.src("app/scripts/**/*.js")
    .pipe($.jshint(
      camelcase: false # python default input id doesnt use camelcase
      eqnull: true # coffeescript take care of it
    ))
    .pipe($.jshint.reporter(require("jshint-stylish")))
    .pipe $.size()

gulp.task "templates", ->
  gulp.src([
      "app/views/*.jade"
      "app/views/work/*.jade"
    ])
    .pipe($.jade(pretty: true))
    .pipe gulp.dest("templates/")
  return

gulp.task "coffee", ->
  gulp.src("app/coffee/*.coffee")
    .pipe($.plumber(errorHandler: onError))
    .pipe($.coffee(bare: true).on("error", util.log))
    .pipe gulp.dest("dist/scripts/")
  return

gulp.task "vendor", ->
  gulp.src(
    [
      'app/bower_components/modernizr/modernizr.js'
      'app/bower_components/jquery/dist/jquery.min.js'
      'app/bower_components/fastclick/lib/fastclick.js'
      'app/bower_components/pickadate/lib/picker.js'
      'app/bower_components/pickadate/lib/picker.date.js'
      'app/bower_components/fitvids/jquery.fitvids.js'
      'dist/scripts/selectordie.js'
      'dist/scripts/raf.js'
      'dist/scripts/router.js'
      'dist/scripts/prettyembed.js'
      'dist/scripts/popup.js'
      'dist/scripts/carousel.js'
      'dist/scripts/menu.js'
      'dist/scripts/nav.js'
      'dist/scripts/collapse.js'
      'dist/scripts/map.js'
      'dist/scripts/main.js'
      'dist/scripts/reservation.js'
    ]
  )
  .pipe($.concat('scripts.js'))
  .pipe($.uglify())
  .pipe(gulp.dest("dist/scripts"))
  return

gulp.task "html", [
  "styles"
  "scripts"
], ->
  jsFilter = $.filter("**/*.js")
  cssFilter = $.filter("**/*.css")
  gulp.src("app/*.html")
    .pipe($.useref.assets(searchPath: "{.tmp,app}"))
    .pipe(jsFilter)
    .pipe($.uglify())
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe($.useref.restore())
    .pipe($.useref())
    .pipe(gulp.dest("dist"))
    .pipe $.size()

gulp.task "images", ->
  # gulp.src("app/images/**/*", {base: 'app/images'})
  #   .pipe($.filter('**/*.{jpg,jpeg,svg,gif,png}'))
  #   .pipe($.cache($.imagemin(
  #     optimizationLevel: 3
  #     progressive: true
  #     interlaced: true
  #   )))
  #   .pipe(gulp.dest("dist/images"))
  #   .pipe $.size()
  gulp.src("app/images/**/*")
    .pipe(gulp.dest("dist/images"))

gulp.task "clear", (done) ->
  $.cache.clearAll done

gulp.task "fonts", ->
  gulp.src("app/fonts/*")
    .pipe($.flatten())
    .pipe(gulp.dest("dist/fonts"))
    .pipe $.size()

gulp.task "extras", ->
  gulp.src([
    "app/*.*"
    "!app/*.html"
    "app/annexes/**/*"
  ],
    dot: true
  ).pipe gulp.dest("dist")

gulp.task "copyJS", ->
    gulp.src("app/scripts/*.js")
        .pipe gulp.dest("dist/scripts")

gulp.task "clean", ->
  gulp.src([
    ".tmp"
    "dist"
  ],
    read: false
  ).pipe $.clean()

gulp.task "build", [
  "html"
  "images"
  "fonts"
  "extras"
]
gulp.task "quick", [
  "html"
  "extras"
]
gulp.task "default", ["clean"], ->
  gulp.start "build"
  return

gulp.task "connect", ->
  connect = require("connect")
  app = connect()
    .use(require("connect-livereload")(port: 35729))
    .use(connect.static("app"))
    .use(connect.static(".tmp"))
    .use(connect.directory("app"))
  require("http").createServer(app).listen(9000).on "listening", ->
    console.log "Started connect web server on http://localhost:9000"
    return

  return

gulp.task "serve", [
  "connect"
  "styles"
  "coffee"
], ->
  require("opn") "http://localhost:9000"
  return


# inject bower components
gulp.task "wiredep", ->
  wiredep = require("wiredep").stream
  gulp.src("app/styles/*.sass")
    .pipe(wiredep(directory: "app/bower_components"))
    .pipe gulp.dest("app/styles")
  gulp.src("app/*.html")
    .pipe(wiredep(directory: "app/bower_components"))
    .pipe gulp.dest("app")
  return

gulp.task "watch", [
  "connect"
  "serve"
], ->
  server = $.livereload()

  # watch for changes
  gulp.watch([
    "app/*.html"
    ".tmp/styles/**/*.css"
    "app/scripts/**/*.js"
    "app/images/**/*"
  ]).on "change", (file) ->
    server.changed file.path
    return

  gulp.watch "app/views/**/*.jade", ["templates"]
  gulp.watch "app/styles/**/*.sass", ["styles"]
  gulp.watch "app/coffee/**/*.coffee", ["coffee","vendor"]
  gulp.watch "app/images/**/*", ["images"]
  gulp.watch "bower.json", ["wiredep"]
  return
