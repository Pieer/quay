from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

# Django settings for quay project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'quay',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts

ALLOWED_HOSTS = ['www.quay.com.au','quay.com.au','quay-live.pollen.com.au']



EMAIL_HOST = 'email-smtp.us-west-2.amazonaws.com'
EMAIL_PORT = '587'
EMAIL_HOST_USER = 'AKIAIETD5GBSDOSQLT7Q'
EMAIL_HOST_PASSWORD = 'AlIBfQv3BQOgaoid8rvBUNbzcS+VNa02G77ywyeu1R5J'
EMAIL_USE_TLS = True

EMAIL_TO = "reservations@quay.com.au"
EMAIL_TO_ADMIN = ""

EMAIL_SUBJECT = "Reservation: %(description)s"
EMAIL_HTML_BODY = "<h1>New Reservation Enquiry</h1>" \
                  "<p>Full name: %(full_name)s</p>" \
                  "<p>Email: %(email_address)s</p>" \
                  "<p>Company (if provided): %(company)s</p>" \
                  "<p>Personal Phone: %(personal_phone)s</p>" \
                  "<p>Hotel/Local Phone (if provided): %(hotel_phone)s</p>" \
                  "<p>Postal Code / Zip: %(postcode)s</p>" \
                  "<h2>Date / Time</h2>" \
                  "<p>Reservation Date / Time: %(reservation_time)s</p>" \
                  "<p>Number of people: %(num_people)s</p><br /><br /><br />" \
                  "<p>Special Requests: %(special_requests)s</p><br /><br /><br />" \
                  "<p>View Reservation: <a href='%(reservation_link)s'>Edit</a></p>" \



EMAIL_TEXT_BODY = "New Reservation Enquiry" \
                  "" \
                  "Full name: %(full_name)s" \
                  "Email: %(email_address)s" \
                  "Company (if provided): %(company)s" \
                  "Personal Phone: %(personal_phone)s" \
                  "Hotel/Local Phone (if provided): %(hotel_phone)s" \
                  "Postal Code: %(postcode)s" \
                  "" \
                  "Date / Time" \
                  "" \
                  "Number of people: %(num_people)s" \
                  "Special Requests: %(special_requests)s" \
                  "" \
                  "View Reservation: %(reservation_link)s"


EMAIL_FROM = "info@petergilmore.com.au"


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Australia/Sydney'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True


SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',

    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'PAGINATE_BY': 10,
    'DATETIME_FORMAT': '%d-%m-%Y',
}

SUIT_CONFIG = {
    'ADMIN_NAME': 'Quay',
    'MENU_OPEN_FIRST_CHILD': True,
    # 'MENU_ORDER': (

    #   ('Pages', '/admin/cms/', 'icon-cog'),
    #   (
    #       ('Home','/admin/cms/homepage/1/'),
    #       ('About','/admin/cms/aboutpage/1/'),
    #       ('Menu','/admin/cms/menupage/1/'),
    #       ('Functions','/admin/cms/functionspage/1/'),
    #       ('Award','/admin/cms/awardpage/1/'),
    #       ('Food philosophy','/admin/cms/foodphilosophypage/1/'),
    #       ('Producers','/admin/cms/ourproducerspage/1/'),
    #       ('News - Events','/admin/cms/newseventspage/4/'),
    #       ('Team','/admin/cms/ourteampage/1/'),
    #   )
    # ),
    'SEARCH_URL': '',
    'MENU': (
        {'label': 'Pages', 'icon': 'icon-leaf','models': [
            {'label': 'Home', 'url': '/admin/cms/homepage/1/'},
            {'label': 'About', 'url': '/admin/cms/aboutpage/1/'},
            {'label': 'Menu', 'url': '/admin/cms/menupage/1/'},
            {'label': 'Functions', 'url': '/admin/cms/functionspage/1/'},
            {'label': 'Award', 'url': '/admin/cms/awardpage/1/'},
            {'label': 'Food philosophy', 'url': '/admin/cms/foodphilosophypage/1/'},
            {'label': 'Producers', 'url': '/admin/cms/ourproducerspage/1/'},
            {'label': 'News - Events','icon': 'icon-question-sign', 'url':  '/admin/cms/newseventspage/4/'},
            {'label': 'Employment', 'url': '/admin/cms/employmentpage/1'},
            {'label': 'Reviews - media','url': '/admin/cms/reviewsmediapage/1/'},
            {'label': 'Team', 'url': '/admin/cms/ourteampage/1/'},
            {'label': 'Contact', 'url': '/admin/cms/contactpage/1/'},
            {'label': 'Reservation', 'url': '/admin/cms/reservationpage/1/'},
        ]},
        {'label': 'Book', 'icon': 'icon-book','models': [
            {'label': 'Book List', 'url': '/admin/cms/book/'},
            {'label': 'Add book', 'url': '/admin/cms/book/add/'},
            {'label': 'Add book images', 'url': '/admin/cms/bookimage/'},
        ]},
        {'label': 'Producers', 'icon': 'icon-user','models': [
            {'label': 'Producers List', 'url': '/admin/cms/producer/'},
            {'label': 'Add a producer', 'url': '/admin/cms/producer/add/'},
        ]},
        {'label': 'Background', 'icon': 'icon-picture','models': [
            {'label': 'Background List', 'url': '/admin/cms/background/'},
            {'label': 'Add background image', 'url': '/admin/cms/background/add/'},
        ]},
        {'label': 'Menu', 'icon': 'icon-align-justify','models': [
            {'label': 'Menu list', 'url': '/admin/cms/menu/'},
            {'label': 'Course List', 'url': '/admin/cms/course/'},
            {'label': 'Add Course', 'url': '/admin/cms/course/add/'},
            {'label': 'Dish list', 'url': '/admin/cms/dish/'},
            {'label': 'Add dish', 'url': '/admin/cms/dish/add/'},
        ]},
        {'label': 'Team', 'icon': 'icon-user','models': [
            {'label': 'Team List', 'url': '/admin/cms/team/'},
            {'label': 'Add team member', 'url': '/admin/cms/team/add/'},
        ]},
        {'label': 'News - Events', 'icon': 'icon-calendar','models': [
            {'label': 'News/Events List', 'url': '/admin/cms/newsarticle/'},
            {'label': 'Add News/Event', 'url': '/admin/cms/newsarticle/add/'},
        ]},
        
        {'label': 'Reviews - media', 'icon': 'icon-heart','models': [
            {'label': 'Reviews list', 'icon': 'icon-time','url': '/admin/cms/media/'},
            {'label': 'Add a review', 'icon': 'icon-time','url': '/admin/cms/media/add'},
        ]},
        {'label': 'Opening Hours', 'icon': 'icon-time','url': '/admin/cms/openinghours/1'},
        
        {'label': 'Contacts', 'icon': 'icon-map-marker','models': [
            {'label': 'General contact','url': '/admin/cms/contact/1'},
            {'label': 'Specific contacts', 'url': '/admin/cms/specialcontact/'},
        ]},
        {'label': 'Jobs', 'icon': 'icon-bullhorn','models': [
            {'label': 'Job List', 'url': '/admin/cms/positions/'},
            {'label': 'Add a job', 'url': '/admin/cms/positions/add/'},
        ]},
        
    ),
}


# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT =  os.path.join(BASE_DIR, "media/")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'


# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = BASE_DIR + '/static/'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
   BASE_DIR + '/dist/',

)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'v%=6)3myh%wuq_!4d+=7r!umcf7&2u8_pt6@sw1cg2j(g*dxf7'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # ('pyjade.ext.django.Loader',(
    # )),
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'quay.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'quay.wsgi.application'

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)
TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    BASE_DIR + '/templates',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'suit',
    'suit_redactor',
    'sortedm2m',
    'django.contrib.admin',
    'rest_framework',
    'cms',

    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
