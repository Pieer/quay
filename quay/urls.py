from django.conf.urls import patterns, include, url
from rest_framework import viewsets, routers
from cms.models import *
from cms.serializers import *
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()


reservation_info = { 'queryset': Reservation.objects.all() }


class ReservationViewSet(viewsets.ModelViewSet):
    model = Reservation
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer

router = routers.DefaultRouter()
router.register(r'cms', ReservationViewSet)


from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'quay.views.home', name='home'),
    # url(r'^quay/', include('quay.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$',               'cms.views.home',            name='home'),
    url(r'^menu',            'cms.views.menu',            name='menu'),
    url(r'^about',           'cms.views.about',           name='about'),
    url(r'^functions',       'cms.views.functions',       name='functions'),
    url(r'^contact',         'cms.views.contact',         name='contact'),
    url(r'^food-philosophy', 'cms.views.foodPhilosophy',  name='foodPhilosophy'),
    url(r'^our-producers',   'cms.views.ourProducers',    name='ourProducers'),
    url(r'^reviews-media',   'cms.views.reviewsMedia',    name='reviewsMedia'),
    url(r'^awards',          'cms.views.awards',          name='awards'),
    url(r'^news-events',     'cms.views.newsEvents',      name='newsEvents'),
    url(r'^our-team',        'cms.views.ourTeam',         name='ourTeam'),
    url(r'^employment',      'cms.views.employment',      name='employment'),
    url(r'^reservation/',    'cms.views.reservation',     name='reservation'),
    url(r'^subscribe/',      'cms.views.subscribe',       name='subscribe'),
    url(r'^media/(?P<path>.*)$','django.views.static.serve', {'document_root': 'media/'}),
    url(r'^api/', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

)
