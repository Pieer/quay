from reservations.models import *
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import *
from pprint import pprint
import json
from quay import  settings
from django.core.mail import EmailMultiAlternatives
from createsend import *

def reservation(request):
     if request.method == 'POST':
            form = ReservationForm(request.POST)

            if form.is_valid():
                e = Reservation()

                e.full_name =  form.cleaned_data['full_name']
                e.email_address =  form.cleaned_data['email_address']
                e.company = form.cleaned_data['company']
                e.personal_phone =  form.cleaned_data['personal_phone']
                e.hotel_phone =  form.cleaned_data['hotel_phone']
                e.postcode =  form.cleaned_data['postcode']
                e.num_people =  form.cleaned_data['num_people']
                e.lunch_dinner = form.cleaned_data['lunch_dinner']
                e.special_requests = form.cleaned_data['special_requests']
                e.optin = form.cleaned_data['optin']

                combinedDateTime =  form.cleaned_data['date'] + form.cleaned_data['time']
                e.reservation_time = datetime.strptime(combinedDateTime,'%d %B, %Y%H:%M')

                e.save()

                link = "http://"+request.META['HTTP_HOST']+ '/admin/reservations/reservation/' + str(e.id)


                html_body = settings.EMAIL_HTML_BODY % {'reservation_link':link,'reservation_time':e.reservation_time,'full_name':e.full_name, 'email_address':e.email_address, 'company':e.company ,'personal_phone':e.personal_phone ,'hotel_phone':e.hotel_phone,'postcode':e.postcode,'num_people':e.num_people,'special_requests':e.special_requests}
                text_body = settings.EMAIL_HTML_BODY % {'reservation_link':link,'reservation_time':e.reservation_time,'full_name':e.full_name, 'email_address':e.email_address, 'company':e.company ,'personal_phone':e.personal_phone ,'hotel_phone':e.hotel_phone,'postcode':e.postcode,'num_people':e.num_people,'special_requests':e.special_requests}

                local_tz = timezone('Australia/Sydney')

                now_sydney= local_tz.localize(e.reservation_time)
                fmt = "%Y-%m-%d"
                subject_stub = e.full_name + " " +  now_sydney.strftime(fmt)
                subject = settings.EMAIL_SUBJECT % {'description': subject_stub}

                email = EmailMultiAlternatives(subject,  text_body, settings.EMAIL_FROM, [settings.EMAIL_TO],[settings.EMAIL_TO_ADMIN],headers={'Reply-To':e.email_address})
                email.attach_alternative(html_body, "text/html")

                mail_error = False

                try:
                    email.send(fail_silently=False)
                    sub = Subscriber({'api_key': '403fe978bf82ea531d01edef9f576523'})
                    sub.add('8a9f7433251762b2432b357997fc9329',e.email_address,e.full_name ,'',True,False)

                    if (e.optin == True):
                      sub.add('acc1ede6a129663ee85063617682554e',e.email_address,e.full_name ,'',True,False)


                except Exception, e:
                    mail_error = True


                response_data = {}
                response_data['title'] = 'THANK YOU<br>FOR YOUR ENQUIRY'
                response_data['para'] = 'Your enquiry has been sent. We will reply to your enquiry shortly. We do receive a high volume of reservations requests daily. Please allow at least 48 hours in which to reply to your request.'

                return HttpResponse(json.dumps(response_data),content_type="application/json")

            else:

                return HttpResponse(json.dumps({"": ""}),content_type="application/json")
     else:
            form = ReservationForm()


     return render_to_response('reservation.html', {'form': form,}, context_instance=RequestContext(request))
