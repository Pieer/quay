-- MySQL dump 10.13  Distrib 5.6.21, for osx10.8 (x86_64)
--
-- Host: localhost    Database: quay
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add log entry',7,'add_logentry'),(20,'Can change log entry',7,'change_logentry'),(21,'Can delete log entry',7,'delete_logentry'),(22,'Can add news article',8,'add_newsarticle'),(23,'Can change news article',8,'change_newsarticle'),(24,'Can delete news article',8,'delete_newsarticle'),(25,'Can add award',9,'add_award'),(26,'Can change award',9,'change_award'),(27,'Can delete award',9,'delete_award'),(28,'Can add book',10,'add_book'),(29,'Can change book',10,'change_book'),(30,'Can delete book',10,'delete_book'),(31,'Can add background',11,'add_background'),(32,'Can change background',11,'change_background'),(33,'Can delete background',11,'delete_background'),(34,'Can add about page',12,'add_aboutpage'),(35,'Can change about page',12,'change_aboutpage'),(36,'Can delete about page',12,'delete_aboutpage'),(37,'Can add news page',13,'add_newspage'),(38,'Can change news page',13,'change_newspage'),(39,'Can delete news page',13,'delete_newspage'),(40,'Can add reservation',14,'add_reservation'),(41,'Can change reservation',14,'change_reservation'),(42,'Can delete reservation',14,'delete_reservation'),(43,'Can add book image',15,'add_bookimage'),(44,'Can change book image',15,'change_bookimage'),(45,'Can delete book image',15,'delete_bookimage'),(46,'Can add contact',16,'add_contact'),(47,'Can change contact',16,'change_contact'),(48,'Can delete contact',16,'delete_contact'),(49,'Can add opening hours',17,'add_openinghours'),(50,'Can change opening hours',17,'change_openinghours'),(51,'Can delete opening hours',17,'delete_openinghours'),(52,'Can add home page',18,'add_homepage'),(53,'Can change home page',18,'change_homepage'),(54,'Can delete home page',18,'delete_homepage'),(55,'Can add menu page',19,'add_menupage'),(56,'Can change menu page',19,'change_menupage'),(57,'Can delete menu page',19,'delete_menupage'),(61,'Can add menu',21,'add_menu'),(62,'Can change menu',21,'change_menu'),(63,'Can delete menu',21,'delete_menu'),(67,'Can add dish',23,'add_dish'),(68,'Can change dish',23,'change_dish'),(69,'Can delete dish',23,'delete_dish'),(70,'Can add course',24,'add_course'),(71,'Can change course',24,'change_course'),(72,'Can delete course',24,'delete_course'),(73,'Can add menu category',25,'add_menucategory'),(74,'Can change menu category',25,'change_menucategory'),(75,'Can delete menu category',25,'delete_menucategory'),(76,'Can add functions page',26,'add_functionspage'),(77,'Can change functions page',26,'change_functionspage'),(78,'Can delete functions page',26,'delete_functionspage'),(79,'Can add contact page',27,'add_contactpage'),(80,'Can change contact page',27,'change_contactpage'),(81,'Can delete contact page',27,'delete_contactpage'),(82,'Can add food philosophy page',28,'add_foodphilosophypage'),(83,'Can change food philosophy page',28,'change_foodphilosophypage'),(84,'Can delete food philosophy page',28,'delete_foodphilosophypage'),(88,'Can add our producers page',30,'add_ourproducerspage'),(89,'Can change our producers page',30,'change_ourproducerspage'),(90,'Can delete our producers page',30,'delete_ourproducerspage'),(91,'Can add media',31,'add_media'),(92,'Can change media',31,'change_media'),(93,'Can delete media',31,'delete_media'),(94,'Can add reviews media page',32,'add_reviewsmediapage'),(95,'Can change reviews media page',32,'change_reviewsmediapage'),(96,'Can delete reviews media page',32,'delete_reviewsmediapage'),(97,'Can add award page',33,'add_awardpage'),(98,'Can change award page',33,'change_awardpage'),(99,'Can delete award page',33,'delete_awardpage'),(100,'Can add news events page',34,'add_newseventspage'),(101,'Can change news events page',34,'change_newseventspage'),(102,'Can delete news events page',34,'delete_newseventspage'),(103,'Can add team',35,'add_team'),(104,'Can change team',35,'change_team'),(105,'Can delete team',35,'delete_team'),(106,'Can add our team page',36,'add_ourteampage'),(107,'Can change our team page',36,'change_ourteampage'),(108,'Can delete our team page',36,'delete_ourteampage'),(109,'Can add producer',37,'add_producer'),(110,'Can change producer',37,'change_producer'),(111,'Can delete producer',37,'delete_producer'),(118,'Can add positions',40,'add_positions'),(119,'Can change positions',40,'change_positions'),(120,'Can delete positions',40,'delete_positions'),(121,'Can add employment page',41,'add_employmentpage'),(122,'Can change employment page',41,'change_employmentpage'),(123,'Can delete employment page',41,'delete_employmentpage'),(124,'Can add special contact',42,'add_specialcontact'),(125,'Can change special contact',42,'change_specialcontact'),(126,'Can delete special contact',42,'delete_specialcontact'),(127,'Can add reservation page',43,'add_reservationpage'),(128,'Can change reservation page',43,'change_reservationpage'),(129,'Can delete reservation page',43,'delete_reservationpage');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$Pk0f12MRdySQ$Ewr/S4qfNkJ7vk0FPw55mfDCmKwr7i+E0ZYUEMEMwt0=','2014-11-26 04:43:57',1,'admin','','','andrew@pollen.com.au',1,1,'2014-11-05 05:43:20');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_aboutpage`
--

DROP TABLE IF EXISTS `cms_aboutpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_aboutpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `background_id` int(11) NOT NULL,
  `news_article_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_aboutpage_48abea43` (`background_id`),
  KEY `cms_aboutpage_4f9844d6` (`news_article_id`),
  CONSTRAINT `cms_about_news_article_id_4a4fc2e1025500d3_fk_cms_newsarticle_id` FOREIGN KEY (`news_article_id`) REFERENCES `cms_newsarticle` (`id`),
  CONSTRAINT `cms_aboutpag_background_id_3ee9c644fa4f0e13_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_aboutpage`
--

LOCK TABLES `cms_aboutpage` WRITE;
/*!40000 ALTER TABLE `cms_aboutpage` DISABLE KEYS */;
INSERT INTO `cms_aboutpage` VALUES (1,'About Quay','At Quay our aim is to match the breathtaking harbour setting with quintessential modern Australian cuisine. Warm, knowledgeable, professional staff will extend genuine hospitality as they guide you through your dining experience.','2014-11-05 10:31:38',1,1,1);
/*!40000 ALTER TABLE `cms_aboutpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_aboutpage_books`
--

DROP TABLE IF EXISTS `cms_aboutpage_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_aboutpage_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aboutpage_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `aboutpage_id` (`aboutpage_id`,`book_id`),
  KEY `cms_aboutpage_books_e1f2c0ce` (`aboutpage_id`),
  KEY `cms_aboutpage_books_0a4572cc` (`book_id`),
  CONSTRAINT `cms_aboutpage_b_aboutpage_id_db99a221cd3def3_fk_cms_aboutpage_id` FOREIGN KEY (`aboutpage_id`) REFERENCES `cms_aboutpage` (`id`),
  CONSTRAINT `cms_aboutpage_books_book_id_73d9809de88f215_fk_cms_book_id` FOREIGN KEY (`book_id`) REFERENCES `cms_book` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_aboutpage_books`
--

LOCK TABLES `cms_aboutpage_books` WRITE;
/*!40000 ALTER TABLE `cms_aboutpage_books` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_aboutpage_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_award`
--

DROP TABLE IF EXISTS `cms_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `image` varchar(100) NOT NULL,
  `team_image` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_award`
--

LOCK TABLES `cms_award` WRITE;
/*!40000 ALTER TABLE `cms_award` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_awardpage`
--

DROP TABLE IF EXISTS `cms_awardpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_awardpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `column_left` varchar(10000) NOT NULL,
  `column_right` varchar(10000) NOT NULL,
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_awardpage_48abea43` (`background_id`),
  CONSTRAINT `cms_awardpag_background_id_2bac4bfd020bc447_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_awardpage`
--

LOCK TABLES `cms_awardpage` WRITE;
/*!40000 ALTER TABLE `cms_awardpage` DISABLE KEYS */;
INSERT INTO `cms_awardpage` VALUES (1,'AWARDS','Quay has long been one of Australia’s most awarded restaurants. Quay has held the coveted 3 Hat & 3 Star rating in The Sydney Morning Herald Good Food Guide & Australian Gourmet Traveller Restaurant Guide for 13 consecutive years.','<p>Quay awarded the Hottest Restaurant in Australia in The Australian Hot 50 Restaurant Awards 2013<br><br><strong>Restaurant of the Year 2013</strong><br>The Sydney Morning Herald 2013 Good Food Guide<br><br><strong>Restaurant of the Year 2013</strong><br>Australian Gourmet Traveller 2013 Restaurant Guide<br><br><strong>Best Restaurant in Australasia</strong><br>S. Pellegrino World’s 50 Best Restaurants 2012<br><br><strong>Quay awarded 3 hats in Sydney Morning Herald Good Food Guide 2012</strong><br>For the 10th Consecutive year Quay is awarded 3 Chefs hats<br><br><strong>Placed 27th in the world</strong><br>S. Pellegrino World’s 50 Best Restaurants 2010<br><br><strong>Restaurant of the Year 2010</strong><br>The Sydney Morning Herald 2010 Good Food Guide<br><br><strong>Placed 46th in the world</strong><br>S. Pellegrino World’s 50 Best Restaurants 2009<br><br><strong>Restaurant of the Year 2009</strong><br>Australian Gourmet Traveller 2009 Restaurant Guide<br><br><strong>Restaurant of the Year 2008</strong><br>Restaurant & Catering 2008 National Awards for Excellence</p>','<p>Quay awarded 3 hats in Sydney Morning Herald Good Food Guide 2014 for the 12th conescutive year<br><br>Quay awarded 3 hats in Sydney Morning Herald Good Food Guide 2013<br><br><strong>Placed 29th in the world</strong><br>S. Pellegrino World’s 50 Best Restaurants 2012<br><br><strong>Chef of the Year 2012</strong><br>Peter Gilmore Awarded Chef of the year in Sydney Morning Herald Good Food Guide<br><br><strong>Placed 26th in the world</strong><br>S. Pellegrino World’s 50 Best Restaurants 2011<br><br><strong>Best Restaurant in Australasia</strong><br>S. Pellegrino World’s 50 Best Restaurants 2010<br><br><strong>Restaurant of the Year 2010</strong><br>Australian Gourmet Traveller 2010 Restaurant Guide<br><br><strong>Restaurant of the Year 2009</strong><br>The Sydney Morning Herald 2009 Good Food Guide<br><br><strong>Restaurant of the Year 2009</strong><br>Restaurant & Catering 2009 National Awards for Excellence</p>',20,1,'2014-11-14 01:08:09');
/*!40000 ALTER TABLE `cms_awardpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_background`
--

DROP TABLE IF EXISTS `cms_background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cssClass` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_background`
--

LOCK TABLES `cms_background` WRITE;
/*!40000 ALTER TABLE `cms_background` DISABLE KEYS */;
INSERT INTO `cms_background` VALUES (1,'image-about','2014-11-05 06:06:08'),(2,'dish-duck','2014-11-13 02:41:47'),(3,'dish-beetroot','2014-11-13 02:41:59'),(4,'dish-pigjowl','2014-11-13 02:42:08'),(5,'dish-beef','2014-11-13 02:42:17'),(6,'dish-muscovado','2014-11-13 02:42:27'),(7,'dish-chocethereal','2014-11-13 02:42:58'),(8,'dish-quail','2014-11-13 02:43:05'),(9,'dish-xosea','2014-11-13 02:43:12'),(10,'dish-blackpudding','2014-11-13 02:43:22'),(11,'illustration-spring','2014-11-13 04:22:55'),(12,'illustration-summer','2014-11-13 04:23:06'),(13,'illustration-autumn','2014-11-13 04:23:15'),(14,'illustration-winter','2014-11-13 04:23:23'),(15,'image-producers-01','2014-11-13 22:27:09'),(16,'image-producers-02','2014-11-13 22:27:15'),(17,'image-producers-03','2014-11-13 22:27:22'),(18,'image-producers-04','2014-11-13 22:27:43'),(19,'image-philosophy-01','2014-11-13 23:37:05'),(20,'image-awards','2014-11-14 01:07:46'),(21,'image-functions-01','2014-11-14 06:39:06'),(22,'uppertower-01','2014-11-14 06:39:18'),(23,'uppertower-02','2014-11-14 06:39:35'),(24,'uppertower-03','2014-11-14 06:39:40'),(25,'uppertower-01','2014-11-14 06:39:51'),(26,'image-functions-uppertower','2014-11-14 06:41:18'),(27,'image-functions-greenroom','2014-11-14 06:41:34'),(28,'greenroom-01','2014-11-14 06:41:52'),(29,'greenroom-02','2014-11-14 06:41:58'),(30,'greenroom-03','2014-11-14 06:42:14'),(31,'image-functions-restaurant','2014-11-14 06:43:51'),(32,'restaurant-01','2014-11-14 06:44:09'),(33,'restaurant-02','2014-11-14 06:44:14'),(34,'restaurant-03','2014-11-14 06:44:19'),(35,'image-functions-02','2014-11-14 06:45:55'),(36,'image-employment','2014-11-16 23:33:29'),(37,'image-contact','2014-11-28 01:04:04');
/*!40000 ALTER TABLE `cms_background` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_book`
--

DROP TABLE IF EXISTS `cms_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(1500) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `author` varchar(255) NOT NULL,
  `book_image_id` int(11) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `quote` varchar(500) NOT NULL,
  `quote_author` varchar(255) NOT NULL,
  `sub_author` varchar(255) NOT NULL,
  `sub_title_line_1` varchar(255) NOT NULL,
  `sub_title_line_2` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_book_d93c8103` (`book_image_id`),
  CONSTRAINT `cms_book_book_image_id_1a303a4650e3bc7c_fk_cms_bookimage_id` FOREIGN KEY (`book_image_id`) REFERENCES `cms_bookimage` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_book`
--

LOCK TABLES `cms_book` WRITE;
/*!40000 ALTER TABLE `cms_book` DISABLE KEYS */;
INSERT INTO `cms_book` VALUES (1,'ORGANUM','<br><br>While there is a layered complexity to world-renowned chef Peter Gilmore\'s ethereal - yet grounded - cuisine, his philosophy of cooking is relatively simple. Just four elements are required to create perfect unison in a dish: nature, texture, intensity and purity.<br><br>In his new book, Peter invites the reader to share in his private obsession with nature - when not in the kitchen at Sydney\'s Quay restaurant, he is working in his experimental garden where he grows a huge array of edible plant species. Each component of a plant, from sweet, earthy roots to bitter fronds and fragrant blossoms, is potentially destined for inclusion in one of the 40 exquisite dishes featured here. Peter also introduces us to the many influences on his cooking, and to the people who grow, catch and source key ingredients. Images include intensely beautiful food and ingredient shots, as well as producers and produce photographed on location.<br><br>Book is available at the restaurant for $100 or the book can be purchased online.',0,'2014-11-13 02:47:21','PETER GILMORE',1,'http://store.quay.com.au/organum-nature-purity-texture-intensity/','My second book Organum delves deeper into my cuisine philosophies and celebrates some of the farmers, producers and providores who share my passion for quality produce.','PETER GILMORE','Photography by Brett Stevens. Designed by Reuben Crossman.','NATURE, TEXTURE,','INTENSITY, PURITY'),(2,'QUAY','<br><br>From the culinary genius Peter Gilmore, one of the top 50 chefs in the world, comes this eagerly-anticipated, ground-breaking book. Quay’s stunning design and photography perfectly echoes Peter’s nature-based philosophy and the organic presentation that is synonymous with the fine dining experience at Quay . Peter’s recipes, including the irresistible eight-textured chocolate cake and his signature iridescent sea pearls, will take you on an inspirational adventure, exploring flavour, texture and technique. Start with a single component, build to a show-stopping dish, or simply enjoy the visual and culinary journey.<br><br>A signed copy of the book is available at the restaurant for $95 or the book can be purchased online.',1,'2014-11-13 02:50:52','PETER GILMORE',2,'http://store.quay.com.au/quay-food-inspired-by-nature/','I am inspired by nature and have coined the term nature-based cuisine to describe my food. Nature offers us so much diversity - a natural elegance and beauty - and it is the organic nature of food, its textures and flavours that is at the heart of my cooking.','PETER GILMORE','Photography by Anson Smart. Designed by Reuben Crossman.','FOOD INSPIRED ','BY NATURE');
/*!40000 ALTER TABLE `cms_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_bookimage`
--

DROP TABLE IF EXISTS `cms_bookimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_bookimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cssClass` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_bookimage`
--

LOCK TABLES `cms_bookimage` WRITE;
/*!40000 ALTER TABLE `cms_bookimage` DISABLE KEYS */;
INSERT INTO `cms_bookimage` VALUES (1,'book-organum','2014-11-13 02:47:07'),(2,'book-quay','2014-11-13 02:49:08');
/*!40000 ALTER TABLE `cms_bookimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_contact`
--

DROP TABLE IF EXISTS `cms_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_line_1` varchar(250) NOT NULL,
  `address_line_2` varchar(250) NOT NULL,
  `address_line_3` varchar(250) NOT NULL,
  `tel` varchar(250) NOT NULL,
  `fax` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_contact`
--

LOCK TABLES `cms_contact` WRITE;
/*!40000 ALTER TABLE `cms_contact` DISABLE KEYS */;
INSERT INTO `cms_contact` VALUES (1,'Upper Level,','Overseas Passenger Terminal','The Rocks, Sydney 2000','(61 2) 9251 5600','(61 2) 9241 4901','reservations@quay.com.au');
/*!40000 ALTER TABLE `cms_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_contactpage`
--

DROP TABLE IF EXISTS `cms_contactpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_contactpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `gift_voucher_copy` varchar(1000),
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `reservation_copy` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_contactpage_48abea43` (`background_id`),
  CONSTRAINT `cms_contactp_background_id_5669ef8e38ad8c90_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_contactpage`
--

LOCK TABLES `cms_contactpage` WRITE;
/*!40000 ALTER TABLE `cms_contactpage` DISABLE KEYS */;
INSERT INTO `cms_contactpage` VALUES (1,'CONTACT','<p>If giving the gift of an unforgettable food and wine experience is on your Christmas shopping list this year, then look no further than a gift voucher for&nbsp;Quay.</p>',37,1,'2014-11-28 04:18:12','<p>The reservations office is open from 1.30pm - 5pm on Monday and 10am - 5pm Tuesday to Friday.<br><br>Alternatively, you can submit an online&nbsp;booking.</p>');
/*!40000 ALTER TABLE `cms_contactpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_contactpage_contacts`
--

DROP TABLE IF EXISTS `cms_contactpage_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_contactpage_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contactpage_id` int(11) NOT NULL,
  `specialcontact_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contactpage_id` (`contactpage_id`,`specialcontact_id`),
  KEY `cms_contactpage_contacts_4d92dd2a` (`contactpage_id`),
  KEY `cms_contactpage_contacts_bd5ef4c2` (`specialcontact_id`),
  CONSTRAINT `cms_c_specialcontact_id_363762367ddf38a_fk_cms_specialcontact_id` FOREIGN KEY (`specialcontact_id`) REFERENCES `cms_specialcontact` (`id`),
  CONSTRAINT `cms_contac_contactpage_id_491499ea7b92d4e7_fk_cms_contactpage_id` FOREIGN KEY (`contactpage_id`) REFERENCES `cms_contactpage` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_contactpage_contacts`
--

LOCK TABLES `cms_contactpage_contacts` WRITE;
/*!40000 ALTER TABLE `cms_contactpage_contacts` DISABLE KEYS */;
INSERT INTO `cms_contactpage_contacts` VALUES (9,1,1,0),(10,1,2,1),(11,1,3,2),(12,1,4,3);
/*!40000 ALTER TABLE `cms_contactpage_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_course`
--

DROP TABLE IF EXISTS `cms_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_number` varchar(250) NOT NULL,
  `course_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_course`
--

LOCK TABLES `cms_course` WRITE;
/*!40000 ALTER TABLE `cms_course` DISABLE KEYS */;
INSERT INTO `cms_course` VALUES (2,'One','Lunch course one'),(3,'Two','Lunch course two'),(4,'Three','Lunch course three'),(5,'Four','Lunch course four'),(6,'none','Tasting course');
/*!40000 ALTER TABLE `cms_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_course_dishes`
--

DROP TABLE IF EXISTS `cms_course_dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_course_dishes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `dish_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `course_id` (`course_id`,`dish_id`),
  KEY `cms_course_dishes_ea134da7` (`course_id`),
  KEY `cms_course_dishes_a5845519` (`dish_id`),
  CONSTRAINT `cms_course_dishes_course_id_3be660066718bfee_fk_cms_course_id` FOREIGN KEY (`course_id`) REFERENCES `cms_course` (`id`),
  CONSTRAINT `cms_course_dishes_dish_id_238467e9defaf2f7_fk_cms_dish_id` FOREIGN KEY (`dish_id`) REFERENCES `cms_dish` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_course_dishes`
--

LOCK TABLES `cms_course_dishes` WRITE;
/*!40000 ALTER TABLE `cms_course_dishes` DISABLE KEYS */;
INSERT INTO `cms_course_dishes` VALUES (13,2,3,4),(14,2,4,5),(15,2,5,6),(16,2,6,7),(21,4,11,8),(22,4,12,9),(23,4,13,10),(24,4,14,11),(25,5,15,12),(26,5,16,13),(27,5,17,14),(28,5,18,15),(29,3,7,12),(30,3,8,13),(31,3,9,14),(32,3,10,15),(33,6,3,16),(34,6,5,17),(35,6,7,18),(36,6,9,19),(37,6,13,20),(38,6,12,21),(39,6,17,22),(40,6,16,23),(41,6,20,24);
/*!40000 ALTER TABLE `cms_course_dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_dish`
--

DROP TABLE IF EXISTS `cms_dish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_dish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(1500) NOT NULL,
  `name` varchar(1500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_dish`
--

LOCK TABLES `cms_dish` WRITE;
/*!40000 ALTER TABLE `cms_dish` DISABLE KEYS */;
INSERT INTO `cms_dish` VALUES (3,'Greenlip abalone, saltwater poached chicken,\r\n    <br>eggplant, smoked eel jelly,\r\n    <br>agretti, takuan, kai-lan','Greenlip abalone'),(4,'Raw smoked Blackmore wagyu,\r\n    <br>horseradish soured cream,\r\n    <br>fermented rye crisps,\r\n    <br>raw funghi','Raw smoked Blackmore wagyu'),(5,'Congee of Northern Australian mud crab,\r\n    <br>fresh palm heart, egg yolk emulsion','Congee of Northern Australian mud crab'),(6,'Salad of celery heart,\r\n    <br>Pantelleria capers, aged feta,\r\n    <br>rare herbs, flowers','Salad of celery heart'),(7,'Smoked and confit pig jowl,\r\n    <br>roasted koji, shiitake, kombu,\r\n    <br>sea scallop, sesame','Smoked and confit pig jowl'),(8,'Bay lobster,\r\n    <br>sea urchin, salted egg yolk','Bay lobster'),(9,'XO Sea\r\n    <br>five sea textures','XO Sea'),(10,'Spring peas, asparagus,\r\n    <br>butter lettuce, milk curd,\r\n    <br>umami vegetable broth','Spring peas'),(11,'Chatham Island blue cod,\r\n    <br>cuttlefish, kabu turnips,\r\n    <br>oyster broth','Chatham Island blue cod'),(12,'Slow cooked duck,\r\n    <br>cultured rice and barley,\r\n    <br>spring garlic, black garlic','Slow cooked duck'),(13,'Flinders Island salt grass lamb,\r\n    <br>spring garden vegetables,\r\n    <br>smoked oyster, seaweed, anchovy','Flinders Island salt grass lamb'),(14,'Stone pot organic green rice,\r\n    <br>silken tofu, mountain spinach,\r\n    <br>spring cabbage stems, seaweed','Stone pot organic green rice'),(15,'Citrus and almonds\r\n    <br>\r\n    <br>Muscovado, prune,\r\n    <br>jersey, dulcey, coconut','Citrus and almonds'),(16,'Chocolate ethereal','Chocolate ethereal'),(17,'Snow egg','Snow egg'),(18,'A selection of cheese,\r\n    <br>raisin and walnut sourdough','Cheese'),(19,'Muscovado, prune,\r\n        <br>jersey, dulcey, coconut','Muscovado'),(20,'Coffee, Tea & Quay Petits Fours','Coffee, Tea & Quay Petits Fours');
/*!40000 ALTER TABLE `cms_dish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_employmentpage`
--

DROP TABLE IF EXISTS `cms_employmentpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_employmentpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `background_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_employmentpage_48abea43` (`background_id`),
  CONSTRAINT `cms_employme_background_id_238504bc64499355_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_employmentpage`
--

LOCK TABLES `cms_employmentpage` WRITE;
/*!40000 ALTER TABLE `cms_employmentpage` DISABLE KEYS */;
INSERT INTO `cms_employmentpage` VALUES (1,'EMPLOYMENT','<p>Quay Restaurant is part of a growing restaurant group that focuses on excellence in the hospitality industry. In addition to Quay, we also own and operate Otto Ristorante at Woolloomooloo Wharf, Sydney. We are always interested in hearing from highly motivated candidates who are seeking a career in the hospitality industry within an organisation that is employee focused and offers career development and opportunities.<br><br>We are always seeking to meet individuals who have a great attitude, are passionate about food and wine and have a great work ethic. We would love to hear from you if you feel you have the right qualities and would like to work in a progressive organisation.</p>','2014-11-17 00:02:38',36);
/*!40000 ALTER TABLE `cms_employmentpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_employmentpage_jobs`
--

DROP TABLE IF EXISTS `cms_employmentpage_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_employmentpage_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employmentpage_id` int(11) NOT NULL,
  `positions_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employmentpage_id` (`employmentpage_id`,`positions_id`),
  KEY `cms_employmentpage_jobs_9851020d` (`employmentpage_id`),
  KEY `cms_employmentpage_jobs_cf0d2a51` (`positions_id`),
  CONSTRAINT `cms__employmentpage_id_6f7b63d0f5e7d6c4_fk_cms_employmentpage_id` FOREIGN KEY (`employmentpage_id`) REFERENCES `cms_employmentpage` (`id`),
  CONSTRAINT `cms_employment_positions_id_21a99273d2323d44_fk_cms_positions_id` FOREIGN KEY (`positions_id`) REFERENCES `cms_positions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_employmentpage_jobs`
--

LOCK TABLES `cms_employmentpage_jobs` WRITE;
/*!40000 ALTER TABLE `cms_employmentpage_jobs` DISABLE KEYS */;
INSERT INTO `cms_employmentpage_jobs` VALUES (3,1,2,0);
/*!40000 ALTER TABLE `cms_employmentpage_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_foodphilosophypage`
--

DROP TABLE IF EXISTS `cms_foodphilosophypage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_foodphilosophypage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `intro_copy2` varchar(1000) NOT NULL,
  `nature` varchar(1000) NOT NULL,
  `texture` varchar(1000) NOT NULL,
  `intensity` varchar(1000) NOT NULL,
  `purity` varchar(1000) NOT NULL,
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_foodphilosophypage_48abea43` (`background_id`),
  CONSTRAINT `cms_foodphil_background_id_53816a7edf5b5ac4_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_foodphilosophypage`
--

LOCK TABLES `cms_foodphilosophypage` WRITE;
/*!40000 ALTER TABLE `cms_foodphilosophypage` DISABLE KEYS */;
INSERT INTO `cms_foodphilosophypage` VALUES (1,'FOOD<br>PHILOSOPHY','My aim is to create original, beautifully crafted food that is inspired by nature, texture, intensity and purity. These four elements inform my approach to cooking. Nature’s incredible diversity inspires my work. My test garden is where I can observe the life cycle of plants and test different varieties of heirloom and rare plants. This gives me unique raw materials, different flavours, textures and colours to work with. I then work closely with growers and farmers to produce beautiful bespoke produce on a larger scale for the restaurant. This direct approach of working closely with farmers has been successful for many years now.','<p>Texture has long been the most important element in the development of my dishes. Texture’s role in the sensory nature of eating cannot be understated. How texture’s work together and bring pleasure to the palate is paramount in my mind when creating a new dish.<br><br>Intensity of flavour where strong, clear, natural flavours are at the fore and where flavours build on each other and harmonise to create distinctive flavour memories is my objective.<br><br>For me purity represents harmony and balance; when all the elements in a dish come together to create something special. It is when textures and flavours are aligned and create something greater than the sum of their parts – something new, something pure. <br><br>When I create a new dish I draw upon my life experiences, taste memories and my imagination. I’m guided and informed by my food philosophy and aim to create dishes that are unique. </p>','Nature and the natural world is such a strong inspiration for many of my dishes. Working my own test garden over the last 9 years I have developed a deep appreciation and wonder for nature. The intricacies of the life cycle of plants, their various forms, shapes, textures and flavours, provide an endless range of possibilities. One of my aims is to reflect nature’s beauty and sense of wonder in my dishes. ','Texture has long been one of the most important elements in the development of my dishes. Texture’s role in the pleasure it brings the palate cannot be underestimated. As an example I always think what is the most pleasurable thing about eating ice cream? Is it the flavour or is it the texture and mouth feel that we respond to? Getting the degrees of texture right within a dish and how texture can be combined and layered to bring pleasure to the eating experience is a powerful skill to master. The role of texture is of paramount importance in many traditional Asian cuisines. The influence of which has informed my food in a profound way.','The flavour intensity of a dish has the power to transport you. Along with aroma the sense of taste can stir memories of time, place and feelings. The impact of a particular flavour or smell can last in your memory for an incredibly long time. For me, the skill of a chef to intensify and blend flavours to create a new sensation is akin to alchemy. ','What I mean by purity is a sense of balance and harmony within a dish. When all the elements come together perfectly, when the selection of ingredients and the nature of those ingredients are fully expressed through considered technique. Purity is the magic that happens when all the parts and components of a dish come together and create something new …. something pure. ',19,1,'2014-11-13 23:37:58');
/*!40000 ALTER TABLE `cms_foodphilosophypage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_functionspage`
--

DROP TABLE IF EXISTS `cms_functionspage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_functionspage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `greenroom_copy1` varchar(2000) NOT NULL,
  `greenroom_copy2` varchar(2000) NOT NULL,
  `greenroom_image_id` int(11) NOT NULL,
  `restaurant_copy1` varchar(2000) NOT NULL,
  `restaurant_copy2` varchar(2000) NOT NULL,
  `restaurant_image_id` int(11) NOT NULL,
  `uppertower_copy1` varchar(2000) NOT NULL,
  `uppertower_copy2` varchar(2000) NOT NULL,
  `uppertower_image_id` int(11) NOT NULL,
  `wedding_background_id` int(11) NOT NULL,
  `wedding_copy` varchar(1000) NOT NULL,
  `wedding_title` varchar(255) NOT NULL,
  `wedding_video` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_functionspage_48abea43` (`background_id`),
  KEY `cms_functionspage_61468f58` (`greenroom_image_id`),
  KEY `cms_functionspage_4a7bff5e` (`restaurant_image_id`),
  KEY `cms_functionspage_05f7acf2` (`uppertower_image_id`),
  KEY `cms_functionspage_80515372` (`wedding_background_id`),
  CONSTRAINT `cms__wedding_background_id_6d44bacfa651863e_fk_cms_background_id` FOREIGN KEY (`wedding_background_id`) REFERENCES `cms_background` (`id`),
  CONSTRAINT `cms_fu_restaurant_image_id_6c2f3906ddd61b10_fk_cms_background_id` FOREIGN KEY (`restaurant_image_id`) REFERENCES `cms_background` (`id`),
  CONSTRAINT `cms_fu_uppertower_image_id_3edcf351e906f166_fk_cms_background_id` FOREIGN KEY (`uppertower_image_id`) REFERENCES `cms_background` (`id`),
  CONSTRAINT `cms_func_greenroom_image_id_599114462717efa_fk_cms_background_id` FOREIGN KEY (`greenroom_image_id`) REFERENCES `cms_background` (`id`),
  CONSTRAINT `cms_function_background_id_4754aaeb59de177d_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_functionspage`
--

LOCK TABLES `cms_functionspage` WRITE;
/*!40000 ALTER TABLE `cms_functionspage` DISABLE KEYS */;
INSERT INTO `cms_functionspage` VALUES (1,'FUNCTIONS','<p>Quay hosts some of the country’s most exclusive private functions.  <br><br>We have different areas to cater for the function that you need whether you are looking to host an intimate dinner for 2 or a stylish cocktail party for 300.</p>',21,1,'2014-11-28 05:10:50','<p>The Green Room at Quay is a spectacular venue situated upstairs from the restaurant. Named after the famous Sydney Opera House Green Room, it offers 240 degree harbour views, with the location of its namesake Sydney Opera House directly across the harbour.<br><br>This stunning venue spans 232 square metres opening up to spacious terraces facing Circular Quay on one side and the Harbour Bridge on the other. The stunning, plush interior of this amazing space subtly reflects the colour and images of its exquisite harbour side setting.<br><br>The elegance and style of this exclusive location is taken to the next level with the inclusion of Quay’s world-class food and wine menu. Incredible cuisine and Sydney’s magical harbour as the backdrop makes the Green Room a breathtaking option for wedding receptions and other functions that require that extra level of style and quality. The venue often hosts unique events that require a unique venue such as press launches and media interviews, private parties, corporate functions and international business meetings.<br><br>Quay’s function staff are well connected when it comes to adding entertainment and flowers and can make quality recommendations when booking your function.</p>','<p>Exclusive Use Conditions and Capacity <br><br>Sit down lunch/dinner with dance floor - 100 pax<br><br>Cocktail parties - 180 pax<br><br>The minimum spend for an exclusive function in Quay\'s Green Room starts from $6,000 for cocktails, \r\n                  $4,000 for lunch and from $6,000 for dinner<br><br>Please note the summer rate for dinner applicable in Nov/Dec is $10,000.\r\n                </p>',27,'<p>Quay Restaurant, with its floor to ceiling glass walls, is a spectacular setting for all modes of corporate entertainment, wedding receptions and cocktail parties.<br><br>The quality and style of our food and wine, and our exceptional service standards will leave a lasting impression on all who attend. Quay\'s unique layout also enables us to respond with great flexibility to your needs.<br><br>Award winning cuisine, world class service and a stellar reputation of international renown, all come together in an iconic Sydney location. An event at Quay will be remembered and talked about for a long time after. \r\n                  The restaurant seats up to 150 guests, or can accommodate 250 guests for a cocktail party.\r\n                </p>','<p><br>Exclusive Use Conditions and Capacity <br><br>Sit down lunch / dinner with dance floor - 110 pax<br><br>Sit down lunch/ dinner with no dance floor - 150 pax<br><br>Cocktail parties - 250 pax<br><br>Exclusive use of Quay Restaurant does not require a hire fee, however we do require a minimum spend of $15,000 on food and beverages for lunch.<br><br>For an exclusive dinner, the minimum spend requirement starts from $25,000.</p>',31,'<p>Quay’s Upper Tower is Sydney’s most spectacular dining room and possibly one of the most impressive and inspiring restaurant experiences in the world.<br><br>In this delightfully intimate setting, floor to ceiling glass walls provide jaw-dropping views of Sydney Harbour that astonish even those familiar with its exceptional sights and sounds.<br><br>Experience an exquisite meal from Australia’s top chef, while the life of the world’s most stunning harbour city floats by outside. Your view of passing historical ferries, luxury liners and Sydney’s quirky local watercraft dotting Circular Quay, is bordered by the iconic Sydney Opera House on one side and the Harbour Bridge on the other, an endless maritime panorama with the city as a glittering backdrop.<br><br>Function menus reflect the current restaurant menu and feature from a selection of four entreés, four main courses and four dessert choices, each dish an embodiment of Quay’s food philosophy.<br><br>An intimate dinner, a business lunch or an important celebration are immediately given a whole new level of style and quality when hosted in the Upper Tower.</p>','<p>Exclusive Use Conditions and Capacity <br><br>Sit down lunch/dinner - 32 pax<br><br>A minimum spend requirement for exclusive use of the Upper Tower starts from $3,500 for lunch and $5,000 for dinner.<br><br>Please note, that the November/December Lunch rate is $4000 and the Dinner rate is $5000.</p>',26,35,'            <p>Quay has played host to some of the country’s most exclusive wedding receptions. We invite you to experience your special day with an exquisite menu from award winning chef Peter Gilmore, whilst enjoying the uninterrupted panorama of the world’s most stunning harbour.<br><br>A wedding at Quay allows for magnificent photo opportunities with breathtaking views of the Opera House, Harbour Bridge and glittering Sydney City skyline. Quay boasts three different reception areas; the entire Restaurant, the exclusive Upper Tower, and the stunning Green Room.<br><br>Whether you are hosting an intimate sit down dinner reception for 30 guests or a stylish cocktail party reception of 300, we are able to provide a venue capable of catering to your every need.</p>','WEDDINGS<br>AT QUAY','jxnx1M9LfRM');
/*!40000 ALTER TABLE `cms_functionspage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_functionspage_contacts`
--

DROP TABLE IF EXISTS `cms_functionspage_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_functionspage_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `functionspage_id` int(11) NOT NULL,
  `specialcontact_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `functionspage_id` (`functionspage_id`,`specialcontact_id`),
  KEY `cms_functionspage_contacts_2975412e` (`functionspage_id`),
  KEY `cms_functionspage_contacts_bd5ef4c2` (`specialcontact_id`),
  CONSTRAINT `cms__specialcontact_id_4d891c2e6f9d7077_fk_cms_specialcontact_id` FOREIGN KEY (`specialcontact_id`) REFERENCES `cms_specialcontact` (`id`),
  CONSTRAINT `cms_fu_functionspage_id_619db8e30245c0f5_fk_cms_functionspage_id` FOREIGN KEY (`functionspage_id`) REFERENCES `cms_functionspage` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_functionspage_contacts`
--

LOCK TABLES `cms_functionspage_contacts` WRITE;
/*!40000 ALTER TABLE `cms_functionspage_contacts` DISABLE KEYS */;
INSERT INTO `cms_functionspage_contacts` VALUES (1,1,1,0),(2,1,5,1);
/*!40000 ALTER TABLE `cms_functionspage_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_functionspage_greenroom_carou`
--

DROP TABLE IF EXISTS `cms_functionspage_greenroom_carou`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_functionspage_greenroom_carou` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `functionspage_id` int(11) NOT NULL,
  `background_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `functionspage_id` (`functionspage_id`,`background_id`),
  KEY `cms_functionspage_greenroom_carou_2975412e` (`functionspage_id`),
  KEY `cms_functionspage_greenroom_carou_48abea43` (`background_id`),
  CONSTRAINT `cms_fu_functionspage_id_4219d132e5f0eb7e_fk_cms_functionspage_id` FOREIGN KEY (`functionspage_id`) REFERENCES `cms_functionspage` (`id`),
  CONSTRAINT `cms_function_background_id_68b7fd1fa3b73c37_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_functionspage_greenroom_carou`
--

LOCK TABLES `cms_functionspage_greenroom_carou` WRITE;
/*!40000 ALTER TABLE `cms_functionspage_greenroom_carou` DISABLE KEYS */;
INSERT INTO `cms_functionspage_greenroom_carou` VALUES (4,1,28,0),(5,1,29,1),(6,1,30,2);
/*!40000 ALTER TABLE `cms_functionspage_greenroom_carou` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_functionspage_restaurant_carou`
--

DROP TABLE IF EXISTS `cms_functionspage_restaurant_carou`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_functionspage_restaurant_carou` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `functionspage_id` int(11) NOT NULL,
  `background_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `functionspage_id` (`functionspage_id`,`background_id`),
  KEY `cms_functionspage_restaurant_carou_2975412e` (`functionspage_id`),
  KEY `cms_functionspage_restaurant_carou_48abea43` (`background_id`),
  CONSTRAINT `cms_fun_functionspage_id_a24bd657628d4ee_fk_cms_functionspage_id` FOREIGN KEY (`functionspage_id`) REFERENCES `cms_functionspage` (`id`),
  CONSTRAINT `cms_function_background_id_148163cb82d90e75_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_functionspage_restaurant_carou`
--

LOCK TABLES `cms_functionspage_restaurant_carou` WRITE;
/*!40000 ALTER TABLE `cms_functionspage_restaurant_carou` DISABLE KEYS */;
INSERT INTO `cms_functionspage_restaurant_carou` VALUES (4,1,32,0),(5,1,33,1),(6,1,34,2);
/*!40000 ALTER TABLE `cms_functionspage_restaurant_carou` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_functionspage_uppertower_carou`
--

DROP TABLE IF EXISTS `cms_functionspage_uppertower_carou`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_functionspage_uppertower_carou` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `functionspage_id` int(11) NOT NULL,
  `background_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `functionspage_id` (`functionspage_id`,`background_id`),
  KEY `cms_functionspage_uppertower_carou_2975412e` (`functionspage_id`),
  KEY `cms_functionspage_uppertower_carou_48abea43` (`background_id`),
  CONSTRAINT `cms_fu_functionspage_id_57b69f4e18f20a24_fk_cms_functionspage_id` FOREIGN KEY (`functionspage_id`) REFERENCES `cms_functionspage` (`id`),
  CONSTRAINT `cms_function_background_id_626f5b7a816554e3_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_functionspage_uppertower_carou`
--

LOCK TABLES `cms_functionspage_uppertower_carou` WRITE;
/*!40000 ALTER TABLE `cms_functionspage_uppertower_carou` DISABLE KEYS */;
INSERT INTO `cms_functionspage_uppertower_carou` VALUES (4,1,25,0),(5,1,23,1),(6,1,24,2);
/*!40000 ALTER TABLE `cms_functionspage_uppertower_carou` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_homepage`
--

DROP TABLE IF EXISTS `cms_homepage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_homepage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `welcome_copy` varchar(500) NOT NULL,
  `menu_copy` varchar(500) NOT NULL,
  `menu_copy2` varchar(500) NOT NULL,
  `function_copy` varchar(500) NOT NULL,
  `about_copy` varchar(500) NOT NULL,
  `display_promo` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `news_article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_homepage_4f9844d6` (`news_article_id`),
  CONSTRAINT `cms_homep_news_article_id_15c1f0fcfbe79cc2_fk_cms_newsarticle_id` FOREIGN KEY (`news_article_id`) REFERENCES `cms_newsarticle` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_homepage`
--

LOCK TABLES `cms_homepage` WRITE;
/*!40000 ALTER TABLE `cms_homepage` DISABLE KEYS */;
INSERT INTO `cms_homepage` VALUES (1,'Our aim is to give you a rich experience. To explore some of Australia’s most beautiful, natural produce.<br><br>To present an original cuisine where texture, purity of flavour and balance is paramount. To bring warmth and knowledge to our service in one of the world’s most extraordinary harbour settings.\r\n','I’m looking to nature for my inspiration. Letting the natural produce speak. There’s so much out there, so much elegance and beauty. ','PETER GILMORE','Every event hosted by Quay not only enjoys our world-class cuisine, but is surrounded by the iconic maritime panorama of the world’s most stunning harbour city.','At Quay our aim is to provide a truly memorable dining experience. An experience that celebrates the diversity of our wonderful Australian ingredients cooked with imagination and passion. Warm, knowledgeable, professional staff will extend genuine hospitality as they guide you through your dining experience.',1,1,'2014-11-26 06:00:43',1);
/*!40000 ALTER TABLE `cms_homepage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_homepage_background`
--

DROP TABLE IF EXISTS `cms_homepage_background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_homepage_background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `homepage_id` int(11) NOT NULL,
  `background_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `homepage_id` (`homepage_id`,`background_id`),
  KEY `cms_homepage_background_a6c7fe0b` (`homepage_id`),
  KEY `cms_homepage_background_48abea43` (`background_id`),
  CONSTRAINT `cms_homepage_bac_homepage_id_2343a20896724fcc_fk_cms_homepage_id` FOREIGN KEY (`homepage_id`) REFERENCES `cms_homepage` (`id`),
  CONSTRAINT `cms_homepage_background_id_6d91f7166d037e56_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_homepage_background`
--

LOCK TABLES `cms_homepage_background` WRITE;
/*!40000 ALTER TABLE `cms_homepage_background` DISABLE KEYS */;
INSERT INTO `cms_homepage_background` VALUES (11,1,2),(12,1,3),(13,1,4),(14,1,5),(15,1,6);
/*!40000 ALTER TABLE `cms_homepage_background` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_media`
--

DROP TABLE IF EXISTS `cms_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quote` varchar(2000) NOT NULL,
  `editor` varchar(500) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `pdf` varchar(100) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_media`
--

LOCK TABLES `cms_media` WRITE;
/*!40000 ALTER TABLE `cms_media` DISABLE KEYS */;
INSERT INTO `cms_media` VALUES (1,'This is Sydney dining at its very best, where the food, the wine, the harbour views and the service don’t compete but blend in perfect harmony.','SMH Good Food Guide 2012',1,'2014-11-14 00:40:07','','static/pdf/good_food_guide_review_2012.pdf'),(2,'In this contemporary space with its trifecta of harbour, bridge and Opera House views, several hours can pass in a blur of harmonious, world-beating tucker. This is a restaurant that just keeps getting better and better. And better.','SMH Good Food Guide 2011',1,'2014-11-14 00:40:48','','static/pdf/good_food_guide_review_2011.pdf'),(3,'Peter Gilmore delivers exciting, enticing, sensual and cutting edge Australian cooking in a landmark setting that makes Quay worthy of the title of Australia\'s best restaurant.<br><br>Enter Heaven - each dish is a work in progress, testament to the chef\'s willingness to suprise and entertain. At first, great restaurants transform your life for just a few hours. Then subtly, they are with you for the rest of your life. They\'re a place of adventure and discovery, a combination of dazzling theatre, kitchen alchemy and unexpected magic - in a fantasy world where you\'re the centre of attention. Quay is all those things and more which is why Quay has been named SMH Good Food Guide - Restaurant of the Year 2009.<br><br>Simon Thomsen','SMH Good Food Guide 2008',1,'2014-11-14 00:41:25','','static/pdf/enter-heaven-2008.pdf'),(4,' Another great win for owners Leon and John Fink together with the master himself Head Chef Peter Gilmore taking out the award for the 2009 Restaurant of the Year - SMH Good Food Guide Awards. Quay\'s continuing drive for excellence sees it score the top award for the third time.<br><br>Simon Thomsen','SMH Good Food Guide 2008',1,'2014-11-14 00:42:03','','static/pdf/SMH-Personal-Best-020908.pdf'),(5,'Owners Leon and John Fink together with Head Chef Peter Gilmore have emerged victorious in a field of over 400 establishments reviewed in the Australian Gourmet Traveller 2009 Restaurant Awards as Restaurant of the Year - 2009. In the past seven years, Quay\'s reputation has grown.<br><br>It\'s the union of contemporary technology and thinking with cool and unusual fruits of the natural world that puts Quay up there with the world\'s best. It certainly gets our vote.<br><br>Pat Nourse','SMH Good Food Guide 2008',1,'2014-11-14 00:42:36','','static/pdf/GT-Article-Sept-08.pdf'),(6,'Maestro Chef, Peter Gilmore continues to scale new heights. Young, courteous and knowledgeable service is the right match for this culinary artist, now showcasing masterworks in a bold new four  course menu with a quartet of choices per course.<br><br>Simon Thomsen,\r\n','SMH Good Food Guide 2008',1,'2014-11-14 00:46:01','','static/pdf/smh_review_2008.pdf'),(7,'\"Experience culinary perfection... and I\'ll be darned if that\'s not exactly what I did.\" \"I\'ve visited every three-star restaurant in Australia... Peter Gilmore\'s style of cooking, as is highlighted through Quay\'s new signature menu, is so wholly formed, so complete in its conception, execution and presentation that to dine at Quay today is to see a gun chef coming to the height of his powers.\"<br><br>Pat Nourse,','Australian Gourmet Traveller, January 2007',1,'2014-11-14 00:43:41','','static/pdf/quay_gourmet_trav_jan07.pdf');
/*!40000 ALTER TABLE `cms_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menu`
--

DROP TABLE IF EXISTS `cms_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `category_id` int(11) NOT NULL,
  `additional_info` varchar(1000) DEFAULT NULL,
  `prices` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_menu_b583a629` (`category_id`),
  CONSTRAINT `cms_menu_category_id_405e4ad64b5d9fb_fk_cms_menucategory_id` FOREIGN KEY (`category_id`) REFERENCES `cms_menucategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menu`
--

LOCK TABLES `cms_menu` WRITE;
/*!40000 ALTER TABLE `cms_menu` DISABLE KEYS */;
INSERT INTO `cms_menu` VALUES (1,1,'2014-11-13 13:10:06',4,'','3 Courses $140\r\n<br>\r\n<br>4 Courses $160'),(2,1,'2014-11-13 13:17:15',5,'','4 Courses $175'),(3,1,'2014-11-26 06:04:19',6,'<p>Optional accompanying wines $95 per person\r\n        <br>Optional accompanying premium wines $175 per person\r\n        <br>\r\n        <br>\r\n        <strong>Tasting Menu available</strong>\r\n        <br>12pm - 1:30pm & 6pm - 9pm</p>','$225 per person');
/*!40000 ALTER TABLE `cms_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menu_courses`
--

DROP TABLE IF EXISTS `cms_menu_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menu_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_id` (`menu_id`,`course_id`),
  KEY `cms_menu_courses_93e25458` (`menu_id`),
  KEY `cms_menu_courses_ea134da7` (`course_id`),
  CONSTRAINT `cms_menu_courses_course_id_3486848adfeeea6f_fk_cms_course_id` FOREIGN KEY (`course_id`) REFERENCES `cms_course` (`id`),
  CONSTRAINT `cms_menu_courses_menu_id_3db80c9ee8415567_fk_cms_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `cms_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menu_courses`
--

LOCK TABLES `cms_menu_courses` WRITE;
/*!40000 ALTER TABLE `cms_menu_courses` DISABLE KEYS */;
INSERT INTO `cms_menu_courses` VALUES (9,1,2,0),(10,1,3,1),(11,1,4,2),(12,1,5,3),(13,2,2,4),(14,2,3,5),(15,2,4,6),(16,2,5,7),(30,3,6,8);
/*!40000 ALTER TABLE `cms_menu_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menucategory`
--

DROP TABLE IF EXISTS `cms_menucategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menucategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menucategory`
--

LOCK TABLES `cms_menucategory` WRITE;
/*!40000 ALTER TABLE `cms_menucategory` DISABLE KEYS */;
INSERT INTO `cms_menucategory` VALUES (4,'lunch'),(5,'dinner'),(6,'tasting');
/*!40000 ALTER TABLE `cms_menucategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menupage`
--

DROP TABLE IF EXISTS `cms_menupage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menupage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intro` varchar(500) NOT NULL,
  `additional_info` varchar(1500) DEFAULT NULL,
  `philosophy` varchar(500) NOT NULL,
  `our_produce` varchar(500) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `background_id` int(11) NOT NULL,
  `display_promo` tinyint(1) NOT NULL,
  `wines_list` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_menupage_48abea43` (`background_id`),
  CONSTRAINT `cms_menupage_background_id_71dfb3aaea3d57ce_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menupage`
--

LOCK TABLES `cms_menupage` WRITE;
/*!40000 ALTER TABLE `cms_menupage` DISABLE KEYS */;
INSERT INTO `cms_menupage` VALUES (1,'At Quay, our passion is to deliver a truly unique dining experience, one which is completely removed from the everyday. Quay’s menu is a continually evolving repertoire; some classics, some reworking of old favourites plus new dishes for each season.','<strong>Please note Quay offers vegetarian options and can cater for most dietary requirements.</strong><br><br>Please feel free to contact us if you have any additional or specific dietary requirements and we will do our best to accommodate your request.<br><br>Please notify us at least 24 hours in advance of the booking.<br><br>In addition, we are also able to offer a vegetarian or gluten free tasting menu upon request if the booking is made at least 24 hours in advance.<br><br>Menu content and prices are subject to change.<br><br>Please be aware that the food images displayed on our website may not reflect the current dishes available on our menu.','Over the years my food philosophy has evolved into a personal style that celebrates being a cook in Australia. It embraces natures diversity and seeks to achieve a sense of balance and purity through produce, technique, texture, flavour and composition.','I am very inspired by nature’s diversity and have been a passionate gardener for years. We have set up close relationships with like minded passionate farmers.<br><br>Continuing on this journey with small independent growers will see even more beautiful and unique produce grace Quay’s menus.',1,'2014-12-01 00:00:43',12,0,'pdf/quay-wine-list.pdf');
/*!40000 ALTER TABLE `cms_menupage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_newsarticle`
--

DROP TABLE IF EXISTS `cms_newsarticle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_newsarticle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `image` varchar(100) NOT NULL,
  `published_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `reservation_link` tinyint(1) NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_newsarticle`
--

LOCK TABLES `cms_newsarticle` WRITE;
/*!40000 ALTER TABLE `cms_newsarticle` DISABLE KEYS */;
INSERT INTO `cms_newsarticle` VALUES (1,'Overseas Passenger Terminal construction','During the overseas passenger terminal upgrade Quay will have modified opening hours. We will be closed for lunch Monday to Thursday and open Friday to Sunday lunch from 12pm - 2pm.','','2014-11-14 05:52:18',1,'2014-11-14 05:52:18',0,'2014-09-19'),(2,'Christmas Day Lunch 2014','5 course Signature Menu with a glass of Charles Heidsieck NV on arrival.<br><br>The cost is $330.00 per person. Children’s menu available for $160.00.<br><br>Payment in full is required at time of booking.','news/news-christmas.jpg','2014-11-14 04:42:15',1,'2014-11-14 04:42:15',1,'2014-12-24'),(3,'NYE 2014','On arrival guests will receive a glass of Charles Heidsieck NV served with a selection of canapés. Six-course signature menu, together with wines expertly matched to each course.<br><br>At the stroke of midnight guests will be treated to a glass of champagne to bring in the New Year.<br><br>The cost is $1250 per person. No children’s price available.<br><br>Payment in full is required at time of booking.','news/news-nye2014.jpg','2014-11-14 04:43:16',1,'2014-11-14 04:43:16',1,'2014-12-31'),(4,'Greenroom NYE 2014','Cocktails in style – Green Room<br><br>The evening will commence at 8pm and conclude at 1am.<br><br>On arrival guests will be served Quay cocktails and sparkling wine.  Sumptuous canapés and premium wines selected by our sommelier Amanda Yallop will flow into the night.','news/news-greenroom.jpg','2014-11-14 04:43:55',1,'2014-11-14 04:43:55',1,'2014-12-31');
/*!40000 ALTER TABLE `cms_newsarticle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_newseventspage`
--

DROP TABLE IF EXISTS `cms_newseventspage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_newseventspage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_newseventspage_48abea43` (`background_id`),
  CONSTRAINT `cms_newseven_background_id_41b24196f19b0ded_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_newseventspage`
--

LOCK TABLES `cms_newseventspage` WRITE;
/*!40000 ALTER TABLE `cms_newseventspage` DISABLE KEYS */;
INSERT INTO `cms_newseventspage` VALUES (4,'NEWS + EVENTS','Complete the form below to receive information on the latest events, news and special offers at Quay.',1,1,'2014-11-14 04:59:26');
/*!40000 ALTER TABLE `cms_newseventspage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_newseventspage_events`
--

DROP TABLE IF EXISTS `cms_newseventspage_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_newseventspage_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newseventspage_id` int(11) NOT NULL,
  `newsarticle_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newseventspage_id` (`newseventspage_id`,`newsarticle_id`),
  KEY `cms_newseventspage_events_4dfdd35f` (`newseventspage_id`),
  KEY `cms_newseventspage_events_7a2f2bdc` (`newsarticle_id`),
  CONSTRAINT `cms__newseventspage_id_53e348bd86311719_fk_cms_newseventspage_id` FOREIGN KEY (`newseventspage_id`) REFERENCES `cms_newseventspage` (`id`),
  CONSTRAINT `cms_newsev_newsarticle_id_70302d7b9ed6ce0a_fk_cms_newsarticle_id` FOREIGN KEY (`newsarticle_id`) REFERENCES `cms_newsarticle` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_newseventspage_events`
--

LOCK TABLES `cms_newseventspage_events` WRITE;
/*!40000 ALTER TABLE `cms_newseventspage_events` DISABLE KEYS */;
INSERT INTO `cms_newseventspage_events` VALUES (1,4,1,0),(2,4,2,1),(3,4,3,2),(4,4,4,3);
/*!40000 ALTER TABLE `cms_newseventspage_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_newspage`
--

DROP TABLE IF EXISTS `cms_newspage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_newspage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `background_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_newspage_48abea43` (`background_id`),
  CONSTRAINT `cms_newspage_background_id_2f07b547e5031092_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_newspage`
--

LOCK TABLES `cms_newspage` WRITE;
/*!40000 ALTER TABLE `cms_newspage` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_newspage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_newspage_news_articles`
--

DROP TABLE IF EXISTS `cms_newspage_news_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_newspage_news_articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newspage_id` int(11) NOT NULL,
  `newsarticle_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newspage_id` (`newspage_id`,`newsarticle_id`),
  KEY `cms_newspage_news_articles_508b60ad` (`newspage_id`),
  KEY `cms_newspage_news_articles_7a2f2bdc` (`newsarticle_id`),
  CONSTRAINT `cms_newspa_newsarticle_id_423ded0cbcdc614c_fk_cms_newsarticle_id` FOREIGN KEY (`newsarticle_id`) REFERENCES `cms_newsarticle` (`id`),
  CONSTRAINT `cms_newspage_new_newspage_id_3dff43a9991c759e_fk_cms_newspage_id` FOREIGN KEY (`newspage_id`) REFERENCES `cms_newspage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_newspage_news_articles`
--

LOCK TABLES `cms_newspage_news_articles` WRITE;
/*!40000 ALTER TABLE `cms_newspage_news_articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_newspage_news_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_openinghours`
--

DROP TABLE IF EXISTS `cms_openinghours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_openinghours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lunch_service` varchar(250) NOT NULL,
  `dinner_service` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_openinghours`
--

LOCK TABLES `cms_openinghours` WRITE;
/*!40000 ALTER TABLE `cms_openinghours` DISABLE KEYS */;
INSERT INTO `cms_openinghours` VALUES (1,'Friday to Sunday from 12pm - 2pm','Daily from 6pm - 10pm');
/*!40000 ALTER TABLE `cms_openinghours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_ourproducerspage`
--

DROP TABLE IF EXISTS `cms_ourproducerspage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_ourproducerspage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_ourproducerspage_48abea43` (`background_id`),
  CONSTRAINT `cms_ourprodu_background_id_67c06b7b2f651154_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_ourproducerspage`
--

LOCK TABLES `cms_ourproducerspage` WRITE;
/*!40000 ALTER TABLE `cms_ourproducerspage` DISABLE KEYS */;
INSERT INTO `cms_ourproducerspage` VALUES (1,'Our<br>producers','The relationships we have formed with our producers, farmers, fishermen and provedores are of paramount importance to the development of our cuisine. Their efforts and passion inspire us every day.',15,1,'2014-11-13 23:29:32');
/*!40000 ALTER TABLE `cms_ourproducerspage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_ourproducerspage_producers`
--

DROP TABLE IF EXISTS `cms_ourproducerspage_producers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_ourproducerspage_producers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ourproducerspage_id` int(11) NOT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ourproducerspage_id` (`ourproducerspage_id`,`producer_id`),
  KEY `cms_ourproducerspage_producers_9637d2f2` (`ourproducerspage_id`),
  KEY `cms_ourproducerspage_producers_295c8ff1` (`producer_id`),
  CONSTRAINT `cms_ourproducers_producer_id_1a930bb3ba08c3ba_fk_cms_producer_id` FOREIGN KEY (`producer_id`) REFERENCES `cms_producer` (`id`),
  CONSTRAINT `ourproducerspage_id_19ee1a75356d2b2e_fk_cms_ourproducerspage_id` FOREIGN KEY (`ourproducerspage_id`) REFERENCES `cms_ourproducerspage` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_ourproducerspage_producers`
--

LOCK TABLES `cms_ourproducerspage_producers` WRITE;
/*!40000 ALTER TABLE `cms_ourproducerspage_producers` DISABLE KEYS */;
INSERT INTO `cms_ourproducerspage_producers` VALUES (4,1,1,0),(5,1,2,1),(6,1,3,2);
/*!40000 ALTER TABLE `cms_ourproducerspage_producers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_ourteampage`
--

DROP TABLE IF EXISTS `cms_ourteampage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_ourteampage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(10000) NOT NULL,
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_ourteampage_48abea43` (`background_id`),
  CONSTRAINT `cms_ourteamp_background_id_699418fe545a9435_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_ourteampage`
--

LOCK TABLES `cms_ourteampage` WRITE;
/*!40000 ALTER TABLE `cms_ourteampage` DISABLE KEYS */;
INSERT INTO `cms_ourteampage` VALUES (1,'OUR TEAM','Quay is a fine example of the sum of its parts being greater than the whole.  Our team has experience that far surpasses their individual job requirements and such passion for food and wine that the end result is an experience of food, wine and service that is simply, award winning.',1,1,'2014-11-14 01:42:46');
/*!40000 ALTER TABLE `cms_ourteampage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_ourteampage_teams`
--

DROP TABLE IF EXISTS `cms_ourteampage_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_ourteampage_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ourteampage_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ourteampage_id` (`ourteampage_id`,`team_id`),
  KEY `cms_ourteampage_producers_2a06bda1` (`ourteampage_id`),
  KEY `cms_ourteampage_producers_f6a7ca40` (`team_id`),
  CONSTRAINT `cms_ourtea_ourteampage_id_3d3e76b7b517a4f2_fk_cms_ourteampage_id` FOREIGN KEY (`ourteampage_id`) REFERENCES `cms_ourteampage` (`id`),
  CONSTRAINT `cms_ourteampage_teams_team_id_1bce7bbacb4748c3_fk_cms_team_id` FOREIGN KEY (`team_id`) REFERENCES `cms_team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_ourteampage_teams`
--

LOCK TABLES `cms_ourteampage_teams` WRITE;
/*!40000 ALTER TABLE `cms_ourteampage_teams` DISABLE KEYS */;
INSERT INTO `cms_ourteampage_teams` VALUES (1,1,1,0),(2,1,2,1),(3,1,3,2),(4,1,4,3),(5,1,5,4),(6,1,6,5),(7,1,7,6),(8,1,8,7),(9,1,9,8),(10,1,10,9);
/*!40000 ALTER TABLE `cms_ourteampage_teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_positions`
--

DROP TABLE IF EXISTS `cms_positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(250) NOT NULL,
  `job_description` varchar(5000) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_positions`
--

LOCK TABLES `cms_positions` WRITE;
/*!40000 ALTER TABLE `cms_positions` DISABLE KEYS */;
INSERT INTO `cms_positions` VALUES (2,'COMMI CHEF/<br>DEMI CHEF','This is a great opportunity to join Peter Gilmore\'s team and work in one of the best restaurants in the country.<br><br>You must have fine dining experience preferably in a hatted or Michelin star restaurant.<br><br>This is a Full Time position, no students.\r\n',1);
/*!40000 ALTER TABLE `cms_positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_producer`
--

DROP TABLE IF EXISTS `cms_producer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_producer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_producers_48abea43` (`background_id`),
  CONSTRAINT `cms_producers_background_id_cf46572409528aa_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_producer`
--

LOCK TABLES `cms_producer` WRITE;
/*!40000 ALTER TABLE `cms_producer` DISABLE KEYS */;
INSERT INTO `cms_producer` VALUES (1,'DAVE<br>ALLEN\r\n','<p>Dave Allen says his fishing career started at the age of 6. Fishing recreationally from the family boat, his father was a stickler for looking after the catch. His philosophy was to ice down promptly, never take more than you need and return the small fish to the water to catch another day. Dave has spent his whole life working with the sea.<br><br>For years Dave has been a strong advocate for the harvesting of sea urchins, which are considered a marine pest in Tasmania. Dave has spent time in Japan learning about the best methods of shucking handling the roe. Dave now exports Tasmanian sea urchin roe internationally.</p>',1,1,'2014-11-13 23:13:41'),(2,'TIM & ELIZABETH<br>JOHNSTONE','<p>Husband and wife team Tim and Elizabeth Johnstone of Johnstone Kitchen Gardens run a small 4 acre farm producing bespoke, heirloom, organically farmed vegetables.<br><br>My working relationship with Tim and Elizabeth began in early 2011. Tim has a Bachelor in Agriculture and worked his small property with his wife Elizabeth to supply a weekend market stall. Tim approached me with the idea of growing produce for Quay as he knew I had similar relationships with other small-scale farmers. Tim and Elizabeth have become my most significant farming partnership. They now produce a large range of heirloom bespoke vegetables for Quay and many other Sydney restaurants.</p>',17,1,'2014-11-13 23:14:17'),(3,'JUNE<br>HENMAN','<p>June Henman is a herb and flower researcher and grower. June has been passionately growing and researching herbs and edible flowers since 1989. June now works as chief herb and flower consultant for Tony Mann who owns and operates Petite Bouche.</p>',18,1,'2014-11-13 23:14:55');
/*!40000 ALTER TABLE `cms_producer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_reservation`
--

DROP TABLE IF EXISTS `cms_reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `personal_phone` varchar(255) NOT NULL,
  `hotel_phone` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) NOT NULL,
  `reservation_time` datetime DEFAULT NULL,
  `num_people` varchar(255) NOT NULL,
  `lunch_dinner` varchar(255) NOT NULL,
  `special_requests` varchar(1000) NOT NULL,
  `optin` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_reservation`
--

LOCK TABLES `cms_reservation` WRITE;
/*!40000 ALTER TABLE `cms_reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_reservationpage`
--

DROP TABLE IF EXISTS `cms_reservationpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_reservationpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `cancelation_policy` varchar(10000) NOT NULL,
  `credit_card` varchar(10000) NOT NULL,
  `dress_code` varchar(10000) NOT NULL,
  `intro_copy` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_reservationpage`
--

LOCK TABLES `cms_reservationpage` WRITE;
/*!40000 ALTER TABLE `cms_reservationpage` DISABLE KEYS */;
INSERT INTO `cms_reservationpage` VALUES (1,1,'2014-12-01 00:59:25','<p>All cancellations must be made via telephone and during the reservation office hours. To avoid incurring a charge lunch bookings must be cancelled prior to 12pm, 24 hours beforehand and dinner bookings by 5pm, 24 hours beforehand.</p>','<p>Quay accepts the following credit cards: Mastercard, Visa. American Express and Diners Club. Please note a 1.5% surcharge will be added for all credit and debit card payments.</p>','<p>The dress code at Quay is smart casual. The dress code is intended to create a pleasant experience for all guests in keeping with our standards of international dining.<br><br>Attire: Please refrain from wearing athletic clothing or footwear. Gentlemen should not wear shorts or sleeveless t-shirts. Footwear: Please refrain from wearing flip flops or ugg boots. Gentlemen should wear fully covered shoes.<br><br>Children are welcome at Quay, although due to the layout of the restaurant we do not have the floor space to accommodate push chairs or prams. Children dining at Quay are the responsibility of their guardian at all time. We require children to remain seated at their table whilst dining at Quay.<br><br>Prams, luggage and large bags can be kept at reception whilst dining. Quay does not take responsibility for any personal items that are lost or damaged whilst dining in the restaurant.</p>','<p>Join us at Quay for a truly memorable dining experience in one of Sydney’s most spectacular settings. An experience that celebrates the diversity of our wonderful Australian produce cooked with imagination and passion. <br><br>Quay is open daily for dinner service from 6pm – 10pm. Lunch service is Friday to Sunday, taking bookings from 12pm – 2pm. If you wish to make a booking within the 48 hour period, please contact reservations directly on 02 9251 5600.<br><br>We do receive a high volume of reservations requests daily. Please allow at least 48 hours in which to reply to your request. We do encourage you to contact reservations directly on 02 9251 5600 if you wish to make a booking within the 48 hour period. <br><br>We take bookings no more than 6 months in advance. Friday, Saturday & Sunday evenings are booked out 6 months in advance for bookings of two and four guests. <br><br>If you do have any dietary requirements please mention these when making your reservation online or via the telephone.</p>');
/*!40000 ALTER TABLE `cms_reservationpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_reservationpage_background`
--

DROP TABLE IF EXISTS `cms_reservationpage_background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_reservationpage_background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservationpage_id` int(11) NOT NULL,
  `background_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reservationpage_id` (`reservationpage_id`,`background_id`),
  KEY `cms_reservationpage_background_d9cc3f1e` (`reservationpage_id`),
  KEY `cms_reservationpage_background_48abea43` (`background_id`),
  CONSTRAINT `cm_reservationpage_id_631cb5856dbe6d48_fk_cms_reservationpage_id` FOREIGN KEY (`reservationpage_id`) REFERENCES `cms_reservationpage` (`id`),
  CONSTRAINT `cms_reservat_background_id_45b260212c9a44ea_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_reservationpage_background`
--

LOCK TABLES `cms_reservationpage_background` WRITE;
/*!40000 ALTER TABLE `cms_reservationpage_background` DISABLE KEYS */;
INSERT INTO `cms_reservationpage_background` VALUES (1,1,7,0),(2,1,8,1),(3,1,9,2),(4,1,10,3);
/*!40000 ALTER TABLE `cms_reservationpage_background` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_reviewsmediapage`
--

DROP TABLE IF EXISTS `cms_reviewsmediapage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_reviewsmediapage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `intro_copy` varchar(1000) NOT NULL,
  `background_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_reviewsmediapage_48abea43` (`background_id`),
  CONSTRAINT `cms_reviewsm_background_id_1564919f230a63aa_fk_cms_background_id` FOREIGN KEY (`background_id`) REFERENCES `cms_background` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_reviewsmediapage`
--

LOCK TABLES `cms_reviewsmediapage` WRITE;
/*!40000 ALTER TABLE `cms_reviewsmediapage` DISABLE KEYS */;
INSERT INTO `cms_reviewsmediapage` VALUES (1,'REVIEWS + MEDIA','Quay is one of Australia’s most highly regarded restaurants.',1,1,'2014-11-14 00:45:23');
/*!40000 ALTER TABLE `cms_reviewsmediapage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_reviewsmediapage_media`
--

DROP TABLE IF EXISTS `cms_reviewsmediapage_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_reviewsmediapage_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewsmediapage_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `sort_value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reviewsmediapage_id` (`reviewsmediapage_id`,`media_id`),
  KEY `cms_reviewsmediapage_media_6c8535fc` (`reviewsmediapage_id`),
  KEY `cms_reviewsmediapage_media_35bf24f6` (`media_id`),
  CONSTRAINT `cms_reviewsmediapage_m_media_id_5357279ff594c712_fk_cms_media_id` FOREIGN KEY (`media_id`) REFERENCES `cms_media` (`id`),
  CONSTRAINT `reviewsmediapage_id_4db1b285b15683b3_fk_cms_reviewsmediapage_id` FOREIGN KEY (`reviewsmediapage_id`) REFERENCES `cms_reviewsmediapage` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_reviewsmediapage_media`
--

LOCK TABLES `cms_reviewsmediapage_media` WRITE;
/*!40000 ALTER TABLE `cms_reviewsmediapage_media` DISABLE KEYS */;
INSERT INTO `cms_reviewsmediapage_media` VALUES (1,1,1,0),(2,1,2,1),(3,1,3,2),(4,1,4,3),(5,1,5,4),(6,1,6,5),(7,1,7,6);
/*!40000 ALTER TABLE `cms_reviewsmediapage_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_specialcontact`
--

DROP TABLE IF EXISTS `cms_specialcontact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_specialcontact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `function` varchar(255),
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `image` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_specialcontact`
--

LOCK TABLES `cms_specialcontact` WRITE;
/*!40000 ALTER TABLE `cms_specialcontact` DISABLE KEYS */;
INSERT INTO `cms_specialcontact` VALUES (1,'FUNCTION BOOKINGS','Functions Manager','(61 2) 9241 4565','(61 2) 9241 4901','functions@quay.com.au',1,'2014-11-28 05:26:21','peoples/michelle.jpg','Michelle Cook'),(2,'OPERATIONS','General Manager','(61 2) 9241 4565','(61 2) 9241 4901','operations@quay.com.au',1,'2014-11-28 05:06:26','','Kylie Ball'),(3,'SOMMELIER',' Head Sommelier','(61 2) 9241 4565','(61 2) 9241 4901','sommelier@quay.com.au',1,'2014-11-28 05:07:07','','Amanda Yallop'),(4,'CHEF','Executive Chef ','','','operations@quay.com.au',1,'2014-11-28 05:05:37','','Peter Gilmore'),(5,'WEDDING ENQUIRIES','Functions Executive','(61 2) 9241 4565','(61 2) 9241 4901','fe@quay.com.au',1,'2014-11-28 05:26:43','peoples/sarah.jpg','Sarah Millett');
/*!40000 ALTER TABLE `cms_specialcontact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_team`
--

DROP TABLE IF EXISTS `cms_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `small_description` varchar(500) NOT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `image` varchar(100),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_team`
--

LOCK TABLES `cms_team` WRITE;
/*!40000 ALTER TABLE `cms_team` DISABLE KEYS */;
INSERT INTO `cms_team` VALUES (1,'Peter Gilmore','EXECUTIVE CHEF','Peter Gilmore is the Executive Chef of Quay restaurant in Sydney. Since 2001, Peter’s creative and original cuisine has seen the restaurant receive an unprecedented number of Australian and international awards, and establish itself at the forefront of Australia’s food scene.','<p>Peter describes his cuisine as \"food inspired by nature\". A passionate gardener, Peter was one of the first chefs in Australia to truly embrace the vegetable garden and its diversity. Sourcing rare plants and heirloom vegetable seeds, he forges unique relationships with small farmers to grow bespoke produce exclusively for Quay. He collaborates with fisherman who hand dive and line catch seafood in pristine Australian waters, and farmers who rear rare breed animals with superior flavour and texture for the table. Peter plans his menus from the ground up a season ahead; a way of working which has strengthened his appreciation of the seasons and the natural world, and is directly reflected in his cuisine.<br><br>Peter describes his cuisine as \"food inspired by nature\". A passionate gardener, Peter was one of the first chefs in Australia to truly embrace the vegetable garden and its diversity. Sourcing rare plants and heirloom vegetable seeds, he forges unique relationships with small farmers to grow bespoke produce exclusively for Quay. He collaborates with fisherman who hand dive and line catch seafood in pristine Australian waters, and farmers who rear rare breed animals with superior flavour and texture for the table. Peter plans his menus from the ground up a season ahead; a way of working which has strengthened his appreciation of the seasons and the natural world, and is directly reflected in his cuisine.<br><br>Peter plays with ideas about food, its provenance and its natural environment. A recent dish was inspired by the beauty of the coral reef in Australia\'s north. In another, he evokes the environment of a hand-caught flathead, pairing it with wild oysters, periwinkles, wakame seaweed and abalone that the fish might feed upon in its cold Southern ocean home. Peter places great emphasis on texture in his cuisine, so a rare breed pig cheek is first slowly braised for twelve hours, then lightly smoked, and teamed with hand dived sea scallops, shaved Tasmanian shiitake mushrooms and crisp Jerusalem artichoke skin to create a perfect interplay of just four ingredients.<br><br>Says Peter: “What we try to do is to produce original, beautifully crafted food with a big emphasis on layers of texture and flavours to create an overall sense of balance. Food that tastes beautiful, that takes you on a journey of different sensations, that makes you think about where it came from.”<br><br>Peter is a 44 year old Australian, born and bred in Sydney. He was inspired to cook at a young age and started his apprenticeship at 16, then spent his twenties working in kitchens overseas and in country New South Wales, developing his own style. Critical recognition came in 2000 when Peter was the Head Chef at De Beers Restaurant at Whale Beach and Terry Durack, food reviewer for _The Sydney Morning Herald_, wrote “De Beers houses a young chef with a real talent for sending out beautifully structured food with innate simplicity”. Peter’s next step was as head&nbsp;chef at Quay, and he has never looked back.</p><p><span style=\"line-height: 1.45em; background-color: initial;\">Quay has been awarded Three Chefs Hats for 12 consecutive years and named Restaurant of the Year five times in _The Sydney Morning Herald Good Food Guide_. It has three stars and has been awarded Restaurant of the Year three times in the _Australian Gourmet Traveller Restaurant Guide_. At _The Sydney Morning Herald Good Food Guide 2012_ Awards, Peter Gilmore was named “Chef of the Year”, a celebration of his ten years at Quay.</span><br></p><p>In 2010, Peter released his first cookbook _Quay: *Food Inspired by Nature*_, a sumptuous book which captures Peter’s nature-based philosophy and the organic presentation of his stunning cuisine at Quay.<br></p><p>Peter regards himself as immensely lucky to be a chef in Australia today, with access to extraordinary produce and with the freedom to explore new ideas and draw on a culture which embraces so many people and parts of the world. He continues to refine his food, bringing new and exciting dining experiences to the table, with picture perfect dishes that celebrate nature’s beauty. He is a chef in his prime, whose combination of extraordinary talent and rare humility has won him not just the highest accolades in the food world, but also the respect of his peers and admirers all over the world.<br></p><p><br></p>',0,'2014-11-28 06:02:57','peoples/peter-gilmore.jpg'),(2,'John Fink','FINK GROUP GENERAL MANAGER','John Fink is a restaurateur, a writer and a filmmaker. As Director of the Fink Group, John runs the family restaurant business with his father, long-time restaurateur, theatre-owner and hotelier Leon Fink. The Fink Group own and operate Quay, the most awarded restaurant in Australia.','<p> Quay was this year named the hottest restaurant in Australia in _The Australian Hot 50_ and is listed amongst the _San Pellegrino World’s 50 Best Restaurants_. It has been named Restaurant of the Year five times in _The Sydney Morning Herald Good Food Guide_ (2003, 2005, 2009, 2010 and 2013) and three times in the _Australian Gourmet Traveller’s Restaurant Guide_ (2009, 2010 and 2013). <br><br>The Fink Group has also for the last seven years owned and operated the iconic Italian-Australian eatery Otto on the Finger Wharf at Woolloomooloo. John can be seen most days at Otto, ensuring that the restaurant retains its place as one of Sydney’s best-loved dining establishments. He has also in recent times pioneered exciting new dining and bar pop-ups on the wharf to capitalize on Sydney’s love of casual dining, and is always on the lookout for the “next thing”.<br><br>In his role managing the restaurants for the group, John oversees a team of 160 highly skilled restaurant professional, as well as controlling operations and marketing for the group.<br><br>John also works in his “spare time” on his personal slate of scripts and films, a passion/talent he undoubtedly inherited from his mother, the prominent Australian film producer Margaret Fink. An alumni of the 2009 screen writing diploma with the AFTRS, his documentary _Big Dreamers_ has appeared in many international film festivals and screened locally with the ABC, as well as overseas. John’s quirky and disturbing short film _Glottal Opera_ screened at the 2010 Sundance Film Festival.<br><br>John lives in Sydney with his two children and is an enthusiastic supporter of charities such as Cure Cancer Australia and Voiceless, along with cultural entities like the Griffin Theatre and the Sydney Theatre Company.</p>',1,'2014-11-28 06:02:45','peoples/john-fink.jpg'),(3,'Kylie Ball','GENERAL MANAGER','Kylie Ball has a passion for hospitality. Following school, she studied Hospitality management at university, all the while working in bars and restaurants and learning the more practical elements of the business.','<p>\r\n               \r\n               Her first major job was working with the Polese family, an iconic Sydney restaurant dynasty, working and managing their Potts Point venue, Mezzaluna. She worked for the business for five years, managing functions, reservations and accounts for this thriving inner-city restaurant. In 2000, Kylie joined Quay as financial controller, managing the books for another fabled restaurant family, the Fink Group. She soon took on responsibility for functions, managing events, partnerships and all functions held in the Green Room, Upper Tower and restaurant.<br><br>The travel bug bit in 2005, and Kylie set off for the United Kingdom. She settled in London where she worked in human resources for two and a half years, and made the most of her proximity to Europe; eating, drinking and travelling her way across the continent at every opportunity. In 2008, Kylie returned to Sydney and was offered the position of Operations Manager at Quay, which she accepted, as she says, “on the spot”. In this role, Kylie oversees the day to day operations of the restaurant, supporting all departments and acts as a personal assistant to Peter Gilmore. Her passion for the industry remains and she continues her education with food, wine, and business courses. Kylie is a vital part of the team that has seen the restaurant establish an unrivalled reputation and list of industry accolades.\r\n            </p>',1,'2014-11-28 06:02:35','peoples/kylie-ball.jpg'),(4,'Sam Aisbett','HEAD CHEF','Growing up in the Whitsundays, Sam Aisbett’s love of food started with him working as an apprentice butcher with his father in the family butcher shop. It was here where he learnt the basic fundamentals of butchery and soon after decided on a career in the kitchen.','<p>\r\n               \r\n              After completing his apprenticeship in some of Queensland’s best restaurants, Sam travelled to Europe to broaden his knowledge and experience, before returning home and moving to Sydney. Sam first began working at Quay in 2006 and quickly worked his way up to Senior Sous Chef. In 2009 Sam got hungry to travel again and spent time completing stages at several Michelin starred restaurants throughout Europe and Asia.<br><br>Returning to Sydney Sam spent two years working at Tetsuya’s restaurant as Sous Chef before spending time in the Blue Mountains as Head Chef at Darley’s restaurant. It was here Sam gained recognition in the industry with his own style of cooking and received a 16/20 review by Terry Durack in the Good Living. While in the mountains, Sam gained a greater appreciation for nature and a passion for using sustainable produce. It was not uncommon to find Sam foraging through the mountains searching for wild herbs and flowers to incorporate into his menus.<br><br>Sam also has a strong love for animals and is an active volunteer with animal conservation groups, which aid in the protection of endangered species. Because of this, Sam would research to ensure he only used sustainable and humanely treated animals. In 2012 Sam was given the opportunity to return to Quay as Peter Gilmore’s Head Chef and accepted the role instantly feeling he has more to learn working alongside his chef and mentor. Sam has a deep understanding of Peter\'s ‘food inspired by nature’ philosophy and his desire and ability has prepared him to continue to lead Quays kitchen team with excellence.\r\n            </p>',1,'2014-11-28 06:02:22','peoples/sam-aisbett.jpg'),(5,'Robert Cockerill','SENIOR SOUS CHEF','Robert began cooking in a small seafood establishment in Newcastle that worked closely with local fishermen and suppliers. Through this and his passion for spear fishing he established the appreciation of marine life, aqua culture and the importance of sustainability.','<p>\r\n               \r\n              Robert has gained great exposure into the significance of quality produce through the insight into his family’s olive oil business further inspiring his appreciation of the harvesting process and the importance of seasonality. In 2006 he headed to London where he was exposed to the varied produce and cuisines of Europe working in high-paced kitchens. From here he heard of what Peter Gilmore was doing at Quay and returned home to Sydney with the ambition to work for him.<br><br>In 2008 a position was available at Quay and Robert was more than happy to accept the opportunity to work with Peter, swiftly forming a close relationship. He worked up to Junior Sous Chef and became Senior Sous chef in 2012. Robert firmly believes in the paddock to plate philosophy and shares Peter’s passion and drive to continue to move Australian cuisine further and deliver excellence.\r\n            </p>',1,'2014-11-28 06:02:10','peoples/robert-cockerill.jpg'),(6,'Sharon Collins','RESTAURANT MANAGER','Sharon is in charge of managing a floor team of over 40 staff and is regarded as one of the best front of house teams in the country.','<p>\r\n                       \r\n              Sharon began working in the hospitality industry while attending University in Adelaide. She subsequently spent several years travelling throughout Europe & the UK, during which time she filled pretty much every role imaginable within a restaurant (with varying degrees of success!). On returning to Australia she moved to Sydney, where casual work transformed into a full-time passion. After several years managing the 3 hatted Pier Restaurant in Sydney Sharon moved to Quay, starting as Assistant Manager in 2010. Lured back in late 2012 by Quay’s ongoing reputation as one of Sydney’s best Restaurants and the opportunity to work alongside Peter Gilmore again as Restaurant Manager.<br><br>Sharon’s vast experience and continual strive for excellence means she continues to raise the bar for service at Quay.\r\n            </p>',1,'2014-11-28 06:01:56','peoples/sharon-collins.jpg'),(7,'Robert Luther Moon','MAITRE D’','Robert is passionate about fine food and wine and consistently inspires Quay’s team to give the restaurant’s discerning clientele the highest possible standards of service.','<p>\r\n               \r\n              Robert has over 25 years experience in the hospitality industry. He started his career in Australia at such renowned hotels as the Sheraton on the Park, Hayman Island Resort and the Ritz Carlton, Sydney. He then headed overseas to gain international experience working at the famed London eatery Quaglino’s in Mayfair before returning to Sydney to take up a position at the legendary Sydney restaurant Level 41 as Assistant Manager.<br><br>Robert took up the role of Maitre d’ at Quay in 2006. His career highlight was meeting and serving the late Princess Diana.\r\n            </p>',1,'2014-11-28 06:01:43','peoples/robert-luther-moon.jpg'),(8,'Amanda Yallop','HEAD SOMMELIER','Amanda is one of the increasing number of women sommeliers making waves in the Australian hospitality industry. In her role as Head Sommelier Amanda manages the wine programme at Quay as well as one of the largest and most talented wine teams in Australia.','<p>\r\n                       \r\n               Amanda’s passion for her craft is evident in Quay’s highly respected and awarded wine list.<br><br>Amanda joined Quay Restaurant in 2006, starting as a member of the floor team. Developing a passion for wine and through constant learning and personal advancement, Amanda achieved her Court of Master Sommeliers Certification in 2009.<br><br>In recognition of her skills, expertise, excellent customer service and hard work Amanda was promoted to Head Sommelier in 2010, becoming responsible for a cellar of over 600 bottles and some of Australia’s most exciting food and wine matching.<br><br>Amanda’s quest to discover new wines and share them with Quay’s guests combined with her growing passion for Champagne led to the introduction of a Champagne feature page including a widespread collection of Small Grower Champagne houses that produce and market their own labels as well as an extensive list of prestige Champagnes from many of the regions Grandes Marques. Additionally, Amanda’s love of wines from the Loire Valley and many different examples of Riesling is also reflected in the list’s extensive selection.<br><br>Amanda has just completed the Wine and Spirit Education Trust Diploma at the Sydney Wine Academy and has her sights set on completing the prestigious Master of Wine exam.<br><br>Amanda has undertaken extensive travel to improve her knowledge of both Old and New World wines, in conjunction with numerous tastings and being involved in several vintages in different vineyards. Her love for Australian wine is reflected by her wide ranging travel throughout Australian wine regions in particular – Yarra Valley, Gippsland, Mornington Peninsula, Macedon Ranges, Geelong, King Valley, Alpine Valley, Beechworth, Rutherglen, Canberra District, Great Southern, Hunter Valley, Orange, Mudgee, Tasmania, Eden Valley, Barossa Valley, Langhorne Creek, Adelaide Hills and the Riverina. She has also visited many vineyards overseas including regions in New Zealand, France, Germany, Portugal, Spain and England, with trips to Austria and Italy planned for 2015.<br><br>Amanda is a budding wine judge and a proud Len Evans Scholar, having completed the 2013 Len Evans tutorial – arguably one of the best and most exclusive wine schools in the world; where once a year only 12 students worldwide are invited to attend. In an effort to constantly work on and improve her palate earlier this year Amanda attended the Advanced Wine Assessment Course at the Australian Wine Research Institute.\r\n            </p>',1,'2014-11-28 06:01:33','peoples/amanda-yallop.jpg'),(9,'Michelle Cook','FUNCTIONS MANAGER','As Functions Manager at Quay, Michelle manages the private dining and special events department at Quay. With close attention to detail and a focus on creating an exceptional event and a lasting impression whether for a corporate event, a special birthday or your wedding day.','<p>Michelle collaborates with the Front of House team and kitchen team to personalise every experience. She entered the hospitality industry in 1998 and worked in events and catering in organisations which include following Ritz Carlton Hotel, Taronga Centre, Gastronomy Catering, Toast Food (Museum of Sydney) and Truffle Group (Opera House). Michelle joined the Quay team in position of Functions Manager in 2005.</p>',1,'2014-11-28 06:01:21','peoples/michelle-cook.jpg'),(10,'Sarah Millett','FUNCTIONS EXECUTIVE','Sarah joined Quay in mid 2014 bringing a youthful energy and a breadth of dining knowledge and experience for our clients and guests.','<p>With an avid interest in the hospitality industry, Sarah has immersed herself into the Australian restaurant scene since arriving from the UK in 2008 working for the Rockpool group for over two years, later moving to the Merivale Group in 2011 her hospitality experience includes restaurant management, HR and payroll and functions.<br><br>As Functions Executive, Sarah manages the private events and weddings at Quay working alongside our Functions Manager. Her warm, personal approach ensures any special event is arranged smoothly, be it an elaborate wedding or an intimate birthday dinner with friends with unique thought given to each client’s individual needs.</p>',1,'2014-11-28 06:01:04','peoples/sarah-millett.jpg');
/*!40000 ALTER TABLE `cms_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-11-05 05:46:52','1','Background object',1,'',11,1),(2,'2014-11-05 05:47:29','1','NewsArticle object',1,'',8,1),(3,'2014-11-05 05:47:39','1','headline',1,'',12,1),(4,'2014-11-05 05:50:30','1','headline',2,'Changed active.',12,1),(5,'2014-11-05 06:05:50','1','image-news',2,'Changed cssClass.',11,1),(6,'2014-11-05 06:06:08','1','image-about',2,'Changed cssClass.',11,1),(7,'2014-11-05 06:07:31','1','headlineAt Quay our aim is to match the breathtaking harbour setting with quintessential modern Australian cuisine. Warm, knowledgeable, professional staff will extend genuine hospitality as they guid',2,'Changed headline.',12,1),(8,'2014-11-05 06:07:40','1','At Quay our aim is to match the breathtaking harbour setting with quintessential modern Australian cuisine. Warm, knowledgeable, professional staff will extend genuine hospitality as they guide you th',2,'Changed headline.',12,1),(9,'2014-11-05 06:08:51','1','About Quay',2,'Changed headline and intro_copy.',12,1),(10,'2014-11-05 10:31:25','1','About Quay2',2,'Changed headline.',12,1),(11,'2014-11-05 10:31:34','1','About Quay2\\',2,'Changed headline.',12,1),(12,'2014-11-05 10:31:38','1','About Quay',2,'Changed headline.',12,1),(13,'2014-11-13 02:41:47','2','dish-duck',1,'',11,1),(14,'2014-11-13 02:41:59','3','dish-beetroot',1,'',11,1),(15,'2014-11-13 02:42:08','4','dish-pigjowl',1,'',11,1),(16,'2014-11-13 02:42:17','5','dish-beef',1,'',11,1),(17,'2014-11-13 02:42:27','6','dish-muscovado',1,'',11,1),(18,'2014-11-13 02:42:58','7','dish-chocethereal',1,'',11,1),(19,'2014-11-13 02:43:05','8','dish-quail',1,'',11,1),(20,'2014-11-13 02:43:12','9','dish-xosea',1,'',11,1),(21,'2014-11-13 02:43:22','10','dish-blackpudding',1,'',11,1),(22,'2014-11-13 02:47:07','1','book-organum',1,'',15,1),(23,'2014-11-13 02:47:21','1','ORGANUM',1,'',10,1),(24,'2014-11-13 02:49:08','2','book-quay',1,'',15,1),(25,'2014-11-13 02:50:52','2','QUAY',1,'',10,1),(26,'2014-11-13 03:13:29','1','HomePage object',1,'',18,1),(27,'2014-11-13 03:19:02','1','NewsArticle object',2,'Changed title and description.',8,1),(28,'2014-11-13 04:22:55','11','illustration-spring',1,'',11,1),(29,'2014-11-13 04:23:06','12','illustration-summer',1,'',11,1),(30,'2014-11-13 04:23:15','13','illustration-autumn',1,'',11,1),(31,'2014-11-13 04:23:23','14','illustration-winter',1,'',11,1),(32,'2014-11-13 04:24:50','1','[(u\'id\', u\'1\'), (\'background\', u\'11\'), (\'intro\', u\'At Quay, our passion is to deliver a truly unique dining experience, one which is completely removed from the everyday. Quay\\u2019s menu is a continu',1,'',19,1),(39,'2014-11-13 12:16:14','4','lunch',1,'',25,1),(40,'2014-11-13 12:16:33','5','dinner',1,'',25,1),(41,'2014-11-13 12:30:40','3','Greenlip abalone',1,'',23,1),(42,'2014-11-13 12:31:09','4','Raw smoked Blackmore wagyu',1,'',23,1),(43,'2014-11-13 12:31:29','5','Congee of Northern Australian mud crab',1,'',23,1),(44,'2014-11-13 12:31:48','6','Salad of celery heart',1,'',23,1),(45,'2014-11-13 12:32:09','7','Smoked and confit pig jowl',1,'',23,1),(46,'2014-11-13 12:32:27','8','Bay lobster',1,'',23,1),(47,'2014-11-13 12:32:42','9','XO Sea',1,'',23,1),(48,'2014-11-13 12:33:08','10','Spring peas',1,'',23,1),(49,'2014-11-13 12:33:31','11','Chatham Island blue cod',1,'',23,1),(50,'2014-11-13 12:33:48','12','Slow cooked duck',1,'',23,1),(51,'2014-11-13 12:34:05','13','Flinders Island salt grass lamb',1,'',23,1),(52,'2014-11-13 12:34:24','14','Stone pot organic green rice',1,'',23,1),(53,'2014-11-13 12:34:46','15','Citrus and almonds',1,'',23,1),(54,'2014-11-13 12:35:05','16','Chocolate ethereal',1,'',23,1),(55,'2014-11-13 12:35:20','17','Snow egg',1,'',23,1),(56,'2014-11-13 12:35:48','18','Cheese',1,'',23,1),(57,'2014-11-13 12:37:37','19','Muscovado',1,'',23,1),(58,'2014-11-13 12:39:18','20','Coffee, Tea & Quay Petits Fours',1,'',23,1),(59,'2014-11-13 12:44:09','2','One',1,'',24,1),(60,'2014-11-13 12:44:53','3','Two',1,'',24,1),(61,'2014-11-13 12:48:40','2','One',2,'Changed course_title.',24,1),(62,'2014-11-13 12:49:00','3','Two',2,'Changed course_title.',24,1),(63,'2014-11-13 12:50:35','4','Lunch course three',1,'',24,1),(64,'2014-11-13 12:51:27','5','Lunch course four',1,'',24,1),(65,'2014-11-13 12:51:46','1','Menu object',1,'',21,1),(66,'2014-11-13 12:52:01','3','Lunch course two',2,'Changed course_title.',24,1),(67,'2014-11-13 13:06:47','1','Menu object',2,'Changed prices and additional_info.',21,1),(68,'2014-11-13 13:10:06','1','Menu object',2,'Changed additional_info.',21,1),(69,'2014-11-13 13:17:15','2','dinner',1,'',21,1),(70,'2014-11-13 13:18:08','6','tasting menu',1,'',25,1),(71,'2014-11-13 13:18:27','6','tasting',2,'Changed category.',25,1),(72,'2014-11-13 13:21:59','6','Tasting course',1,'',24,1),(73,'2014-11-13 13:22:49','3','tasting',1,'',21,1),(74,'2014-11-13 14:10:00','1','Menu page   -  created: 11/13/2014',2,'Changed additional_info.',19,1),(76,'2014-11-13 22:27:09','15','image-producers-01',1,'',11,1),(77,'2014-11-13 22:27:15','16','image-producers-02',1,'',11,1),(78,'2014-11-13 22:27:22','17','image-producers-03',1,'',11,1),(79,'2014-11-13 22:27:43','18','image-producers-04',1,'',11,1),(83,'2014-11-13 23:19:12','1','Our producers',1,'',30,1),(84,'2014-11-13 23:29:32','1','Our<br>producers',2,'Changed headline.',30,1),(85,'2014-11-13 23:37:05','19','image-philosophy-01',1,'',11,1),(86,'2014-11-13 23:37:58','1','FOODPHILOSOPHY',1,'',28,1),(87,'2014-11-14 00:18:06','1','Media object',1,'',31,1),(88,'2014-11-14 00:40:07','1','SMH Good Food Guide 2012',2,'Changed active and url.',31,1),(89,'2014-11-14 00:40:48','2','SMH Good Food Guide 2011',1,'',31,1),(90,'2014-11-14 00:41:25','3','SMH Good Food Guide 2008',1,'',31,1),(91,'2014-11-14 00:42:03','4','SMH Good Food Guide 2008',1,'',31,1),(92,'2014-11-14 00:42:36','5','SMH Good Food Guide 2008',1,'',31,1),(93,'2014-11-14 00:43:09','6','SMH Good Food Guide 200',1,'',31,1),(94,'2014-11-14 00:43:41','7','Australian Gourmet Traveller, January 2007',1,'',31,1),(95,'2014-11-14 00:45:23','1','REVIEWS + MEDIA',1,'',32,1),(96,'2014-11-14 00:46:01','6','SMH Good Food Guide 2008',2,'Changed quote and editor.',31,1),(97,'2014-11-14 01:07:46','20','image-awards',1,'',11,1),(98,'2014-11-14 01:08:09','1','AWARDS',1,'',33,1),(99,'2014-11-14 01:13:08','1','OUR TEAM',1,'',36,1),(100,'2014-11-14 01:20:47','1','Team object',1,'',35,1),(101,'2014-11-14 01:34:23','1','Peter Gilmore',2,'Changed description.',35,1),(102,'2014-11-14 01:35:46','2','John Fink',1,'',35,1),(103,'2014-11-14 01:36:38','3','Kylie Ball',1,'',35,1),(104,'2014-11-14 01:37:30','4','Sam Aisbett',1,'',35,1),(105,'2014-11-14 01:38:32','5','Robert Cockerill',1,'',35,1),(106,'2014-11-14 01:39:07','6','Sharon Collins',1,'',35,1),(107,'2014-11-14 01:39:55','7','Robert Luther Moon',1,'',35,1),(108,'2014-11-14 01:40:33','8','Amanda Yallop',1,'',35,1),(109,'2014-11-14 01:41:17','9','Michelle Cook',1,'',35,1),(110,'2014-11-14 01:42:01','10','Sarah Millett',1,'',35,1),(111,'2014-11-14 01:42:46','1','OUR TEAM',2,'Changed producers.',36,1),(112,'2014-11-14 03:30:34','1','NEWS + EVENTS',1,'',34,1),(113,'2014-11-14 04:40:29','2','Christmas Day Lunch 2014  -  created: 11/14/2014',1,'',8,1),(114,'2014-11-14 04:41:41','2','Christmas Day Lunch 2014  -  created: 11/14/2014',2,'Changed description.',8,1),(115,'2014-11-14 04:42:15','2','Christmas Day Lunch 2014  -  created: 11/14/2014',2,'Changed description.',8,1),(116,'2014-11-14 04:43:16','3','NYE 2014  -  created: 11/14/2014',1,'',8,1),(117,'2014-11-14 04:43:55','4','Greenroom NYE 2014  -  created: 11/14/2014',1,'',8,1),(118,'2014-11-14 04:59:26','4','NEWS + EVENTS',1,'',34,1),(119,'2014-11-14 05:52:18','1','Overseas Passenger Terminal construction  -  created: 11/14/2014',2,'Changed date and image.',8,1),(120,'2014-11-14 06:39:06','21','image-functions-01',1,'',11,1),(121,'2014-11-14 06:39:18','22','uppertower-01',1,'',11,1),(122,'2014-11-14 06:39:35','23','uppertower-02',1,'',11,1),(123,'2014-11-14 06:39:40','24','uppertower-03',1,'',11,1),(124,'2014-11-14 06:39:51','25','uppertower-01',1,'',11,1),(125,'2014-11-14 06:41:18','26','image-functions-uppertower',1,'',11,1),(126,'2014-11-14 06:41:34','27','image-functions-greenroom',1,'',11,1),(127,'2014-11-14 06:41:52','28','greenroom-01',1,'',11,1),(128,'2014-11-14 06:41:58','29','greenroom-02',1,'',11,1),(129,'2014-11-14 06:42:14','30','greenroom-03',1,'',11,1),(130,'2014-11-14 06:43:51','31','image-functions-restaurant',1,'',11,1),(131,'2014-11-14 06:44:09','32','restaurant-01',1,'',11,1),(132,'2014-11-14 06:44:14','33','restaurant-02',1,'',11,1),(133,'2014-11-14 06:44:19','34','restaurant-03',1,'',11,1),(134,'2014-11-14 06:45:55','35','image-functions-02',1,'',11,1),(135,'2014-11-14 06:45:59','1','FUNCTIONS',1,'',26,1),(136,'2014-11-14 06:57:38','1','FUNCTIONS',2,'Changed active.',26,1),(137,'2014-11-16 10:32:07','1','OpeningHours object',1,'',17,1),(138,'2014-11-16 10:42:34','1','Contact object',1,'',16,1),(139,'2014-11-16 10:51:04','1','Contact object',2,'Changed fax.',16,1),(140,'2014-11-16 10:51:21','1','Contact object',2,'Changed fax.',16,1),(141,'2014-11-16 23:33:29','36','image-employment',1,'',11,1),(142,'2014-11-16 23:35:40','2','COMMI CHEF/<br>DEMI CHEF',1,'',40,1),(143,'2014-11-16 23:36:23','3','COMMI CHEF/<br>DEMI CHEF',1,'',40,1),(144,'2014-11-16 23:36:41','1','EMPLOYMENT',1,'',41,1),(145,'2014-11-16 23:58:49','3','COMMI CHEF/<br>DEMI CHEF',3,'',40,1),(146,'2014-11-17 00:02:38','1','EMPLOYMENT',2,'No fields changed.',41,1),(147,'2014-11-17 00:28:02','2','COMMI CHEF/<br>DEMI CHEF',2,'Changed job_description.',40,1),(148,'2014-11-26 04:59:17','3','tasting',2,'Changed courses.',21,1),(149,'2014-11-26 05:00:11','3','tasting',2,'Changed courses.',21,1),(150,'2014-11-26 05:58:27','3','tasting',2,'Changed courses.',21,1),(151,'2014-11-26 05:58:43','3','tasting',2,'No fields changed.',21,1),(152,'2014-11-26 05:59:40','3','tasting',2,'No fields changed.',21,1),(153,'2014-11-26 06:00:04','3','tasting',2,'Changed courses.',21,1),(154,'2014-11-26 06:00:24','1','Home page   -  created: 11/26/2014',2,'Changed welcome_copy.',18,1),(155,'2014-11-26 06:00:43','1','Home page   -  created: 11/26/2014',2,'Changed welcome_copy.',18,1),(156,'2014-11-26 06:03:42','3','tasting',2,'Changed courses.',21,1),(157,'2014-11-26 06:04:19','3','tasting',2,'Changed courses.',21,1),(158,'2014-11-26 06:45:13','1','FUNCTIONS',2,'Changed uppertower_carou, greenroom_carou and restaurant_carou.',26,1),(159,'2014-11-27 06:56:58','1','Menu page   -  created: 11/27/2014',2,'Changed background.',19,1),(160,'2014-11-28 01:04:04','37','image-contact',1,'',11,1),(161,'2014-11-28 01:25:03','1','CONTACT',1,'',27,1),(162,'2014-11-28 03:22:10','1','FUNCTION BOOKINGS',1,'',42,1),(163,'2014-11-28 03:24:23','2','OPERATIONS',1,'',42,1),(164,'2014-11-28 03:30:08','3','SOMMELIER',1,'',42,1),(165,'2014-11-28 03:30:44','4','CHEF',1,'',42,1),(166,'2014-11-28 03:40:48','1','CONTACT',2,'Changed contacts.',27,1),(167,'2014-11-28 04:17:58','1','CONTACT',2,'No fields changed.',27,1),(168,'2014-11-28 04:18:12','1','CONTACT',2,'No fields changed.',27,1),(169,'2014-11-28 04:50:58','1','FUNCTION BOOKINGS',2,'Changed image.',42,1),(170,'2014-11-28 04:57:12','5','WEDDING ENQUIRIES',1,'',42,1),(171,'2014-11-28 05:05:26','1','FUNCTION BOOKINGS',2,'Changed name and function.',42,1),(172,'2014-11-28 05:05:37','4','CHEF',2,'Changed name and function.',42,1),(173,'2014-11-28 05:05:53','5','WEDDING ENQUIRIES',2,'Changed name and function.',42,1),(174,'2014-11-28 05:06:26','2','OPERATIONS',2,'Changed name and function.',42,1),(175,'2014-11-28 05:07:07','3','SOMMELIER',2,'Changed name and function.',42,1),(176,'2014-11-28 05:10:50','1','FUNCTIONS',2,'Changed contacts.',26,1),(177,'2014-11-28 05:25:17','5','WEDDING ENQUIRIES',2,'Changed image.',42,1),(178,'2014-11-28 05:25:30','1','FUNCTION BOOKINGS',2,'Changed image.',42,1),(179,'2014-11-28 05:26:21','1','FUNCTION BOOKINGS',2,'Changed image.',42,1),(180,'2014-11-28 05:26:43','5','WEDDING ENQUIRIES',2,'Changed image.',42,1),(181,'2014-11-28 06:01:04','10','Sarah Millett',2,'Changed image.',35,1),(182,'2014-11-28 06:01:21','9','Michelle Cook',2,'Changed image.',35,1),(183,'2014-11-28 06:01:33','8','Amanda Yallop',2,'Changed image.',35,1),(184,'2014-11-28 06:01:43','7','Robert Luther Moon',2,'Changed image.',35,1),(185,'2014-11-28 06:01:56','6','Sharon Collins',2,'Changed image.',35,1),(186,'2014-11-28 06:02:10','5','Robert Cockerill',2,'Changed image.',35,1),(187,'2014-11-28 06:02:22','4','Sam Aisbett',2,'Changed image.',35,1),(188,'2014-11-28 06:02:35','3','Kylie Ball',2,'Changed image.',35,1),(189,'2014-11-28 06:02:45','2','John Fink',2,'Changed image.',35,1),(190,'2014-11-28 06:02:57','1','Peter Gilmore',2,'Changed image.',35,1),(191,'2014-11-30 22:35:03','1','Menu page   -  created: 11/30/2014',2,'Changed wines_list.',19,1),(192,'2014-11-30 23:55:35','1','Menu page   -  created: 11/30/2014',2,'Changed wines_list.',19,1),(193,'2014-12-01 00:00:43','1','Menu page   -  created: 12/01/2014',2,'Changed wines_list.',19,1),(194,'2014-12-01 00:41:03','1','Reservation page   -  created: 12/01/2014',1,'',43,1),(195,'2014-12-01 00:59:25','1','Reservation page   -  created: 12/01/2014',2,'Changed background, intro_copy, dress_code, cancelation_policy and credit_card.',43,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'log entry','admin','logentry'),(8,'news article','cms','newsarticle'),(9,'award','cms','award'),(10,'book','cms','book'),(11,'background','cms','background'),(12,'about page','cms','aboutpage'),(13,'news page','cms','newspage'),(14,'reservation','cms','reservation'),(15,'book image','cms','bookimage'),(16,'contact','cms','contact'),(17,'opening hours','cms','openinghours'),(18,'home page','cms','homepage'),(19,'menu page','cms','menupage'),(21,'menu','cms','menu'),(23,'dish','cms','dish'),(24,'course','cms','course'),(25,'menu category','cms','menucategory'),(26,'functions page','cms','functionspage'),(27,'contact page','cms','contactpage'),(28,'food philosophy page','cms','foodphilosophypage'),(30,'our producers page','cms','ourproducerspage'),(31,'media','cms','media'),(32,'reviews media page','cms','reviewsmediapage'),(33,'award page','cms','awardpage'),(34,'news events page','cms','newseventspage'),(35,'team','cms','team'),(36,'our team page','cms','ourteampage'),(37,'producer','cms','producer'),(40,'positions','cms','positions'),(41,'employment page','cms','employmentpage'),(42,'special contact','cms','specialcontact'),(43,'reservation page','cms','reservationpage');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2014-11-05 05:42:56'),(2,'auth','0001_initial','2014-11-05 05:42:56'),(3,'admin','0001_initial','2014-11-05 05:42:56'),(4,'cms','0001_initial','2014-11-05 05:42:56'),(5,'cms','0002_remove_reservation_time','2014-11-05 05:42:56'),(6,'cms','0003_auto_20141105_1640','2014-11-05 05:42:57'),(7,'sessions','0001_initial','2014-11-05 05:42:57'),(8,'sites','0001_initial','2014-11-05 05:42:57'),(9,'cms','0004_award_active','2014-11-05 05:49:35'),(10,'cms','0005_auto_20141105_1650','2014-11-05 05:50:22'),(11,'cms','0006_auto_20141113_1229','2014-11-13 01:52:49'),(12,'cms','0007_auto_20141113_1412','2014-11-13 03:12:04'),(14,'cms','0008_menupage','2014-11-13 04:20:45'),(15,'cms','0009_menupage_display_promo','2014-11-13 04:57:51'),(16,'cms','0010_auto_20141113_1627','2014-11-13 05:27:58'),(17,'cms','0011_remove_homepage_order','2014-11-13 05:32:24'),(18,'cms','0010_auto_20141113_1846','2014-11-13 07:49:11'),(19,'cms','0011_menuitem_name','2014-11-13 08:30:35'),(20,'cms','0012_auto_20141113_2304','2014-11-13 12:04:58'),(21,'cms','0013_course_course_title','2014-11-13 12:48:07'),(22,'cms','0014_auto_20141113_2355','2014-11-13 12:55:17'),(23,'cms','0015_auto_20141114_0007','2014-11-13 13:07:05'),(24,'cms','0016_auto_20141114_0009','2014-11-13 13:09:37'),(25,'cms','0017_auto_20141114_0109','2014-11-13 14:09:42'),(26,'cms','0018_auto_20141114_0754','2014-11-13 20:54:16'),(27,'cms','0019_auto_20141114_0801','2014-11-13 21:01:53'),(28,'cms','0020_auto_20141114_1120','2014-11-14 00:20:45'),(29,'cms','0021_auto_20141114_1154','2014-11-14 00:55:02'),(30,'cms','0022_team_image','2014-11-14 01:17:27'),(31,'cms','0023_auto_20141114_1222','2014-11-14 01:22:19'),(32,'cms','0024_auto_20141114_1224','2014-11-14 01:24:25'),(33,'cms','0025_auto_20141114_1228','2014-11-14 01:28:54'),(34,'cms','0026_auto_20141114_1234','2014-11-14 01:34:20'),(35,'cms','0027_auto_20141114_1252','2014-11-14 01:52:04'),(36,'cms','0028_newsevents','2014-11-14 03:36:10'),(37,'cms','0029_auto_20141114_1442','2014-11-14 03:42:15'),(38,'cms','0030_auto_20141114_1452','2014-11-14 03:53:25'),(39,'cms','0031_auto_20141114_1457','2014-11-14 03:57:05'),(40,'cms','0032_auto_20141114_1517','2014-11-14 04:18:06'),(41,'cms','0033_newseventspage_events','2014-11-14 04:18:24'),(42,'cms','0034_delete_newsevent','2014-11-14 04:18:43'),(43,'cms','0035_newsarticle_date','2014-11-14 04:29:37'),(44,'cms','0036_auto_20141114_1552','2014-11-14 04:52:49'),(45,'cms','0037_remove_newseventspage_events','2014-11-14 04:58:48'),(46,'cms','0038_newseventspage_events','2014-11-14 04:59:07'),(47,'cms','0039_auto_20141114_1731','2014-11-14 06:31:10'),(48,'cms','0040_auto_20141117_1027','2014-11-16 23:27:52'),(49,'cms','0041_auto_20141126_1737','2014-11-26 06:37:49'),(50,'cms','0042_auto_20141126_1743','2014-11-26 06:43:26'),(51,'cms','0043_auto_20141126_1743','2014-11-26 06:43:46'),(52,'cms','0044_auto_20141128_1215','2014-11-28 01:16:43'),(53,'cms','0045_auto_20141128_1412','2014-11-28 03:16:29'),(54,'cms','0046_auto_20141128_1531','2014-11-28 04:32:06'),(55,'cms','0047_auto_20141128_1602','2014-11-28 05:02:29'),(56,'cms','0048_auto_20141128_1624','2014-11-28 05:24:28'),(57,'cms','0049_auto_20141128_1640','2014-11-28 05:40:10'),(58,'cms','0050_menupage_wines_list','2014-11-30 22:33:05'),(59,'cms','0051_auto_20141201_1011','2014-11-30 23:12:05'),(60,'cms','0052_auto_20141201_1055','2014-11-30 23:55:06'),(61,'cms','0053_auto_20141201_1056','2014-11-30 23:56:33'),(62,'cms','0054_reservationpage','2014-12-01 00:39:21'),(63,'cms','0055_auto_20141201_1145','2014-12-01 00:45:51'),(64,'cms','0056_remove_reservationpage_background','2014-12-01 00:56:19'),(65,'cms','0057_reservationpage_background','2014-12-01 00:56:38');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('gjulp1hvah78rordoqmjdtlet8j4qyky','ZTVjNGE4NTA3ZjU4ZjMzYmE0ZTY3M2FiYTBlOTFlMTMwMDA3OTgyYTp7Il9hdXRoX3VzZXJfaGFzaCI6IjZhYjI2MWFiNDAxMDRiYTIwYTBmYjdlNmExMmQ5ZTY5ZmQxNDhhMjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-11-19 05:43:33'),('ldi0ejpa86tmd7lmel01t2heyg731xii','ZTVjNGE4NTA3ZjU4ZjMzYmE0ZTY3M2FiYTBlOTFlMTMwMDA3OTgyYTp7Il9hdXRoX3VzZXJfaGFzaCI6IjZhYjI2MWFiNDAxMDRiYTIwYTBmYjdlNmExMmQ5ZTY5ZmQxNDhhMjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-12-10 04:43:57'),('ln331lglzygt52enzt5ykc86jc669rvl','ZTVjNGE4NTA3ZjU4ZjMzYmE0ZTY3M2FiYTBlOTFlMTMwMDA3OTgyYTp7Il9hdXRoX3VzZXJfaGFzaCI6IjZhYjI2MWFiNDAxMDRiYTIwYTBmYjdlNmExMmQ5ZTY5ZmQxNDhhMjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2014-11-27 02:14:28');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-01 12:08:53
